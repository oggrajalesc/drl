package com.carvajal.platform.frecuenciaLegado;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carvajal.platform.frecuencialegado.FrecuenciaLegadoApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FrecuenciaLegadoApplication.class)
public class FrecuenciaLegadoApplicationTests {

	@Autowired
	private FrecuenciaLegadoApplication frecuenciaLegadoApplication;

	@Test
	public void mainTest() {
		String[] args = {""};
		frecuenciaLegadoApplication.main(args);
	}
}
