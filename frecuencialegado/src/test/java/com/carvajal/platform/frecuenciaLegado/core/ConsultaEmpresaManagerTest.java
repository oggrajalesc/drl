package com.carvajal.platform.frecuenciaLegado.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.carvajal.platform.frecuencialegado.EnumTipoEmpresa;
import com.carvajal.platform.frecuencialegado.core.ConsultaEmpresaManager;
import com.carvajal.platform.frecuencialegado.core.impl.ConsultaEmpresaManagerImpl;
import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.EmpresaResponseDTO;
import com.carvajal.platform.frecuencialegado.service.OrdenCompraService;
import com.carvajal.platform.frecuencialegado.service.ValidacionesService;
import com.carvajal.platform.frecuencialegado.service.impl.OrdenCompraServiceImpl;
import com.carvajal.platform.frecuencialegado.service.impl.ValidacionesServiceImpl;

@RunWith(SpringRunner.class)
public class ConsultaEmpresaManagerTest {

	@Mock
	private ConsultaEmpresaManager consultaEmpresaManager;
	
	@Mock
	private ValidacionesService validacionesService = new ValidacionesServiceImpl();
	
	@Mock
	private OrdenCompraService ordenCompraService = new OrdenCompraServiceImpl();
	
	@Before
	public void init() {
		consultaEmpresaManager = new ConsultaEmpresaManagerImpl();
		ReflectionTestUtils.setField(consultaEmpresaManager,"validacionesService",validacionesService);
		ReflectionTestUtils.setField(consultaEmpresaManager,"ordenCompraService",ordenCompraService);
	}

	
	@Test
	public void consultarIdEmpresaTest() {
		EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
		EmpresaResponseDTO empresaResponseDTO = consultaEmpresaManager.obtenerIdEmpresa(empresaRequestDTO);
		assertNotNull(empresaResponseDTO);
	}
	
	@Test
	public void consultarIdEmpresaOKSUTest() {
		EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
		empresaRequestDTO.setTipoEmpresa(EnumTipoEmpresa.SU.toString());
		empresaRequestDTO.setCodigoEAN("EAN123");
		empresaRequestDTO.setCodigoEAN("CO");
		
		Boolean validarEmpresa = true;
		Boolean validarRequest = true;
		Boolean validarPais = true;
		String idEmpresa = "1234";
		
		EmpresaResponseDTO empresaResponseDTO = new EmpresaResponseDTO();
		empresaResponseDTO.setError(Boolean.FALSE);
		empresaResponseDTO.setIdEmpresa(idEmpresa);
		Mockito.when(validacionesService.validarRequestNotNull(empresaRequestDTO)).thenReturn(validarRequest);
		Mockito.when(validacionesService.validarRequestEmpresa(empresaRequestDTO)).thenReturn(validarEmpresa);
		Mockito.when(validacionesService.validarPais(empresaRequestDTO.getCodigoPais())).thenReturn(validarPais);
		Mockito.when(ordenCompraService.obtenerIdEmpresa(empresaRequestDTO)).thenReturn(empresaResponseDTO);
		
		EmpresaResponseDTO empresaResponseDTO2 = consultaEmpresaManager.obtenerIdEmpresa(empresaRequestDTO);
		assertFalse(empresaResponseDTO2.isError());
		assertEquals(empresaResponseDTO2.getCodigoEAN(), empresaRequestDTO.getCodigoEAN());
		assertEquals(empresaResponseDTO2.getCodigoPais(), empresaRequestDTO.getCodigoPais());
		assertEquals(empresaResponseDTO2.getTipoEmpresa(), empresaRequestDTO.getTipoEmpresa());
	}
	
	@Test
	public void consultarIdEmpresaOKBYTest() {
		EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
		empresaRequestDTO.setTipoEmpresa(EnumTipoEmpresa.BY.toString());
		empresaRequestDTO.setCodigoEAN("EAN123");
		empresaRequestDTO.setCodigoEAN("CO");		
		
		Boolean validarEmpresa = true;
		Boolean validarRequest = true;
		Boolean validarPais = true;
		String idEmpresa = "1234";
		
		EmpresaResponseDTO empresaResponseDTO = new EmpresaResponseDTO();
		empresaResponseDTO.setError(Boolean.FALSE);
		empresaResponseDTO.setIdEmpresa(idEmpresa);
		Mockito.when(validacionesService.validarRequestNotNull(empresaRequestDTO)).thenReturn(validarRequest);
		Mockito.when(validacionesService.validarRequestEmpresa(empresaRequestDTO)).thenReturn(validarEmpresa);
		Mockito.when(validacionesService.validarPais(empresaRequestDTO.getCodigoPais())).thenReturn(validarPais);
		Mockito.when(ordenCompraService.obtenerIdEmpresa(empresaRequestDTO)).thenReturn(empresaResponseDTO);
		
		EmpresaResponseDTO empresaResponseDTO2 = consultaEmpresaManager.obtenerIdEmpresa(empresaRequestDTO);
		assertFalse(empresaResponseDTO2.isError());
		assertEquals(empresaResponseDTO2.getCodigoEAN(), empresaRequestDTO.getCodigoEAN());
		assertEquals(empresaResponseDTO2.getCodigoPais(), empresaRequestDTO.getCodigoPais());
		assertEquals(empresaResponseDTO2.getTipoEmpresa(), empresaRequestDTO.getTipoEmpresa());		
	}
//	
//	@Test
//	public void consultarIdEmpresaOKBYTest() {
//		EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
//		empresaRequestDTO.setTipoEmpresa(EnumTipoEmpresa.BY.toString());
//		
//		Boolean validarEmpresa = true;
//		Boolean validarRequest = true;
//		Boolean validarPais = true;
//		Mockito.when(validacionesService.validarRequestNotNull(empresaRequestDTO)).thenReturn(validarRequest);
//		Mockito.when(validacionesService.validarRequestEmpresa(empresaRequestDTO)).thenReturn(validarEmpresa);
//		Mockito.when(validacionesService.validarPais(empresaRequestDTO.getCodigoPais())).thenReturn(validarPais);
//		
//		EmpresaResponseDTO empresaResponseDTO = consultaEmpresaManager.obtenerIdEmpresa(empresaRequestDTO);
//		assertFalse(empresaResponseDTO.isError());
//	}
	
	@Test
	public void consultarIdEmpresaFailRequestNullTest() {
		EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
		
		Boolean validarEmpresa = true;
		Boolean validarRequest = false;
		Boolean validarPais = true;
		Mockito.when(validacionesService.validarRequestNotNull(empresaRequestDTO)).thenReturn(validarRequest);
		Mockito.when(validacionesService.validarRequestEmpresa(empresaRequestDTO)).thenReturn(validarEmpresa);
		Mockito.when(validacionesService.validarPais(empresaRequestDTO.getCodigoPais())).thenReturn(validarPais);
		
		EmpresaResponseDTO empresaResponseDTO = consultaEmpresaManager.obtenerIdEmpresa(empresaRequestDTO);
		assertTrue(empresaResponseDTO.isError());
	}
	
	@Test
	public void consultarIdEmpresaFailRequestEmpresaTest() {
		EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
		
		Boolean validarEmpresa = false;
		Boolean validarRequest = true;
		Boolean validarPais = true;
		Mockito.when(validacionesService.validarRequestNotNull(empresaRequestDTO)).thenReturn(validarRequest);
		Mockito.when(validacionesService.validarRequestEmpresa(empresaRequestDTO)).thenReturn(validarEmpresa);
		Mockito.when(validacionesService.validarPais(empresaRequestDTO.getCodigoPais())).thenReturn(validarPais);
		
		EmpresaResponseDTO empresaResponseDTO = consultaEmpresaManager.obtenerIdEmpresa(empresaRequestDTO);
		assertTrue(empresaResponseDTO.isError());
	}
	
	@Test
	public void consultarIdEmpresaFailPaisTest() {
		EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
		
		Boolean validarEmpresa = true;
		Boolean validarRequest = true;
		Boolean validarPais = false;
		Mockito.when(validacionesService.validarRequestNotNull(empresaRequestDTO)).thenReturn(validarRequest);
		Mockito.when(validacionesService.validarRequestEmpresa(empresaRequestDTO)).thenReturn(validarEmpresa);
		Mockito.when(validacionesService.validarPais(empresaRequestDTO.getCodigoPais())).thenReturn(validarPais);
		
		EmpresaResponseDTO empresaResponseDTO = consultaEmpresaManager.obtenerIdEmpresa(empresaRequestDTO);
		assertTrue(empresaResponseDTO.isError());
	}
	
	@Test
	public void consultarIdEmpresaFailTipoEmpresaTest() {
		EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
		empresaRequestDTO.setTipoEmpresa("NO");
		
		Boolean validarEmpresa = true;
		Boolean validarRequest = true;
		Boolean validarPais = true;
		EmpresaResponseDTO empresaResponseDTO = new EmpresaResponseDTO();
		empresaResponseDTO.setError(Boolean.TRUE);
		Mockito.when(validacionesService.validarRequestNotNull(empresaRequestDTO)).thenReturn(validarRequest);
		Mockito.when(validacionesService.validarRequestEmpresa(empresaRequestDTO)).thenReturn(validarEmpresa);
		Mockito.when(validacionesService.validarPais(empresaRequestDTO.getCodigoPais())).thenReturn(validarPais);
		Mockito.when(ordenCompraService.obtenerIdEmpresa(empresaRequestDTO)).thenReturn(empresaResponseDTO);
		
		
		EmpresaResponseDTO empresaResponseDTO2 = consultaEmpresaManager.obtenerIdEmpresa(empresaRequestDTO);
		assertTrue(empresaResponseDTO2.isError());
	}
}
