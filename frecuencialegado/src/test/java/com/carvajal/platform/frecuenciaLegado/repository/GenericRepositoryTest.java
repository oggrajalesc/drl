package com.carvajal.platform.frecuenciaLegado.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carvajal.platform.frecuencialegado.repository.impl.GenericRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GenericRepositoryTest {

	@Autowired
	private GenericRepository genericRepository;
	
	@Test
	public void getDataSourceCentKTest() {
		genericRepository.getDataSourceCEN("CO");
	}
	
	@Test
	public void getDataSourceOrdersTest() {
		genericRepository.getDataSourceOrders("CO");
	}
}
