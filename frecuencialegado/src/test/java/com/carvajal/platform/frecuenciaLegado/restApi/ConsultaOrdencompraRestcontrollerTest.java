package com.carvajal.platform.frecuenciaLegado.restApi;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.restApi.ConsultaOrdencompraRestcontroller;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsultaOrdencompraRestcontrollerTest {

	@Autowired
	private ConsultaOrdencompraRestcontroller consultaOrdencompraRestcontroller;
	
	@Test
	public void validacionOrdenTest() {
		RequestDTO e1= new RequestDTO();
		e1.setCodigoEanEmpresa("7705326070458");
		e1.setCodigoPais("CO");
		
		List<String> codigoEanPuntoVenta = new ArrayList<>();
		codigoEanPuntoVenta.add("1234566");
		codigoEanPuntoVenta.add("1234567");
		codigoEanPuntoVenta.add("1234568");
		
		e1.setCodigosEanPuntoVenta(codigoEanPuntoVenta);

		consultaOrdencompraRestcontroller.validacionOrden(e1);
	}
}
