package com.carvajal.platform.frecuenciaLegado.repository;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.repository.OrdenCompraRespository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrdenCompraRespositoryTest {

	@Autowired
	private OrdenCompraRespository ordenCompraRespository;
	
	@Test
	public void validarFrecuenciaOrdenOKTest() {
		RequestDTO request = new RequestDTO();
		request.setCodigoPais("CO");
		
		List<String> eans = Arrays.asList("1213E,54545,5488A");
		request.setCodigosEanPuntoVenta(eans);
		
		ordenCompraRespository.validarFrecuenciaOrden("1233", request );
	}
}
