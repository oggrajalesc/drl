package com.carvajal.platform.frecuenciaLegado.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carvajal.platform.frecuencialegado.EnumTipoEmpresa;
import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.service.ValidacionesService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidacionesServiceImplTest {

	@Autowired
	private ValidacionesService validacionesService;
	
    @Before
    public void init() {
    }
    
    @Test
    public void validarDatosOK() {
    	RequestDTO empresa  = new RequestDTO();
    	empresa.setCodigoEanEmpresa("778955");
    	empresa.setCodigoPais("CO");
    	
		List<String> codigoEanPuntoVenta = new ArrayList<>();
		codigoEanPuntoVenta.add("1234566");
		codigoEanPuntoVenta.add("1234567");
		codigoEanPuntoVenta.add("1234568");	    	
    	empresa.setCodigosEanPuntoVenta(codigoEanPuntoVenta);
    	boolean respuesta = validacionesService.validarDatos(empresa);
    	
    	assertTrue(respuesta);
    }
    
    @Test
    public void validarDatosPaisFail() {
    	RequestDTO empresa  = new RequestDTO();
    	empresa.setCodigoEanEmpresa("778955");
    	empresa.setCodigoPais(null);
    	
		List<String> codigoEanPuntoVenta = new ArrayList<>();
		codigoEanPuntoVenta.add("1234566");
		codigoEanPuntoVenta.add("1234567");
		codigoEanPuntoVenta.add("1234568");	    	
    	empresa.setCodigosEanPuntoVenta(codigoEanPuntoVenta);
    	boolean respuesta = validacionesService.validarDatos(empresa);
    	
    	assertFalse(respuesta);
    }
    
    @Test
    public void validarDatosEanFail() {
    	RequestDTO empresa  = new RequestDTO();
    	empresa.setCodigoEanEmpresa("778955");
    	empresa.setCodigoPais("CO");
    	
		List<String> codigoEanPuntoVenta = new ArrayList<>();
    	empresa.setCodigosEanPuntoVenta(codigoEanPuntoVenta);
    	boolean respuesta = validacionesService.validarDatos(empresa);
    	
    	assertFalse(respuesta);
    }
    
    @Test
    public void validarPaisOK() {
    	RequestDTO request  = new RequestDTO();
    	request.setCodigoEanEmpresa("778955");
    	request.setCodigoPais("CO");
    	
		List<String> codigoEanPuntoVenta = new ArrayList<>();
		codigoEanPuntoVenta.add("1234566");
		codigoEanPuntoVenta.add("1234567");
		codigoEanPuntoVenta.add("1234568");	    	
		request.setCodigosEanPuntoVenta(codigoEanPuntoVenta);
    	
    	boolean respuesta = validacionesService.validarPais(request.getCodigoPais());
    	
    	assertTrue(respuesta);
    }
    
    @Test
    public void validarPaisFail() {
    	RequestDTO request  = new RequestDTO();
    	request.setCodigoEanEmpresa("778955");
    	request.setCodigoPais("NO");
    	
		List<String> codigoEanPuntoVenta = new ArrayList<>();
		codigoEanPuntoVenta.add("1234566");
		codigoEanPuntoVenta.add("1234567");
		codigoEanPuntoVenta.add("1234568");	    	
		request.setCodigosEanPuntoVenta(codigoEanPuntoVenta);
    	
    	boolean respuesta = validacionesService.validarPais(request.getCodigoPais());
    	
    	assertFalse(respuesta);
    }
    
    @Test
    public void validarRequestNotNull() {
    	Object object = new Object();
    	
    	boolean respuesta = validacionesService.validarRequestNotNull(object);
    	
    	assertTrue(respuesta);
    }
    
    @Test
    public void validarRequestNullTest() {
    	Object object = null;
    	
    	boolean respuesta = validacionesService.validarRequestNotNull(object);
    	
    	assertFalse(respuesta);
    }
    
    @Test
    public void validarRequestEmpresaOKTest() {
    	EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
    	empresaRequestDTO.setCodigoEAN("aaaa");
    	empresaRequestDTO.setCodigoPais("CO");
    	empresaRequestDTO.setTipoEmpresa(EnumTipoEmpresa.SU.toString());
    	
    	boolean respuesta = validacionesService.validarRequestEmpresa(empresaRequestDTO);
    	
    	assertTrue(respuesta);
    }
    
    @Test
    public void validarRequestEmpresaFailEanTest() {
    	EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
    	empresaRequestDTO.setCodigoEAN(null);
    	empresaRequestDTO.setCodigoPais("CO");
    	empresaRequestDTO.setTipoEmpresa(EnumTipoEmpresa.SU.toString());
    	
    	boolean respuesta = validacionesService.validarRequestEmpresa(empresaRequestDTO);
    	
    	assertFalse(respuesta);
    }
    
    @Test
    public void validarRequestEmpresaFailPaisTest() {
    	EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
    	empresaRequestDTO.setCodigoEAN("aaaa");
    	empresaRequestDTO.setCodigoPais(null);
    	empresaRequestDTO.setTipoEmpresa(EnumTipoEmpresa.SU.toString());
    	
    	boolean respuesta = validacionesService.validarRequestEmpresa(empresaRequestDTO);
    	
    	assertFalse(respuesta);
    }
    
    @Test
    public void validarRequestEmpresaFailTipoEmpresaTest() {
    	EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
    	empresaRequestDTO.setCodigoEAN("aaaa");
    	empresaRequestDTO.setCodigoPais("CO");
    	empresaRequestDTO.setTipoEmpresa(null);
    	
    	boolean respuesta = validacionesService.validarRequestEmpresa(empresaRequestDTO);
    	
    	assertFalse(respuesta);
    }
}
