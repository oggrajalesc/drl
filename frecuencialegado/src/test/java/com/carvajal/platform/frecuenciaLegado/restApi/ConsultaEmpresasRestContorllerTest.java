package com.carvajal.platform.frecuenciaLegado.restApi;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.EmpresaResponseDTO;
import com.carvajal.platform.frecuencialegado.restApi.ConsultaEmpresasRestController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsultaEmpresasRestContorllerTest {

	@Autowired
	private ConsultaEmpresasRestController consultaEmpresasRestContorller;
	
	@Test
	public void obtenerIdEmpresa() {
		EmpresaRequestDTO empresaRequestDTO = new EmpresaRequestDTO();
		empresaRequestDTO.setCodigoEAN("AABBCC");
		empresaRequestDTO.setCodigoPais("CO");
		empresaRequestDTO.setTipoEmpresa("SU");
		
		EmpresaResponseDTO empresaResponseDTO = consultaEmpresasRestContorller.obtenerIdEmpresa(empresaRequestDTO);
		assertNotNull(empresaResponseDTO);
		
	}
}
