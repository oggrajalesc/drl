package com.carvajal.platform.frecuenciaLegado.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.carvajal.platform.frecuencialegado.EnumErrores;
import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.dto.RespuestaDTO;
import com.carvajal.platform.frecuencialegado.exception.LegadoException;
import com.carvajal.platform.frecuencialegado.repository.OrdenCompraRespository;
import com.carvajal.platform.frecuencialegado.repository.EmpresaRespository;
import com.carvajal.platform.frecuencialegado.service.OrdenCompraService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrdenCompraServiceTest {

	@Autowired
	private OrdenCompraService ordenCompraService;

	@Mock
	private OrdenCompraRespository ordenCompraRespository;
	
	@Mock
	private EmpresaRespository sucursalEmpresaRespository;	

	@Before
	public void init() {
		ReflectionTestUtils.setField(ordenCompraService, "ordenCompraRespository", ordenCompraRespository);
		ReflectionTestUtils.setField(ordenCompraService, "sucursalEmpresaRespository", sucursalEmpresaRespository);
	}
	
	@Test
	public void validarFrecuenciaOKTest(){
		RequestDTO e1= new RequestDTO();
		e1.setCodigoEanEmpresa("7705326070458");
		e1.setCodigoPais("CO");
		
		List<String> codigoEanPuntoVenta = new ArrayList<>();
		codigoEanPuntoVenta.add("1234566");
		codigoEanPuntoVenta.add("1234567");
		codigoEanPuntoVenta.add("1234568");		

		e1.setCodigosEanPuntoVenta(codigoEanPuntoVenta);
		List<String> eanconOrden = new ArrayList<>();
		eanconOrden.add("1234566");		
		
		String idEmpresa = "1020";
		
		try {
			Mockito.when(sucursalEmpresaRespository.obtenerIdEmpresaSUPorCodigoEAN(e1.getCodigoPais(),e1.getCodigoEanEmpresa())).thenReturn(idEmpresa);
			Mockito.when(ordenCompraRespository.validarFrecuenciaOrden(idEmpresa, e1)).thenReturn(eanconOrden);		
			
			RespuestaDTO respuesta = ordenCompraService.validarFrecuencia(e1);
			
			assertEquals(respuesta.getCodigosEan().size(), 1);
		} catch (LegadoException e) {
		}		

	}
	
	@Test
	public void validarFrecuenciaFailEmpresaTest(){
		RequestDTO e1= new RequestDTO();
		e1.setCodigoEanEmpresa("7705326070458");
		e1.setCodigoPais("CO");
		
		List<String> codigoEanPuntoVenta = new ArrayList<>();
		codigoEanPuntoVenta.add("1234566");
		codigoEanPuntoVenta.add("1234567");
		codigoEanPuntoVenta.add("1234568");		

		e1.setCodigosEanPuntoVenta(codigoEanPuntoVenta);
		List<String> eanconOrden = new ArrayList<>();
		eanconOrden.add("1234566");		
		
		String idEmpresa = null;

		StringBuilder eans = new StringBuilder();
		codigoEanPuntoVenta.forEach(puntoVeta ->{
			eans.append(puntoVeta).append(",");			
		});		
		eans.append("''");
		
		try {
			Mockito.when(sucursalEmpresaRespository.obtenerIdEmpresaSUPorCodigoEAN(e1.getCodigoPais(),e1.getCodigoEanEmpresa())).thenReturn(idEmpresa);
			Mockito.when(ordenCompraRespository.validarFrecuenciaOrden(idEmpresa, e1)).thenReturn(eanconOrden);		
			
			RespuestaDTO respuesta = ordenCompraService.validarFrecuencia(e1);
			
			assertEquals(respuesta.getMensaje(), EnumErrores.EAN.toString());
		} catch (LegadoException e) {

		}		

	}
	
	@Test
	public void validarFrecuenciaFailPaisTest(){
		RequestDTO e1= new RequestDTO();
		e1.setCodigoEanEmpresa("7705326070458");
		e1.setCodigoPais("NO");
		
		List<String> codigoEanPuntoVenta = new ArrayList<>();
		codigoEanPuntoVenta.add("1234566");
		codigoEanPuntoVenta.add("1234567");
		codigoEanPuntoVenta.add("1234568");		

		e1.setCodigosEanPuntoVenta(codigoEanPuntoVenta);		
		List<String> eanconOrden = new ArrayList<>();
		eanconOrden.add("1234566");		
		
		String idEmpresa = "123456";

		StringBuilder eans = new StringBuilder();
		codigoEanPuntoVenta.forEach(puntoVeta ->{
			eans.append(puntoVeta).append(",");			
		});		
		eans.append("''");
		
		try {
			Mockito.when(sucursalEmpresaRespository.obtenerIdEmpresaSUPorCodigoEAN(e1.getCodigoPais(),e1.getCodigoEanEmpresa())).thenReturn(idEmpresa);
			Mockito.when(ordenCompraRespository.validarFrecuenciaOrden(idEmpresa, e1)).thenReturn(eanconOrden);		
			RespuestaDTO respuesta = ordenCompraService.validarFrecuencia(e1);
			assertEquals(respuesta.getMensaje(), EnumErrores.PAIS.toString());
		} catch (LegadoException e) {

		}		

	}
	
//	@Test
//	public void validarFrecuenciaFailDatoNuloTest(){
//		List<RequestDTO> empresas = new ArrayList<>();
//		RequestDTO e1 = new RequestDTO();
//		e1.setCodigoEan(null);
//		e1.setCodigoPais("SE");
//		empresas.add(e1);
//		
//		List<RespuestaDTO> respuestas = ordenCompraService.validarFrecuencia(empresas);
//		assertEquals(respuestas.get(0).getError(), EnumErrores.NULO.toString());
//		assertEquals(respuestas.get(0).isRespuesta(), false);
//	}
	
}
