package com.carvajal.platform.frecuenciaLegado.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.exception.LegadoException;
import com.carvajal.platform.frecuencialegado.repository.EmpresaRespository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SucursalEmpresaRespositoryTest {

	@Autowired
	private EmpresaRespository sucursalEmpresaRespository;
	
	@Test
	public void obtenerIdempresaPorCodigoEANOKTest() {
		RequestDTO requestDTO = new RequestDTO();
		requestDTO.setCodigoEanEmpresa("7789736");
		requestDTO.setCodigoPais("CO");
		try {
			sucursalEmpresaRespository.obtenerIdEmpresaSUPorCodigoEAN(requestDTO.getCodigoPais(),requestDTO.getCodigoEanEmpresa());
		} catch (LegadoException e) {
			e.getMessage();
		}
	}
}
