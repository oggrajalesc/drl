package com.carvajal.platform.frecuencialegado.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


@Configuration
public class DataBaseConfiguration {
	
	@Autowired
	private Environment environment;		

	@Bean(name = "v1DataSource")
	public DataSource getDataSourceV1() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getProperty("carvajalco-v1.datasource.driveClassName"));
		dataSource.setUsername(environment.getProperty("carvajalco-v1.datasource.username"));
		dataSource.setPassword(environment.getProperty("carvajalco-v1.datasource.password"));
		return dataSource;
	}	
	
	@Bean(name = "v3DataSource")
	public DataSource getDataSourceV3() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getProperty("carvajalco-v3.datasource.driveClassName"));
		dataSource.setUsername(environment.getProperty("carvajalco-v3.datasource.username"));
		dataSource.setPassword(environment.getProperty("carvajalco-v3.datasource.password"));
		return dataSource;
	}	
}
