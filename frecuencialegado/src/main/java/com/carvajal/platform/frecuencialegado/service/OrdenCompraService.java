package com.carvajal.platform.frecuencialegado.service;

import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.EmpresaResponseDTO;
import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.dto.RespuestaDTO;

public interface OrdenCompraService {
	RespuestaDTO validarFrecuencia(RequestDTO requestDTO);

	EmpresaResponseDTO obtenerIdEmpresa(EmpresaRequestDTO empresaRequestDTO);
}
