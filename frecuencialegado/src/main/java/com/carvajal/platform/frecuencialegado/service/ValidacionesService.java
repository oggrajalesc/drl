package com.carvajal.platform.frecuencialegado.service;

import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.RequestDTO;

public interface ValidacionesService {
	boolean validarDatos(RequestDTO empresa);
	
	boolean validarPais(String pais);

	boolean validarRequestEmpresa(EmpresaRequestDTO empresaRequestDTO);

	boolean validarRequestNotNull(Object request);
}
