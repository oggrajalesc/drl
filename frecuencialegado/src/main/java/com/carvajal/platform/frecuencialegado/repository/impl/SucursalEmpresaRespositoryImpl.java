package com.carvajal.platform.frecuencialegado.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.carvajal.platform.frecuencialegado.EnumErrores;
import com.carvajal.platform.frecuencialegado.exception.LegadoException;
import com.carvajal.platform.frecuencialegado.repository.EmpresaRespository;

@Repository
public class SucursalEmpresaRespositoryImpl extends GenericRepository implements EmpresaRespository {
	private static Logger logger = LoggerFactory.getLogger(SucursalEmpresaRespositoryImpl.class);

	/**
	 * @see Obtiene el id empresa SU con el código ean
	 * @param codigoPais
	 * @param codigoEan
	 */
	public String obtenerIdEmpresaSUPorCodigoEAN(String codigoPais, String codigoEan) throws LegadoException{
		DataSource dataSource = getDataSourceCEN(codigoPais);
		logger.info("INICIO. OBTENER ID EMPRESA");
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		String selectSQL = "SELECT top(1) ID_EMPRESA FROM sucursal_empresa with(nolock) WHERE ean_sucursal_empresa_nad_su = ?";
		String result = null;

		try {
			dbConnection = 	dataSource.getConnection();
			preparedStatement = dbConnection.prepareStatement(selectSQL);
			preparedStatement.setString(1, codigoEan);

			rs = preparedStatement.executeQuery();

			if (rs.next()) {
				result = rs.getString("ID_EMPRESA");
				logger.info("RESULTADO:{}",result);
			}
			logger.info("FIN. OBTENER ID EMPRESA");
		} catch (SQLException e) {
			logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
			throw new LegadoException(EnumErrores.BD.toString());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
				}
			}			
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
				}
			}
			if(dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
				}
			}
		}
		
		return result;
	}

	/**
	 * @see Obtiene el id empresa BY con el código ean
	 * @param codigoPais
	 * @param codigoEan
	 */
	public String obtenerIdEmpresaBYPorCodigoEAN(String codigoPais, String codigoEAN) throws LegadoException {
		DataSource dataSource = getDataSourceCEN(codigoPais);
		logger.info("INICIO. OBTENER ID EMPRESA");
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		String selectSQL = "SELECT top(1) ID_EMPRESA FROM empresa with(nolock) WHERE ean_compradora_by = ?";
		String result = null;

		try {
			dbConnection = 	dataSource.getConnection();
			preparedStatement = dbConnection.prepareStatement(selectSQL);
			preparedStatement.setString(1, codigoEAN);

			rs = preparedStatement.executeQuery();

			if (rs.next()) {
				result = rs.getString("ID_EMPRESA");
				logger.info("RESULTADO:{}",result);
			}
			logger.info("FIN. OBTENER ID EMPRESA");
		} catch (SQLException e) {
			logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
			throw new LegadoException(EnumErrores.BD.toString());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
				}
			}			
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
				}
			}
			if(dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
				}
			}
		}
		
		return result;
	}

}
