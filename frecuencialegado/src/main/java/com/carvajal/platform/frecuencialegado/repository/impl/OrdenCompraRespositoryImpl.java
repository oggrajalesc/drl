package com.carvajal.platform.frecuencialegado.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.carvajal.platform.frecuencialegado.EnumErrores;
import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.repository.OrdenCompraRespository;

@Repository
public class OrdenCompraRespositoryImpl extends GenericRepository implements OrdenCompraRespository{
	private static Logger logger = LoggerFactory.getLogger(OrdenCompraRespositoryImpl.class);
	
	/**
	 * @see Consulta los puntos de venta que han radicado orde de compra
	 * @param idEmpresa
	 * @param request
	 * @return 
	 */
	@Override
	public List<String> validarFrecuenciaOrden(String idEmpresa,RequestDTO request) {
		DataSource dataSource = getDataSourceOrders(request.getCodigoPais());
		logger.info("INICIO. VALIDAR EXISTE ORDEN");
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		int i = 4;		
		
		List<String> eansConOrden = new ArrayList<>();
		
		StringBuilder eansQuery = new StringBuilder("SELECT NAD_SN_PUNTOVENTA,1 FROM ORDEN_COMPRA with(nolock) WHERE id_empresa = ? AND dtm_fecha_documento between ? and ? and NAD_SN_PUNTOVENTA in (");
		request.getCodigosEanPuntoVenta().forEach(d ->{
			eansQuery.append("?,");
		});
		eansQuery.append("?");
		eansQuery.append(") group by NAD_SN_PUNTOVENTA");
		
		String selectSQL = eansQuery.toString();
		
		try {
			dbConnection = 	dataSource.getConnection();
			preparedStatement = dbConnection.prepareStatement(selectSQL);
			preparedStatement.setString(1, idEmpresa);
			preparedStatement.setTimestamp (2, new java.sql.Timestamp(getCurrentDatetimeDiaInicio().getTime()));
			preparedStatement.setTimestamp(3, new java.sql.Timestamp(getCurrentDatetimeDiaFin().getTime()));

			for(String ean:request.getCodigosEanPuntoVenta()) {
				preparedStatement.setString(i++, ean);
			}
			preparedStatement.setString(i++, "");
			
			rs = preparedStatement.executeQuery();

			while (rs.next()) {
				eansConOrden.add(rs.getString("NAD_SN_PUNTOVENTA"));
				logger.info("RESULTADO:{}",rs.getString("NAD_SN_PUNTOVENTA"));
			}
			logger.info("FIN. VALIDAR EXISTE ORDEN-");
		} catch (SQLException e) {
			logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
				}
			}
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
				}
			}
			if(dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					logger.error(EnumErrores.LOGGER.toString(),e.getMessage());
				}
			}			
		}
		
		return eansConOrden;
	}
	
	private java.util.Date getCurrentDatetimeDiaInicio() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}	
	
	private java.util.Date getCurrentDatetimeDiaFin() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}	

}
