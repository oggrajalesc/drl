package com.carvajal.platform.frecuencialegado.dto;

import java.io.Serializable;

/**
 * 
 * @author Dsarmientoa
 *
 */
public class EmpresaResponseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4649489958514984526L;

	private String codigoEAN;
	private String codigoPais;
	private String tipoEmpresa;
	private String idEmpresa;
	private boolean error;
	
	public String getCodigoEAN() {
		return codigoEAN;
	}
	public void setCodigoEAN(String codigoEAN) {
		this.codigoEAN = codigoEAN;
	}
	public String getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	public String getTipoEmpresa() {
		return tipoEmpresa;
	}
	public void setTipoEmpresa(String tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}
	public String getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	@Override
	public String toString() {
		return "EmpresaResponseDTO [codigoEAN=" + codigoEAN + ", codigoPAIS=" + codigoPais + ", tipoEmpresa="
				+ tipoEmpresa + ", idEmpresa=" + idEmpresa + ", error=" + error + "]";
	}
	
}
