package com.carvajal.platform.frecuencialegado.repository;

import java.util.List;

import com.carvajal.platform.frecuencialegado.dto.RequestDTO;

public interface OrdenCompraRespository {
	List<String> validarFrecuenciaOrden(String idEmpresa,RequestDTO request);
}
