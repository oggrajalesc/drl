package com.carvajal.platform.frecuencialegado.restApi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.platform.frecuencialegado.core.ConsultaEmpresaManager;
import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.EmpresaResponseDTO;

@RestController
public class ConsultaEmpresasRestController {
	private static Logger logger = LoggerFactory.getLogger(ConsultaEmpresasRestController.class);
	
	@Autowired
	private ConsultaEmpresaManager consultaEmpresaManager;

	/**
	 * @see api encargado de hallar el id de la empresa 
	 * @param empresaRequestDTO
	 * @return
	 */
	@PostMapping("/obtenerIdEmpresa")	
	public EmpresaResponseDTO obtenerIdEmpresa(@RequestBody EmpresaRequestDTO empresaRequestDTO) {
		logger.info("parametros: {}",empresaRequestDTO);
		EmpresaResponseDTO empresaResponseDTO = consultaEmpresaManager.obtenerIdEmpresa(empresaRequestDTO);
		logger.info("Resultado: {} ", empresaResponseDTO);
		return empresaResponseDTO;
	}

}
