package com.carvajal.platform.frecuencialegado.dto;

import java.io.Serializable;
import java.util.List;

public class RequestDTO implements Serializable{
	
	private static final long serialVersionUID = -6925284722161211979L;
	
	
	private String codigoEanEmpresa;
	private List<String> codigosPuntoVenta;
	private String codigoPais;
	
	public String getCodigoEanEmpresa() {
		return codigoEanEmpresa;
	}
	public void setCodigoEanEmpresa(String codigoEanEmpresa) {
		this.codigoEanEmpresa = codigoEanEmpresa;
	}
	public List<String> getCodigosEanPuntoVenta() {
		return codigosPuntoVenta;
	}
	public void setCodigosEanPuntoVenta(List<String> codigosEanPuntoVenta) {
		this.codigosPuntoVenta = codigosEanPuntoVenta;
	}
	public String getCodigoPais() {
		return codigoPais;
	}
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	@Override
	public String toString() {
		return "EmpresaDTO [codigoEanEmpresa=" + codigoEanEmpresa + ", codigoEanPuntoVenta=" + codigosPuntoVenta
				+ ", codigoPais=" + codigoPais + "]";
	}
	
	
}
