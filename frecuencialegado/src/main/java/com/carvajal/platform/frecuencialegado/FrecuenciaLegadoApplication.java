package com.carvajal.platform.frecuencialegado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class})
@ComponentScan
public class FrecuenciaLegadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrecuenciaLegadoApplication.class, args);
	}
}
