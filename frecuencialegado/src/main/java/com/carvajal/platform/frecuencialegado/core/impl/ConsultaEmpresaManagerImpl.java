package com.carvajal.platform.frecuencialegado.core.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carvajal.platform.frecuencialegado.core.ConsultaEmpresaManager;
import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.EmpresaResponseDTO;
import com.carvajal.platform.frecuencialegado.service.OrdenCompraService;
import com.carvajal.platform.frecuencialegado.service.ValidacionesService;

@Component
public class ConsultaEmpresaManagerImpl implements ConsultaEmpresaManager{
	private static Logger logger = LoggerFactory.getLogger(ConsultaEmpresaManagerImpl.class);
	
	@Autowired
	private ValidacionesService validacionesService;

	@Autowired
	private OrdenCompraService ordenCompraService;

	@Override
	public EmpresaResponseDTO obtenerIdEmpresa(EmpresaRequestDTO empresaRequestDTO) {
		logger.info("parametros: {}",empresaRequestDTO);
		EmpresaResponseDTO empresaResponseDTO = new EmpresaResponseDTO();

		if(validacionesService.validarRequestNotNull(empresaRequestDTO) && validacionesService.validarRequestEmpresa(empresaRequestDTO) && validacionesService.validarPais(empresaRequestDTO.getCodigoPais())) {
			empresaResponseDTO  = ordenCompraService.obtenerIdEmpresa(empresaRequestDTO);
		}else {
			empresaResponseDTO.setError(Boolean.TRUE);
		}
		empresaResponseDTO.setCodigoEAN(empresaRequestDTO.getCodigoEAN());
		empresaResponseDTO.setCodigoPais(empresaRequestDTO.getCodigoPais());
		empresaResponseDTO.setTipoEmpresa(empresaRequestDTO.getTipoEmpresa());		
		logger.info("respueta: {}",empresaResponseDTO);
		return empresaResponseDTO;
	}
	

}
