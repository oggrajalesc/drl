package com.carvajal.platform.frecuencialegado.service.impl;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carvajal.platform.frecuencialegado.EnumErrores;
import com.carvajal.platform.frecuencialegado.EnumTipoEmpresa;
import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.EmpresaResponseDTO;
import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.dto.RespuestaDTO;
import com.carvajal.platform.frecuencialegado.exception.LegadoException;
import com.carvajal.platform.frecuencialegado.repository.EmpresaRespository;
import com.carvajal.platform.frecuencialegado.repository.OrdenCompraRespository;
import com.carvajal.platform.frecuencialegado.service.OrdenCompraService;
import com.carvajal.platform.frecuencialegado.service.ValidacionesService;

@Service
public class OrdenCompraServiceImpl implements OrdenCompraService{
	private static Logger logger = LoggerFactory.getLogger(OrdenCompraServiceImpl.class);
	
	@Autowired
	private OrdenCompraRespository ordenCompraRespository;
	
	@Autowired
	private EmpresaRespository sucursalEmpresaRespository;
	
	@Autowired
	private ValidacionesService validacionesService;

	@Autowired
	private EmpresaRespository empresaRespository;
	
	@Transactional(readOnly = true)
	public RespuestaDTO validarFrecuencia(RequestDTO requestDTO) {
		logger.info("INICIA VALIDACION {}",requestDTO);
		RespuestaDTO respuesta = new RespuestaDTO();
		if(validacionesService.validarDatos(requestDTO)) {
			if(validacionesService.validarPais(requestDTO.getCodigoPais())) {
				
				String idEmpresa = null;
				try {
					idEmpresa = sucursalEmpresaRespository.obtenerIdEmpresaSUPorCodigoEAN(requestDTO.getCodigoPais(),requestDTO.getCodigoEanEmpresa());
					if(!Objects.isNull(idEmpresa)) {
						StringBuilder eans = new StringBuilder();
						requestDTO.getCodigosEanPuntoVenta().forEach(puntoVeta ->{
							eans.append(puntoVeta).append(",");			
						});
						eans.append("''");
						List<String> codigosEan = ordenCompraRespository.validarFrecuenciaOrden(idEmpresa, requestDTO);
						respuesta.setCodigosEan(codigosEan);
						respuesta.setProcesoTerminado(true);
					}else{
						respuesta.setCodigosEan(null);
						respuesta.setMensaje(EnumErrores.EAN.toString());
						respuesta.setProcesoTerminado(false);
					}
				} catch (LegadoException e) {
					respuesta.setCodigosEan(null);
					respuesta.setMensaje(e.getMessage());
					respuesta.setProcesoTerminado(false);
				}
			}else {
				respuesta.setCodigosEan(null);
				respuesta.setMensaje(EnumErrores.PAIS.toString());
				respuesta.setProcesoTerminado(false);
			}
		}else {
			respuesta.setCodigosEan(null);
			respuesta.setMensaje(EnumErrores.NULO.toString());
			respuesta.setProcesoTerminado(false);
		}
		
		logger.info("FIN VALIDACION {}",respuesta);
		return respuesta;
	}

	@Override
	public EmpresaResponseDTO obtenerIdEmpresa(EmpresaRequestDTO empresaRequestDTO) {
		EmpresaResponseDTO empresaResponseDTO = new EmpresaResponseDTO();

		if(EnumTipoEmpresa.SU.toString().equals(empresaRequestDTO.getTipoEmpresa())) {
			try {
				String idEmpresa = empresaRespository.obtenerIdEmpresaSUPorCodigoEAN(empresaRequestDTO.getCodigoPais(),empresaRequestDTO.getCodigoEAN());
				if(Objects.isNull(idEmpresa)) {
					empresaResponseDTO.setError(Boolean.TRUE);
					empresaResponseDTO.setIdEmpresa(null);
					logger.info("ERROR: {}","EAN NO ENCONTRADO");
				}else {
					empresaResponseDTO.setError(Boolean.FALSE);
					empresaResponseDTO.setIdEmpresa(idEmpresa);
					logger.info("OK: {}",idEmpresa);
				}
			} catch (LegadoException e) {
				empresaResponseDTO.setError(Boolean.TRUE);
				logger.info("ERROR: {}",e.getMessage());
			}
		}else if(EnumTipoEmpresa.BY.toString().equals(empresaRequestDTO.getTipoEmpresa())) {
			try {
				String idEmpresa = empresaRespository.obtenerIdEmpresaBYPorCodigoEAN(empresaRequestDTO.getCodigoPais(),empresaRequestDTO.getCodigoEAN());
				if(Objects.isNull(idEmpresa)) {
					empresaResponseDTO.setError(Boolean.TRUE);
					empresaResponseDTO.setIdEmpresa(null);
					logger.info("ERROR: {}","EAN NO ENCONTRADO");
				}else {
					empresaResponseDTO.setError(Boolean.FALSE);
					empresaResponseDTO.setIdEmpresa(idEmpresa);
					logger.info("OK: {}",idEmpresa);
				}
			} catch (LegadoException e) {
				empresaResponseDTO.setError(Boolean.TRUE);
				logger.info("ERROR: {}",e.getMessage());
			}
			
			empresaResponseDTO.setError(Boolean.FALSE);
		}else {
			empresaResponseDTO.setError(Boolean.TRUE);
		}		
		return empresaResponseDTO;
	}

}
