package com.carvajal.platform.frecuencialegado.restApi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.dto.RespuestaDTO;
import com.carvajal.platform.frecuencialegado.service.OrdenCompraService;

@RestController
public class ConsultaOrdencompraRestcontroller {
	private static Logger logger = LoggerFactory.getLogger(ConsultaOrdencompraRestcontroller.class);
	
	@Autowired
	private OrdenCompraService ordenCompraService;

	@PostMapping("/validacionOrden")
    public RespuestaDTO validacionOrden(@RequestBody  RequestDTO requestDTO) {
		logger.info("parametros: {}",requestDTO);

		RespuestaDTO respuesta =  ordenCompraService.validarFrecuencia(requestDTO);
    	logger.info("resulado: {}",respuesta);
    	return respuesta;
    }

}
