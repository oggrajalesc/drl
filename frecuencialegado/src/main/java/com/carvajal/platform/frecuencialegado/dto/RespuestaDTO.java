package com.carvajal.platform.frecuencialegado.dto;

import java.io.Serializable;
import java.util.List;


public class RespuestaDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private List<String> codigosEan;
	private String mensaje; 
	private Boolean procesoTerminado; 
	
	public List<String> getCodigosEan() {
		return codigosEan;
	}
	public void setCodigosEan(List<String> codigosEan) {
		this.codigosEan = codigosEan;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Boolean getProcesoTerminado() {
		return procesoTerminado;
	}
	public void setProcesoTerminado(Boolean procesoTerminado) {
		this.procesoTerminado = procesoTerminado;
	}
	@Override
	public String toString() {
		return "RespuestaDTO [codigosEan=" + codigosEan + ", mensaje=" + mensaje + ", procesoTerminado="
				+ procesoTerminado + "]";
	}

	
	
}
