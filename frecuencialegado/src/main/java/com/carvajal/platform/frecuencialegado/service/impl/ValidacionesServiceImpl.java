package com.carvajal.platform.frecuencialegado.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.RequestDTO;
import com.carvajal.platform.frecuencialegado.service.ValidacionesService;

@Service
public class ValidacionesServiceImpl implements ValidacionesService{
	
	@Autowired
	private Environment environment;
	
	/**
	 * @see validar la información del request
	 */
	public boolean validarDatos(RequestDTO request) {
		boolean respuesta = true;
		if(Objects.isNull(request.getCodigoEanEmpresa())) {
			respuesta = false;
		}
		if(Objects.isNull(request.getCodigosEanPuntoVenta()) || request.getCodigosEanPuntoVenta().isEmpty()) {
			respuesta = false;
		}
		if(Objects.isNull(request.getCodigoPais())) {
			respuesta = false;
		}
		return respuesta;
	}

	/**
	 * @see validar Pais
	 */
	public boolean validarPais(String pais) {
		boolean response = true;
		String[] paises = environment.getProperty("app.config.paises", String[].class);
		List<String> paises2 = Arrays.asList(paises);
		if(!paises2.contains(pais)) {
			response = false;
		}
		return response;
	}

	/**
	 * @see validar datos not null de request
	 * @param empresaRequestDTO
	 */
	public boolean validarRequestEmpresa(EmpresaRequestDTO empresaRequestDTO) {
		boolean response = true;
		if(Objects.isNull(empresaRequestDTO.getCodigoEAN())) {
			response = false;
		}
		if(Objects.isNull(empresaRequestDTO.getCodigoPais())) {
			response = false;
		}
		if(Objects.isNull(empresaRequestDTO.getTipoEmpresa())) {
			response = false;
		}
		return response;
	}

	/**
	 * @see validar objectos null
	 * @param empresaRequestDTO
	 */
	public boolean validarRequestNotNull(Object request) {
		if(Objects.isNull(request)) {
			return false;
			
		}
		return true;
	}

}
