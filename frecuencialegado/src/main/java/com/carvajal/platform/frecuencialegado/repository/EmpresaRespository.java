package com.carvajal.platform.frecuencialegado.repository;

import com.carvajal.platform.frecuencialegado.exception.LegadoException;

public interface EmpresaRespository {
	String obtenerIdEmpresaSUPorCodigoEAN(String codigoPais, String codigoEan) throws LegadoException;

	String obtenerIdEmpresaBYPorCodigoEAN(String codigoPAIS, String codigoEAN) throws LegadoException;
}
