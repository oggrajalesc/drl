package com.carvajal.platform.frecuencialegado.dto;

import java.io.Serializable;

/**
 * 
 * @author Dsarmientoa
 *
 */
public class EmpresaRequestDTO implements Serializable{
	private static final long serialVersionUID = 4824809246176800896L;
	
	private String codigoEAN;
	private String codigoPais;
	private String tipoEmpresa;
	
	public String getCodigoEAN() {
		return codigoEAN;
	}
	public void setCodigoEAN(String codigoEAN) {
		this.codigoEAN = codigoEAN;
	}

	public String getCodigoPais() {
		return codigoPais;
	}
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	public String getTipoEmpresa() {
		return tipoEmpresa;
	}
	public void setTipoEmpresa(String tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}
	@Override
	public String toString() {
		return "EmpresaRequestDTO [codigoEAN=" + codigoEAN + ", codigoPais=" + codigoPais + ", tipoEmpresa="
				+ tipoEmpresa + "]";
	}

}
