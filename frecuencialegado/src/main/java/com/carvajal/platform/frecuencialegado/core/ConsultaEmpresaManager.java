package com.carvajal.platform.frecuencialegado.core;

import com.carvajal.platform.frecuencialegado.dto.EmpresaRequestDTO;
import com.carvajal.platform.frecuencialegado.dto.EmpresaResponseDTO;

public interface ConsultaEmpresaManager {

	EmpresaResponseDTO obtenerIdEmpresa(EmpresaRequestDTO empresaRequestDTO);

}
