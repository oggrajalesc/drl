package com.carvajal.platform.frecuencialegado.repository.impl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

@Repository
public class GenericRepository {
	
	@Autowired
	private Environment environment;
	
	public DataSource getDataSourceOrders(String pais) { //orders
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(environment.getProperty("carvajalco-v1.datasource.url").replace("CNX", pais));
		dataSource.setDriverClassName(environment.getProperty("carvajalco-v1.datasource.driveClassName"));
		dataSource.setUsername(environment.getProperty("carvajalco-v1.datasource.username"));
		dataSource.setPassword(environment.getProperty("carvajalco-v1.datasource.password"));
		return dataSource;
	}
	
	public DataSource getDataSourceCEN(String pais) {  
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(environment.getProperty("carvajalco-v3.datasource.url").replace("CNX", pais));
		dataSource.setDriverClassName(environment.getProperty("carvajalco-v3.datasource.driveClassName").replace("CNX", pais));
		dataSource.setUsername(environment.getProperty("carvajalco-v3.datasource.username"));
		dataSource.setPassword(environment.getProperty("carvajalco-v3.datasource.password"));
		return dataSource;
	}	
}
