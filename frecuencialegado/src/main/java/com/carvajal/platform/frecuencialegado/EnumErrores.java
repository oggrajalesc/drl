package com.carvajal.platform.frecuencialegado;

public enum EnumErrores {
    ORDEN("NO SE HA REGISTRADO ORDEN DE COMRPA"),
    EAN("EAN NO EXISTE"),
    PAIS("PAIS NO CONFIGURADO"),
    NULO("FALTAN DATOS"),
	LOGGER("ERROR: {}"), 
	BD("ERROR CONEXION BASE DATOS");

    private String error;

    EnumErrores(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
    	return error;
    }
}
