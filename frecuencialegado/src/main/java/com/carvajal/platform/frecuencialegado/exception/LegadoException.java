package com.carvajal.platform.frecuencialegado.exception;

public class LegadoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7300624752630506638L;
	
	public LegadoException() {
		super();
	}
	
	/**
	 * 
	 * @param message
	 */
	public LegadoException(String message) {
		super(message);
	}

}
