package com.carvajal.documentservice.web.rest;

import com.carvajal.documentservice.DocumentServiceApp;

import com.carvajal.documentservice.domain.PuntosVentaFallidos;
import com.carvajal.documentservice.repository.PuntosVentaFallidosRepository;
import com.carvajal.documentservice.service.PuntosVentaFallidosService;
import com.carvajal.documentservice.service.dto.PuntosVentaFallidosDTO;
import com.carvajal.documentservice.service.mapper.PuntosVentaFallidosMapper;
import com.carvajal.documentservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.carvajal.documentservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PuntosVentaFallidosResource REST controller.
 *
 * @see PuntosVentaFallidosResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DocumentServiceApp.class)
public class PuntosVentaFallidosResourceIntTest {

    private static final String DEFAULT_COD_COMERCIO = "AAAAAAAAAA";
    private static final String UPDATED_COD_COMERCIO = "BBBBBBBBBB";

    private static final String DEFAULT_CAMPO_ORDEN_COMPRA = "AAAAAAAAAA";
    private static final String UPDATED_CAMPO_ORDEN_COMPRA = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_FALLO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_FALLO = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_ORDEN_COMPRA = 1L;
    private static final Long UPDATED_ID_ORDEN_COMPRA = 2L;

    @Autowired
    private PuntosVentaFallidosRepository puntosVentaFallidosRepository;


    @Autowired
    private PuntosVentaFallidosMapper puntosVentaFallidosMapper;
    

    @Autowired
    private PuntosVentaFallidosService puntosVentaFallidosService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPuntosVentaFallidosMockMvc;

    private PuntosVentaFallidos puntosVentaFallidos;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PuntosVentaFallidosResource puntosVentaFallidosResource = new PuntosVentaFallidosResource(puntosVentaFallidosService);
        this.restPuntosVentaFallidosMockMvc = MockMvcBuilders.standaloneSetup(puntosVentaFallidosResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PuntosVentaFallidos createEntity(EntityManager em) {
        PuntosVentaFallidos puntosVentaFallidos = new PuntosVentaFallidos()
            .codComercio(DEFAULT_COD_COMERCIO)
            .campoOrdenCompra(DEFAULT_CAMPO_ORDEN_COMPRA)
            .tipoFallo(DEFAULT_TIPO_FALLO)
            .idOrdenCompra(DEFAULT_ID_ORDEN_COMPRA);
        return puntosVentaFallidos;
    }

    @Before
    public void initTest() {
        puntosVentaFallidos = createEntity(em);
    }

    @Test
    @Transactional
    public void createPuntosVentaFallidos() throws Exception {
        int databaseSizeBeforeCreate = puntosVentaFallidosRepository.findAll().size();

        // Create the PuntosVentaFallidos
        PuntosVentaFallidosDTO puntosVentaFallidosDTO = puntosVentaFallidosMapper.toDto(puntosVentaFallidos);
        restPuntosVentaFallidosMockMvc.perform(post("/api/puntos-venta-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(puntosVentaFallidosDTO)))
            .andExpect(status().isCreated());

        // Validate the PuntosVentaFallidos in the database
        List<PuntosVentaFallidos> puntosVentaFallidosList = puntosVentaFallidosRepository.findAll();
        assertThat(puntosVentaFallidosList).hasSize(databaseSizeBeforeCreate + 1);
        PuntosVentaFallidos testPuntosVentaFallidos = puntosVentaFallidosList.get(puntosVentaFallidosList.size() - 1);
        assertThat(testPuntosVentaFallidos.getCodComercio()).isEqualTo(DEFAULT_COD_COMERCIO);
        assertThat(testPuntosVentaFallidos.getCampoOrdenCompra()).isEqualTo(DEFAULT_CAMPO_ORDEN_COMPRA);
        assertThat(testPuntosVentaFallidos.getTipoFallo()).isEqualTo(DEFAULT_TIPO_FALLO);
        assertThat(testPuntosVentaFallidos.getIdOrdenCompra()).isEqualTo(DEFAULT_ID_ORDEN_COMPRA);
    }

    @Test
    @Transactional
    public void createPuntosVentaFallidosWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = puntosVentaFallidosRepository.findAll().size();

        // Create the PuntosVentaFallidos with an existing ID
        puntosVentaFallidos.setId(1L);
        PuntosVentaFallidosDTO puntosVentaFallidosDTO = puntosVentaFallidosMapper.toDto(puntosVentaFallidos);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPuntosVentaFallidosMockMvc.perform(post("/api/puntos-venta-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(puntosVentaFallidosDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PuntosVentaFallidos in the database
        List<PuntosVentaFallidos> puntosVentaFallidosList = puntosVentaFallidosRepository.findAll();
        assertThat(puntosVentaFallidosList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodComercioIsRequired() throws Exception {
        int databaseSizeBeforeTest = puntosVentaFallidosRepository.findAll().size();
        // set the field null
        puntosVentaFallidos.setCodComercio(null);

        // Create the PuntosVentaFallidos, which fails.
        PuntosVentaFallidosDTO puntosVentaFallidosDTO = puntosVentaFallidosMapper.toDto(puntosVentaFallidos);

        restPuntosVentaFallidosMockMvc.perform(post("/api/puntos-venta-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(puntosVentaFallidosDTO)))
            .andExpect(status().isBadRequest());

        List<PuntosVentaFallidos> puntosVentaFallidosList = puntosVentaFallidosRepository.findAll();
        assertThat(puntosVentaFallidosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCampoOrdenCompraIsRequired() throws Exception {
        int databaseSizeBeforeTest = puntosVentaFallidosRepository.findAll().size();
        // set the field null
        puntosVentaFallidos.setCampoOrdenCompra(null);

        // Create the PuntosVentaFallidos, which fails.
        PuntosVentaFallidosDTO puntosVentaFallidosDTO = puntosVentaFallidosMapper.toDto(puntosVentaFallidos);

        restPuntosVentaFallidosMockMvc.perform(post("/api/puntos-venta-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(puntosVentaFallidosDTO)))
            .andExpect(status().isBadRequest());

        List<PuntosVentaFallidos> puntosVentaFallidosList = puntosVentaFallidosRepository.findAll();
        assertThat(puntosVentaFallidosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoFalloIsRequired() throws Exception {
        int databaseSizeBeforeTest = puntosVentaFallidosRepository.findAll().size();
        // set the field null
        puntosVentaFallidos.setTipoFallo(null);

        // Create the PuntosVentaFallidos, which fails.
        PuntosVentaFallidosDTO puntosVentaFallidosDTO = puntosVentaFallidosMapper.toDto(puntosVentaFallidos);

        restPuntosVentaFallidosMockMvc.perform(post("/api/puntos-venta-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(puntosVentaFallidosDTO)))
            .andExpect(status().isBadRequest());

        List<PuntosVentaFallidos> puntosVentaFallidosList = puntosVentaFallidosRepository.findAll();
        assertThat(puntosVentaFallidosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdOrdenCompraIsRequired() throws Exception {
        int databaseSizeBeforeTest = puntosVentaFallidosRepository.findAll().size();
        // set the field null
        puntosVentaFallidos.setIdOrdenCompra(null);

        // Create the PuntosVentaFallidos, which fails.
        PuntosVentaFallidosDTO puntosVentaFallidosDTO = puntosVentaFallidosMapper.toDto(puntosVentaFallidos);

        restPuntosVentaFallidosMockMvc.perform(post("/api/puntos-venta-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(puntosVentaFallidosDTO)))
            .andExpect(status().isBadRequest());

        List<PuntosVentaFallidos> puntosVentaFallidosList = puntosVentaFallidosRepository.findAll();
        assertThat(puntosVentaFallidosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPuntosVentaFallidos() throws Exception {
        // Initialize the database
        puntosVentaFallidosRepository.saveAndFlush(puntosVentaFallidos);

        // Get all the puntosVentaFallidosList
        restPuntosVentaFallidosMockMvc.perform(get("/api/puntos-venta-fallidos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(puntosVentaFallidos.getId().intValue())))
            .andExpect(jsonPath("$.[*].codComercio").value(hasItem(DEFAULT_COD_COMERCIO.toString())))
            .andExpect(jsonPath("$.[*].campoOrdenCompra").value(hasItem(DEFAULT_CAMPO_ORDEN_COMPRA.toString())))
            .andExpect(jsonPath("$.[*].tipoFallo").value(hasItem(DEFAULT_TIPO_FALLO.toString())))
            .andExpect(jsonPath("$.[*].idOrdenCompra").value(hasItem(DEFAULT_ID_ORDEN_COMPRA.intValue())));
    }
    

    @Test
    @Transactional
    public void getPuntosVentaFallidos() throws Exception {
        // Initialize the database
        puntosVentaFallidosRepository.saveAndFlush(puntosVentaFallidos);

        // Get the puntosVentaFallidos
        restPuntosVentaFallidosMockMvc.perform(get("/api/puntos-venta-fallidos/{id}", puntosVentaFallidos.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(puntosVentaFallidos.getId().intValue()))
            .andExpect(jsonPath("$.codComercio").value(DEFAULT_COD_COMERCIO.toString()))
            .andExpect(jsonPath("$.campoOrdenCompra").value(DEFAULT_CAMPO_ORDEN_COMPRA.toString()))
            .andExpect(jsonPath("$.tipoFallo").value(DEFAULT_TIPO_FALLO.toString()))
            .andExpect(jsonPath("$.idOrdenCompra").value(DEFAULT_ID_ORDEN_COMPRA.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingPuntosVentaFallidos() throws Exception {
        // Get the puntosVentaFallidos
        restPuntosVentaFallidosMockMvc.perform(get("/api/puntos-venta-fallidos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePuntosVentaFallidos() throws Exception {
        // Initialize the database
        puntosVentaFallidosRepository.saveAndFlush(puntosVentaFallidos);

        int databaseSizeBeforeUpdate = puntosVentaFallidosRepository.findAll().size();

        // Update the puntosVentaFallidos
        PuntosVentaFallidos updatedPuntosVentaFallidos = puntosVentaFallidosRepository.findById(puntosVentaFallidos.getId()).get();
        // Disconnect from session so that the updates on updatedPuntosVentaFallidos are not directly saved in db
        em.detach(updatedPuntosVentaFallidos);
        updatedPuntosVentaFallidos
            .codComercio(UPDATED_COD_COMERCIO)
            .campoOrdenCompra(UPDATED_CAMPO_ORDEN_COMPRA)
            .tipoFallo(UPDATED_TIPO_FALLO)
            .idOrdenCompra(UPDATED_ID_ORDEN_COMPRA);
        PuntosVentaFallidosDTO puntosVentaFallidosDTO = puntosVentaFallidosMapper.toDto(updatedPuntosVentaFallidos);

        restPuntosVentaFallidosMockMvc.perform(put("/api/puntos-venta-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(puntosVentaFallidosDTO)))
            .andExpect(status().isOk());

        // Validate the PuntosVentaFallidos in the database
        List<PuntosVentaFallidos> puntosVentaFallidosList = puntosVentaFallidosRepository.findAll();
        assertThat(puntosVentaFallidosList).hasSize(databaseSizeBeforeUpdate);
        PuntosVentaFallidos testPuntosVentaFallidos = puntosVentaFallidosList.get(puntosVentaFallidosList.size() - 1);
        assertThat(testPuntosVentaFallidos.getCodComercio()).isEqualTo(UPDATED_COD_COMERCIO);
        assertThat(testPuntosVentaFallidos.getCampoOrdenCompra()).isEqualTo(UPDATED_CAMPO_ORDEN_COMPRA);
        assertThat(testPuntosVentaFallidos.getTipoFallo()).isEqualTo(UPDATED_TIPO_FALLO);
        assertThat(testPuntosVentaFallidos.getIdOrdenCompra()).isEqualTo(UPDATED_ID_ORDEN_COMPRA);
    }

    @Test
    @Transactional
    public void updateNonExistingPuntosVentaFallidos() throws Exception {
        int databaseSizeBeforeUpdate = puntosVentaFallidosRepository.findAll().size();

        // Create the PuntosVentaFallidos
        PuntosVentaFallidosDTO puntosVentaFallidosDTO = puntosVentaFallidosMapper.toDto(puntosVentaFallidos);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPuntosVentaFallidosMockMvc.perform(put("/api/puntos-venta-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(puntosVentaFallidosDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PuntosVentaFallidos in the database
        List<PuntosVentaFallidos> puntosVentaFallidosList = puntosVentaFallidosRepository.findAll();
        assertThat(puntosVentaFallidosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePuntosVentaFallidos() throws Exception {
        // Initialize the database
        puntosVentaFallidosRepository.saveAndFlush(puntosVentaFallidos);

        int databaseSizeBeforeDelete = puntosVentaFallidosRepository.findAll().size();

        // Get the puntosVentaFallidos
        restPuntosVentaFallidosMockMvc.perform(delete("/api/puntos-venta-fallidos/{id}", puntosVentaFallidos.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PuntosVentaFallidos> puntosVentaFallidosList = puntosVentaFallidosRepository.findAll();
        assertThat(puntosVentaFallidosList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PuntosVentaFallidos.class);
        PuntosVentaFallidos puntosVentaFallidos1 = new PuntosVentaFallidos();
        puntosVentaFallidos1.setId(1L);
        PuntosVentaFallidos puntosVentaFallidos2 = new PuntosVentaFallidos();
        puntosVentaFallidos2.setId(puntosVentaFallidos1.getId());
        assertThat(puntosVentaFallidos1).isEqualTo(puntosVentaFallidos2);
        puntosVentaFallidos2.setId(2L);
        assertThat(puntosVentaFallidos1).isNotEqualTo(puntosVentaFallidos2);
        puntosVentaFallidos1.setId(null);
        assertThat(puntosVentaFallidos1).isNotEqualTo(puntosVentaFallidos2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PuntosVentaFallidosDTO.class);
        PuntosVentaFallidosDTO puntosVentaFallidosDTO1 = new PuntosVentaFallidosDTO();
        puntosVentaFallidosDTO1.setId(1L);
        PuntosVentaFallidosDTO puntosVentaFallidosDTO2 = new PuntosVentaFallidosDTO();
        assertThat(puntosVentaFallidosDTO1).isNotEqualTo(puntosVentaFallidosDTO2);
        puntosVentaFallidosDTO2.setId(puntosVentaFallidosDTO1.getId());
        assertThat(puntosVentaFallidosDTO1).isEqualTo(puntosVentaFallidosDTO2);
        puntosVentaFallidosDTO2.setId(2L);
        assertThat(puntosVentaFallidosDTO1).isNotEqualTo(puntosVentaFallidosDTO2);
        puntosVentaFallidosDTO1.setId(null);
        assertThat(puntosVentaFallidosDTO1).isNotEqualTo(puntosVentaFallidosDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(puntosVentaFallidosMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(puntosVentaFallidosMapper.fromId(null)).isNull();
    }
}
