package com.carvajal.documentservice.web.rest;

import com.carvajal.documentservice.DocumentServiceApp;

import com.carvajal.documentservice.domain.ReglasFallidas;
import com.carvajal.documentservice.repository.ReglasFallidasRepository;
import com.carvajal.documentservice.service.ReglasFallidasService;
import com.carvajal.documentservice.service.dto.ReglasFallidasDTO;
import com.carvajal.documentservice.service.mapper.ReglasFallidasMapper;
import com.carvajal.documentservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static com.carvajal.documentservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ReglasFallidasResource REST controller.
 *
 * @see ReglasFallidasResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DocumentServiceApp.class)
public class ReglasFallidasResourceIntTest {

    private static final Instant DEFAULT_FECHA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TIPO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_ORDEN_COMPRA = 1L;
    private static final Long UPDATED_ID_ORDEN_COMPRA = 2L;

    @Autowired
    private ReglasFallidasRepository reglasFallidasRepository;


    @Autowired
    private ReglasFallidasMapper reglasFallidasMapper;
    

    @Autowired
    private ReglasFallidasService reglasFallidasService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restReglasFallidasMockMvc;

    private ReglasFallidas reglasFallidas;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ReglasFallidasResource reglasFallidasResource = new ReglasFallidasResource(reglasFallidasService);
        this.restReglasFallidasMockMvc = MockMvcBuilders.standaloneSetup(reglasFallidasResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReglasFallidas createEntity(EntityManager em) {
        ReglasFallidas reglasFallidas = new ReglasFallidas()
            .fecha(DEFAULT_FECHA)
            .tipo(DEFAULT_TIPO)
            .idOrdenCompra(DEFAULT_ID_ORDEN_COMPRA);
        return reglasFallidas;
    }

    @Before
    public void initTest() {
        reglasFallidas = createEntity(em);
    }

    @Test
    @Transactional
    public void createReglasFallidas() throws Exception {
        int databaseSizeBeforeCreate = reglasFallidasRepository.findAll().size();

        // Create the ReglasFallidas
        ReglasFallidasDTO reglasFallidasDTO = reglasFallidasMapper.toDto(reglasFallidas);
        restReglasFallidasMockMvc.perform(post("/api/reglas-fallidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasFallidasDTO)))
            .andExpect(status().isCreated());

        // Validate the ReglasFallidas in the database
        List<ReglasFallidas> reglasFallidasList = reglasFallidasRepository.findAll();
        assertThat(reglasFallidasList).hasSize(databaseSizeBeforeCreate + 1);
        ReglasFallidas testReglasFallidas = reglasFallidasList.get(reglasFallidasList.size() - 1);
        assertThat(testReglasFallidas.getFecha()).isEqualTo(DEFAULT_FECHA);
        assertThat(testReglasFallidas.getTipo()).isEqualTo(DEFAULT_TIPO);
        assertThat(testReglasFallidas.getIdOrdenCompra()).isEqualTo(DEFAULT_ID_ORDEN_COMPRA);
    }

    @Test
    @Transactional
    public void createReglasFallidasWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = reglasFallidasRepository.findAll().size();

        // Create the ReglasFallidas with an existing ID
        reglasFallidas.setId(1L);
        ReglasFallidasDTO reglasFallidasDTO = reglasFallidasMapper.toDto(reglasFallidas);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReglasFallidasMockMvc.perform(post("/api/reglas-fallidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasFallidasDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ReglasFallidas in the database
        List<ReglasFallidas> reglasFallidasList = reglasFallidasRepository.findAll();
        assertThat(reglasFallidasList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFechaIsRequired() throws Exception {
        int databaseSizeBeforeTest = reglasFallidasRepository.findAll().size();
        // set the field null
        reglasFallidas.setFecha(null);

        // Create the ReglasFallidas, which fails.
        ReglasFallidasDTO reglasFallidasDTO = reglasFallidasMapper.toDto(reglasFallidas);

        restReglasFallidasMockMvc.perform(post("/api/reglas-fallidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasFallidasDTO)))
            .andExpect(status().isBadRequest());

        List<ReglasFallidas> reglasFallidasList = reglasFallidasRepository.findAll();
        assertThat(reglasFallidasList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoIsRequired() throws Exception {
        int databaseSizeBeforeTest = reglasFallidasRepository.findAll().size();
        // set the field null
        reglasFallidas.setTipo(null);

        // Create the ReglasFallidas, which fails.
        ReglasFallidasDTO reglasFallidasDTO = reglasFallidasMapper.toDto(reglasFallidas);

        restReglasFallidasMockMvc.perform(post("/api/reglas-fallidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasFallidasDTO)))
            .andExpect(status().isBadRequest());

        List<ReglasFallidas> reglasFallidasList = reglasFallidasRepository.findAll();
        assertThat(reglasFallidasList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdOrdenCompraIsRequired() throws Exception {
        int databaseSizeBeforeTest = reglasFallidasRepository.findAll().size();
        // set the field null
        reglasFallidas.setIdOrdenCompra(null);

        // Create the ReglasFallidas, which fails.
        ReglasFallidasDTO reglasFallidasDTO = reglasFallidasMapper.toDto(reglasFallidas);

        restReglasFallidasMockMvc.perform(post("/api/reglas-fallidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasFallidasDTO)))
            .andExpect(status().isBadRequest());

        List<ReglasFallidas> reglasFallidasList = reglasFallidasRepository.findAll();
        assertThat(reglasFallidasList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllReglasFallidas() throws Exception {
        // Initialize the database
        reglasFallidasRepository.saveAndFlush(reglasFallidas);

        // Get all the reglasFallidasList
        restReglasFallidasMockMvc.perform(get("/api/reglas-fallidas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reglasFallidas.getId().intValue())))
            .andExpect(jsonPath("$.[*].fecha").value(hasItem(DEFAULT_FECHA.toString())))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())))
            .andExpect(jsonPath("$.[*].idOrdenCompra").value(hasItem(DEFAULT_ID_ORDEN_COMPRA.intValue())));
    }
    

    @Test
    @Transactional
    public void getReglasFallidas() throws Exception {
        // Initialize the database
        reglasFallidasRepository.saveAndFlush(reglasFallidas);

        // Get the reglasFallidas
        restReglasFallidasMockMvc.perform(get("/api/reglas-fallidas/{id}", reglasFallidas.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(reglasFallidas.getId().intValue()))
            .andExpect(jsonPath("$.fecha").value(DEFAULT_FECHA.toString()))
            .andExpect(jsonPath("$.tipo").value(DEFAULT_TIPO.toString()))
            .andExpect(jsonPath("$.idOrdenCompra").value(DEFAULT_ID_ORDEN_COMPRA.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingReglasFallidas() throws Exception {
        // Get the reglasFallidas
        restReglasFallidasMockMvc.perform(get("/api/reglas-fallidas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReglasFallidas() throws Exception {
        // Initialize the database
        reglasFallidasRepository.saveAndFlush(reglasFallidas);

        int databaseSizeBeforeUpdate = reglasFallidasRepository.findAll().size();

        // Update the reglasFallidas
        ReglasFallidas updatedReglasFallidas = reglasFallidasRepository.findById(reglasFallidas.getId()).get();
        // Disconnect from session so that the updates on updatedReglasFallidas are not directly saved in db
        em.detach(updatedReglasFallidas);
        updatedReglasFallidas
            .fecha(UPDATED_FECHA)
            .tipo(UPDATED_TIPO)
            .idOrdenCompra(UPDATED_ID_ORDEN_COMPRA);
        ReglasFallidasDTO reglasFallidasDTO = reglasFallidasMapper.toDto(updatedReglasFallidas);

        restReglasFallidasMockMvc.perform(put("/api/reglas-fallidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasFallidasDTO)))
            .andExpect(status().isOk());

        // Validate the ReglasFallidas in the database
        List<ReglasFallidas> reglasFallidasList = reglasFallidasRepository.findAll();
        assertThat(reglasFallidasList).hasSize(databaseSizeBeforeUpdate);
        ReglasFallidas testReglasFallidas = reglasFallidasList.get(reglasFallidasList.size() - 1);
        assertThat(testReglasFallidas.getFecha()).isEqualTo(UPDATED_FECHA);
        assertThat(testReglasFallidas.getTipo()).isEqualTo(UPDATED_TIPO);
        assertThat(testReglasFallidas.getIdOrdenCompra()).isEqualTo(UPDATED_ID_ORDEN_COMPRA);
    }

    @Test
    @Transactional
    public void updateNonExistingReglasFallidas() throws Exception {
        int databaseSizeBeforeUpdate = reglasFallidasRepository.findAll().size();

        // Create the ReglasFallidas
        ReglasFallidasDTO reglasFallidasDTO = reglasFallidasMapper.toDto(reglasFallidas);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restReglasFallidasMockMvc.perform(put("/api/reglas-fallidas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasFallidasDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ReglasFallidas in the database
        List<ReglasFallidas> reglasFallidasList = reglasFallidasRepository.findAll();
        assertThat(reglasFallidasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteReglasFallidas() throws Exception {
        // Initialize the database
        reglasFallidasRepository.saveAndFlush(reglasFallidas);

        int databaseSizeBeforeDelete = reglasFallidasRepository.findAll().size();

        // Get the reglasFallidas
        restReglasFallidasMockMvc.perform(delete("/api/reglas-fallidas/{id}", reglasFallidas.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ReglasFallidas> reglasFallidasList = reglasFallidasRepository.findAll();
        assertThat(reglasFallidasList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReglasFallidas.class);
        ReglasFallidas reglasFallidas1 = new ReglasFallidas();
        reglasFallidas1.setId(1L);
        ReglasFallidas reglasFallidas2 = new ReglasFallidas();
        reglasFallidas2.setId(reglasFallidas1.getId());
        assertThat(reglasFallidas1).isEqualTo(reglasFallidas2);
        reglasFallidas2.setId(2L);
        assertThat(reglasFallidas1).isNotEqualTo(reglasFallidas2);
        reglasFallidas1.setId(null);
        assertThat(reglasFallidas1).isNotEqualTo(reglasFallidas2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReglasFallidasDTO.class);
        ReglasFallidasDTO reglasFallidasDTO1 = new ReglasFallidasDTO();
        reglasFallidasDTO1.setId(1L);
        ReglasFallidasDTO reglasFallidasDTO2 = new ReglasFallidasDTO();
        assertThat(reglasFallidasDTO1).isNotEqualTo(reglasFallidasDTO2);
        reglasFallidasDTO2.setId(reglasFallidasDTO1.getId());
        assertThat(reglasFallidasDTO1).isEqualTo(reglasFallidasDTO2);
        reglasFallidasDTO2.setId(2L);
        assertThat(reglasFallidasDTO1).isNotEqualTo(reglasFallidasDTO2);
        reglasFallidasDTO1.setId(null);
        assertThat(reglasFallidasDTO1).isNotEqualTo(reglasFallidasDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(reglasFallidasMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(reglasFallidasMapper.fromId(null)).isNull();
    }
}
