package com.carvajal.documentservice.web.rest;

import com.carvajal.documentservice.DocumentServiceApp;

import com.carvajal.documentservice.domain.Reglas;
import com.carvajal.documentservice.repository.ReglasRepository;
import com.carvajal.documentservice.service.ReglasService;
import com.carvajal.documentservice.service.dto.ReglasDTO;
import com.carvajal.documentservice.service.mapper.ReglasMapper;
import com.carvajal.documentservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.carvajal.documentservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ReglasResource REST controller.
 *
 * @see ReglasResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DocumentServiceApp.class)
public class ReglasResourceIntTest {

    private static final String DEFAULT_COD_REGLA = "AAAAAAAAAA";
    private static final String UPDATED_COD_REGLA = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    @Autowired
    private ReglasRepository reglasRepository;


    @Autowired
    private ReglasMapper reglasMapper;
    

    @Autowired
    private ReglasService reglasService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restReglasMockMvc;

    private Reglas reglas;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ReglasResource reglasResource = new ReglasResource(reglasService);
        this.restReglasMockMvc = MockMvcBuilders.standaloneSetup(reglasResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Reglas createEntity(EntityManager em) {
        Reglas reglas = new Reglas()
            .codRegla(DEFAULT_COD_REGLA)
            .descripcion(DEFAULT_DESCRIPCION);
        return reglas;
    }

    @Before
    public void initTest() {
        reglas = createEntity(em);
    }

    @Test
    @Transactional
    public void createReglas() throws Exception {
        int databaseSizeBeforeCreate = reglasRepository.findAll().size();

        // Create the Reglas
        ReglasDTO reglasDTO = reglasMapper.toDto(reglas);
        restReglasMockMvc.perform(post("/api/reglas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasDTO)))
            .andExpect(status().isCreated());

        // Validate the Reglas in the database
        List<Reglas> reglasList = reglasRepository.findAll();
        assertThat(reglasList).hasSize(databaseSizeBeforeCreate + 1);
        Reglas testReglas = reglasList.get(reglasList.size() - 1);
        assertThat(testReglas.getCodRegla()).isEqualTo(DEFAULT_COD_REGLA);
        assertThat(testReglas.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
    }

    @Test
    @Transactional
    public void createReglasWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = reglasRepository.findAll().size();

        // Create the Reglas with an existing ID
        reglas.setId(1L);
        ReglasDTO reglasDTO = reglasMapper.toDto(reglas);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReglasMockMvc.perform(post("/api/reglas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Reglas in the database
        List<Reglas> reglasList = reglasRepository.findAll();
        assertThat(reglasList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodReglaIsRequired() throws Exception {
        int databaseSizeBeforeTest = reglasRepository.findAll().size();
        // set the field null
        reglas.setCodRegla(null);

        // Create the Reglas, which fails.
        ReglasDTO reglasDTO = reglasMapper.toDto(reglas);

        restReglasMockMvc.perform(post("/api/reglas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasDTO)))
            .andExpect(status().isBadRequest());

        List<Reglas> reglasList = reglasRepository.findAll();
        assertThat(reglasList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescripcionIsRequired() throws Exception {
        int databaseSizeBeforeTest = reglasRepository.findAll().size();
        // set the field null
        reglas.setDescripcion(null);

        // Create the Reglas, which fails.
        ReglasDTO reglasDTO = reglasMapper.toDto(reglas);

        restReglasMockMvc.perform(post("/api/reglas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasDTO)))
            .andExpect(status().isBadRequest());

        List<Reglas> reglasList = reglasRepository.findAll();
        assertThat(reglasList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllReglas() throws Exception {
        // Initialize the database
        reglasRepository.saveAndFlush(reglas);

        // Get all the reglasList
        restReglasMockMvc.perform(get("/api/reglas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reglas.getId().intValue())))
            .andExpect(jsonPath("$.[*].codRegla").value(hasItem(DEFAULT_COD_REGLA.toString())))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())));
    }
    

    @Test
    @Transactional
    public void getReglas() throws Exception {
        // Initialize the database
        reglasRepository.saveAndFlush(reglas);

        // Get the reglas
        restReglasMockMvc.perform(get("/api/reglas/{id}", reglas.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(reglas.getId().intValue()))
            .andExpect(jsonPath("$.codRegla").value(DEFAULT_COD_REGLA.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingReglas() throws Exception {
        // Get the reglas
        restReglasMockMvc.perform(get("/api/reglas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReglas() throws Exception {
        // Initialize the database
        reglasRepository.saveAndFlush(reglas);

        int databaseSizeBeforeUpdate = reglasRepository.findAll().size();

        // Update the reglas
        Reglas updatedReglas = reglasRepository.findById(reglas.getId()).get();
        // Disconnect from session so that the updates on updatedReglas are not directly saved in db
        em.detach(updatedReglas);
        updatedReglas
            .codRegla(UPDATED_COD_REGLA)
            .descripcion(UPDATED_DESCRIPCION);
        ReglasDTO reglasDTO = reglasMapper.toDto(updatedReglas);

        restReglasMockMvc.perform(put("/api/reglas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasDTO)))
            .andExpect(status().isOk());

        // Validate the Reglas in the database
        List<Reglas> reglasList = reglasRepository.findAll();
        assertThat(reglasList).hasSize(databaseSizeBeforeUpdate);
        Reglas testReglas = reglasList.get(reglasList.size() - 1);
        assertThat(testReglas.getCodRegla()).isEqualTo(UPDATED_COD_REGLA);
        assertThat(testReglas.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
    }

    @Test
    @Transactional
    public void updateNonExistingReglas() throws Exception {
        int databaseSizeBeforeUpdate = reglasRepository.findAll().size();

        // Create the Reglas
        ReglasDTO reglasDTO = reglasMapper.toDto(reglas);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restReglasMockMvc.perform(put("/api/reglas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reglasDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Reglas in the database
        List<Reglas> reglasList = reglasRepository.findAll();
        assertThat(reglasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteReglas() throws Exception {
        // Initialize the database
        reglasRepository.saveAndFlush(reglas);

        int databaseSizeBeforeDelete = reglasRepository.findAll().size();

        // Get the reglas
        restReglasMockMvc.perform(delete("/api/reglas/{id}", reglas.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Reglas> reglasList = reglasRepository.findAll();
        assertThat(reglasList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Reglas.class);
        Reglas reglas1 = new Reglas();
        reglas1.setId(1L);
        Reglas reglas2 = new Reglas();
        reglas2.setId(reglas1.getId());
        assertThat(reglas1).isEqualTo(reglas2);
        reglas2.setId(2L);
        assertThat(reglas1).isNotEqualTo(reglas2);
        reglas1.setId(null);
        assertThat(reglas1).isNotEqualTo(reglas2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReglasDTO.class);
        ReglasDTO reglasDTO1 = new ReglasDTO();
        reglasDTO1.setId(1L);
        ReglasDTO reglasDTO2 = new ReglasDTO();
        assertThat(reglasDTO1).isNotEqualTo(reglasDTO2);
        reglasDTO2.setId(reglasDTO1.getId());
        assertThat(reglasDTO1).isEqualTo(reglasDTO2);
        reglasDTO2.setId(2L);
        assertThat(reglasDTO1).isNotEqualTo(reglasDTO2);
        reglasDTO1.setId(null);
        assertThat(reglasDTO1).isNotEqualTo(reglasDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(reglasMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(reglasMapper.fromId(null)).isNull();
    }
}
