package com.carvajal.documentservice.web.rest;

import static com.carvajal.documentservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.carvajal.documentservice.DocumentServiceApp;
import com.carvajal.documentservice.domain.Documento;
import com.carvajal.documentservice.repository.DocumentoRepository;
import com.carvajal.documentservice.service.DocumentoService;
import com.carvajal.documentservice.service.dto.DocumentoDTO;
import com.carvajal.documentservice.service.mapper.DocumentoMapper;
import com.carvajal.documentservice.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the DocumentoResource REST controller.
 *
 * @see DocumentoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DocumentServiceApp.class)
public class DocumentoResourceIntTest {

    private static final Long DEFAULT_ID_DOCUMENTO_PADRE = 1L;
    private static final Long UPDATED_ID_DOCUMENTO_PADRE = 2L;

    private static final String DEFAULT_NOMBRE_DOCUMENTO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_DOCUMENTO = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_DOCUMENTO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_DOCUMENTO = "BBBBBBBBBB";

    private static final Date DEFAULT_FECHA_DOCUMENTO = new Date();
    private static final Date UPDATED_FECHA_DOCUMENTO = new Date();

    private static final Date DEFAULT_FECHA_ENTREGA = new Date();
    private static final Date UPDATED_FECHA_ENTREGA = new Date();

    private static final String DEFAULT_COD_PAIS = "AAAAAAAAAA";
    private static final String UPDATED_COD_PAIS = "BBBBBBBBBB";

    private static final String DEFAULT_EMISOR = "AAAAAAAAAA";
    private static final String UPDATED_EMISOR = "BBBBBBBBBB";

    private static final String DEFAULT_RECEPTOR = "AAAAAAAAAA";
    private static final String UPDATED_RECEPTOR = "BBBBBBBBBB";

    private static final String DEFAULT_ESTADO = "AAAAAAAAAA";
    private static final String UPDATED_ESTADO = "BBBBBBBBBB";

    private static final String DEFAULT_ID_DOCUMENT_STORAGE = "AAAAAAAAAA";
    private static final String UPDATED_ID_DOCUMENT_STORAGE = "BBBBBBBBBB";

    private static final String DEFAULT_ID_DYNAMO = "AAAAAAAAAA";
    private static final String UPDATED_ID_DYNAMO = "BBBBBBBBBB";

    private static final String DEFAULT_FORMATO = "AAAAAAAAAA";
    private static final String UPDATED_FORMATO = "BBBBBBBBBB";

    @Autowired
    private DocumentoRepository documentoRepository;


    @Autowired
    private DocumentoMapper documentoMapper;
    

    @Autowired
    private DocumentoService documentoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDocumentoMockMvc;

    private Documento documento;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DocumentoResource documentoResource = new DocumentoResource(documentoService);
        this.restDocumentoMockMvc = MockMvcBuilders.standaloneSetup(documentoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Documento createEntity(EntityManager em) {
        Documento documento = new Documento()
            .idDocumentoPadre(DEFAULT_ID_DOCUMENTO_PADRE)
            .nombreDocumento(DEFAULT_NOMBRE_DOCUMENTO)
            .tipoDocumento(DEFAULT_TIPO_DOCUMENTO)
            .fechaDocumento(DEFAULT_FECHA_DOCUMENTO)
            .fechaEntrega(DEFAULT_FECHA_ENTREGA)
            .codPais(DEFAULT_COD_PAIS)
            .emisor(DEFAULT_EMISOR)
            .receptor(DEFAULT_RECEPTOR)
            .estado(DEFAULT_ESTADO)
            .idDocumentStorage(DEFAULT_ID_DOCUMENT_STORAGE)
            .idDynamo(DEFAULT_ID_DYNAMO)
            .formato(DEFAULT_FORMATO);
        return documento;
    }

    @Before
    public void initTest() {
        documento = createEntity(em);
    }

    @Test
    @Transactional
    public void createDocumento() throws Exception {
        int databaseSizeBeforeCreate = documentoRepository.findAll().size();

        // Create the Documento
        DocumentoDTO documentoDTO = documentoMapper.toDto(documento);
        restDocumentoMockMvc.perform(post("/api/documentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoDTO)))
            .andExpect(status().isCreated());

        // Validate the Documento in the database
        List<Documento> documentoList = documentoRepository.findAll();
        assertThat(documentoList).hasSize(databaseSizeBeforeCreate + 1);
        Documento testDocumento = documentoList.get(documentoList.size() - 1);
        assertThat(testDocumento.getIdDocumentoPadre()).isEqualTo(DEFAULT_ID_DOCUMENTO_PADRE);
        assertThat(testDocumento.getNombreDocumento()).isEqualTo(DEFAULT_NOMBRE_DOCUMENTO);
        assertThat(testDocumento.getTipoDocumento()).isEqualTo(DEFAULT_TIPO_DOCUMENTO);
        assertThat(testDocumento.getFechaDocumento()).isEqualTo(DEFAULT_FECHA_DOCUMENTO);
        assertThat(testDocumento.getFechaEntrega()).isEqualTo(DEFAULT_FECHA_ENTREGA);
        assertThat(testDocumento.getCodPais()).isEqualTo(DEFAULT_COD_PAIS);
        assertThat(testDocumento.getEmisor()).isEqualTo(DEFAULT_EMISOR);
        assertThat(testDocumento.getReceptor()).isEqualTo(DEFAULT_RECEPTOR);
        assertThat(testDocumento.getEstado()).isEqualTo(DEFAULT_ESTADO);
        assertThat(testDocumento.getIdDocumentStorage()).isEqualTo(DEFAULT_ID_DOCUMENT_STORAGE);
        assertThat(testDocumento.getIdDynamo()).isEqualTo(DEFAULT_ID_DYNAMO);
        assertThat(testDocumento.getFormato()).isEqualTo(DEFAULT_FORMATO);
    }

    @Test
    @Transactional
    public void createDocumentoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = documentoRepository.findAll().size();

        // Create the Documento with an existing ID
        documento.setId(1L);
        DocumentoDTO documentoDTO = documentoMapper.toDto(documento);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDocumentoMockMvc.perform(post("/api/documentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Documento in the database
        List<Documento> documentoList = documentoRepository.findAll();
        assertThat(documentoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreDocumentoIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentoRepository.findAll().size();
        // set the field null
        documento.setNombreDocumento(null);

        // Create the Documento, which fails.
        DocumentoDTO documentoDTO = documentoMapper.toDto(documento);

        restDocumentoMockMvc.perform(post("/api/documentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoDTO)))
            .andExpect(status().isBadRequest());

        List<Documento> documentoList = documentoRepository.findAll();
        assertThat(documentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFechaDocumentoIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentoRepository.findAll().size();
        // set the field null
        documento.setFechaDocumento(null);

        // Create the Documento, which fails.
        DocumentoDTO documentoDTO = documentoMapper.toDto(documento);

        restDocumentoMockMvc.perform(post("/api/documentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoDTO)))
            .andExpect(status().isBadRequest());

        List<Documento> documentoList = documentoRepository.findAll();
        assertThat(documentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentoRepository.findAll().size();
        // set the field null
        documento.setEstado(null);

        // Create the Documento, which fails.
        DocumentoDTO documentoDTO = documentoMapper.toDto(documento);

        restDocumentoMockMvc.perform(post("/api/documentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoDTO)))
            .andExpect(status().isBadRequest());

        List<Documento> documentoList = documentoRepository.findAll();
        assertThat(documentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDocumentos() throws Exception {
        // Initialize the database
        documentoRepository.saveAndFlush(documento);

        // Get all the documentoList
        restDocumentoMockMvc.perform(get("/api/documentos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(documento.getId().intValue())))
            .andExpect(jsonPath("$.[*].idDocumentoPadre").value(hasItem(DEFAULT_ID_DOCUMENTO_PADRE.intValue())))
            .andExpect(jsonPath("$.[*].nombreDocumento").value(hasItem(DEFAULT_NOMBRE_DOCUMENTO.toString())))
            .andExpect(jsonPath("$.[*].tipoDocumento").value(hasItem(DEFAULT_TIPO_DOCUMENTO.toString())))
//            .andExpect(jsonPath("$.[*].fechaDocumento").value(hasItem(DEFAULT_FECHA_DOCUMENTO.toString())))
//            .andExpect(jsonPath("$.[*].fechaEntrega").value(hasItem(DEFAULT_FECHA_ENTREGA.toString())))
            .andExpect(jsonPath("$.[*].codPais").value(hasItem(DEFAULT_COD_PAIS.toString())))
            .andExpect(jsonPath("$.[*].emisor").value(hasItem(DEFAULT_EMISOR.toString())))
            .andExpect(jsonPath("$.[*].receptor").value(hasItem(DEFAULT_RECEPTOR.toString())))
            .andExpect(jsonPath("$.[*].estado").value(hasItem(DEFAULT_ESTADO.toString())))
            .andExpect(jsonPath("$.[*].idDocumentStorage").value(hasItem(DEFAULT_ID_DOCUMENT_STORAGE.toString())))
            .andExpect(jsonPath("$.[*].idDynamo").value(hasItem(DEFAULT_ID_DYNAMO.toString())))
            .andExpect(jsonPath("$.[*].formato").value(hasItem(DEFAULT_FORMATO.toString())));
    }
    

    @Test
    @Transactional
    public void getDocumento() throws Exception {
        // Initialize the database
        documentoRepository.saveAndFlush(documento);

        // Get the documento
        restDocumentoMockMvc.perform(get("/api/documentos/{id}", documento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(documento.getId().intValue()))
            .andExpect(jsonPath("$.idDocumentoPadre").value(DEFAULT_ID_DOCUMENTO_PADRE.intValue()))
            .andExpect(jsonPath("$.nombreDocumento").value(DEFAULT_NOMBRE_DOCUMENTO.toString()))
            .andExpect(jsonPath("$.tipoDocumento").value(DEFAULT_TIPO_DOCUMENTO.toString()))
//            .andExpect(jsonPath("$.fechaDocumento").value(DEFAULT_FECHA_DOCUMENTO.toString()))
//            .andExpect(jsonPath("$.fechaEntrega").value(DEFAULT_FECHA_ENTREGA.toString()))
            .andExpect(jsonPath("$.codPais").value(DEFAULT_COD_PAIS.toString()))
            .andExpect(jsonPath("$.emisor").value(DEFAULT_EMISOR.toString()))
            .andExpect(jsonPath("$.receptor").value(DEFAULT_RECEPTOR.toString()))
            .andExpect(jsonPath("$.estado").value(DEFAULT_ESTADO.toString()))
            .andExpect(jsonPath("$.idDocumentStorage").value(DEFAULT_ID_DOCUMENT_STORAGE.toString()))
            .andExpect(jsonPath("$.idDynamo").value(DEFAULT_ID_DYNAMO.toString()))
            .andExpect(jsonPath("$.formato").value(DEFAULT_FORMATO.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingDocumento() throws Exception {
        // Get the documento
        restDocumentoMockMvc.perform(get("/api/documentos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDocumento() throws Exception {
        // Initialize the database
        documentoRepository.saveAndFlush(documento);

        int databaseSizeBeforeUpdate = documentoRepository.findAll().size();

        // Update the documento
        Documento updatedDocumento = documentoRepository.findById(documento.getId()).get();
        // Disconnect from session so that the updates on updatedDocumento are not directly saved in db
        em.detach(updatedDocumento);
        updatedDocumento
            .idDocumentoPadre(UPDATED_ID_DOCUMENTO_PADRE)
            .nombreDocumento(UPDATED_NOMBRE_DOCUMENTO)
            .tipoDocumento(UPDATED_TIPO_DOCUMENTO)
            .fechaDocumento(UPDATED_FECHA_DOCUMENTO)
            .fechaEntrega(UPDATED_FECHA_ENTREGA)
            .codPais(UPDATED_COD_PAIS)
            .emisor(UPDATED_EMISOR)
            .receptor(UPDATED_RECEPTOR)
            .estado(UPDATED_ESTADO)
            .idDocumentStorage(UPDATED_ID_DOCUMENT_STORAGE)
            .idDynamo(UPDATED_ID_DYNAMO)
            .formato(UPDATED_FORMATO);
        DocumentoDTO documentoDTO = documentoMapper.toDto(updatedDocumento);

        restDocumentoMockMvc.perform(put("/api/documentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoDTO)))
            .andExpect(status().isOk());

        // Validate the Documento in the database
        List<Documento> documentoList = documentoRepository.findAll();
        assertThat(documentoList).hasSize(databaseSizeBeforeUpdate);
        Documento testDocumento = documentoList.get(documentoList.size() - 1);
        assertThat(testDocumento.getIdDocumentoPadre()).isEqualTo(UPDATED_ID_DOCUMENTO_PADRE);
        assertThat(testDocumento.getNombreDocumento()).isEqualTo(UPDATED_NOMBRE_DOCUMENTO);
        assertThat(testDocumento.getTipoDocumento()).isEqualTo(UPDATED_TIPO_DOCUMENTO);
        assertThat(testDocumento.getFechaDocumento()).isEqualTo(UPDATED_FECHA_DOCUMENTO);
        assertThat(testDocumento.getFechaEntrega()).isEqualTo(UPDATED_FECHA_ENTREGA);
        assertThat(testDocumento.getCodPais()).isEqualTo(UPDATED_COD_PAIS);
        assertThat(testDocumento.getEmisor()).isEqualTo(UPDATED_EMISOR);
        assertThat(testDocumento.getReceptor()).isEqualTo(UPDATED_RECEPTOR);
        assertThat(testDocumento.getEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testDocumento.getIdDocumentStorage()).isEqualTo(UPDATED_ID_DOCUMENT_STORAGE);
        assertThat(testDocumento.getIdDynamo()).isEqualTo(UPDATED_ID_DYNAMO);
        assertThat(testDocumento.getFormato()).isEqualTo(UPDATED_FORMATO);
    }

    @Test
    @Transactional
    public void updateNonExistingDocumento() throws Exception {
        int databaseSizeBeforeUpdate = documentoRepository.findAll().size();

        // Create the Documento
        DocumentoDTO documentoDTO = documentoMapper.toDto(documento);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDocumentoMockMvc.perform(put("/api/documentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Documento in the database
        List<Documento> documentoList = documentoRepository.findAll();
        assertThat(documentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDocumento() throws Exception {
        // Initialize the database
        documentoRepository.saveAndFlush(documento);

        int databaseSizeBeforeDelete = documentoRepository.findAll().size();

        // Get the documento
        restDocumentoMockMvc.perform(delete("/api/documentos/{id}", documento.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Documento> documentoList = documentoRepository.findAll();
        assertThat(documentoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Documento.class);
        Documento documento1 = new Documento();
        documento1.setId(1L);
        Documento documento2 = new Documento();
        documento2.setId(documento1.getId());
        assertThat(documento1).isEqualTo(documento2);
        documento2.setId(2L);
        assertThat(documento1).isNotEqualTo(documento2);
        documento1.setId(null);
        assertThat(documento1).isNotEqualTo(documento2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DocumentoDTO.class);
        DocumentoDTO documentoDTO1 = new DocumentoDTO();
        documentoDTO1.setId(1L);
        DocumentoDTO documentoDTO2 = new DocumentoDTO();
        assertThat(documentoDTO1).isNotEqualTo(documentoDTO2);
        documentoDTO2.setId(documentoDTO1.getId());
        assertThat(documentoDTO1).isEqualTo(documentoDTO2);
        documentoDTO2.setId(2L);
        assertThat(documentoDTO1).isNotEqualTo(documentoDTO2);
        documentoDTO1.setId(null);
        assertThat(documentoDTO1).isNotEqualTo(documentoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(documentoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(documentoMapper.fromId(null)).isNull();
    }
}
