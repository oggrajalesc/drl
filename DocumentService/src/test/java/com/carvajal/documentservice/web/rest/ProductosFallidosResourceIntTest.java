package com.carvajal.documentservice.web.rest;

import com.carvajal.documentservice.DocumentServiceApp;

import com.carvajal.documentservice.domain.ProductosFallidos;
import com.carvajal.documentservice.repository.ProductosFallidosRepository;
import com.carvajal.documentservice.service.ProductosFallidosService;
import com.carvajal.documentservice.service.dto.ProductosFallidosDTO;
import com.carvajal.documentservice.service.mapper.ProductosFallidosMapper;
import com.carvajal.documentservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.carvajal.documentservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProductosFallidosResource REST controller.
 *
 * @see ProductosFallidosResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DocumentServiceApp.class)
public class ProductosFallidosResourceIntTest {

    private static final String DEFAULT_COD_PRODUCTO = "AAAAAAAAAA";
    private static final String UPDATED_COD_PRODUCTO = "BBBBBBBBBB";

    private static final String DEFAULT_CAMPO_ORDEN_COMPRA = "AAAAAAAAAA";
    private static final String UPDATED_CAMPO_ORDEN_COMPRA = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_FALLO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_FALLO = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_ORDEN_COMPRA = 1L;
    private static final Long UPDATED_ID_ORDEN_COMPRA = 2L;

    @Autowired
    private ProductosFallidosRepository productosFallidosRepository;


    @Autowired
    private ProductosFallidosMapper productosFallidosMapper;
    

    @Autowired
    private ProductosFallidosService productosFallidosService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProductosFallidosMockMvc;

    private ProductosFallidos productosFallidos;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProductosFallidosResource productosFallidosResource = new ProductosFallidosResource(productosFallidosService);
        this.restProductosFallidosMockMvc = MockMvcBuilders.standaloneSetup(productosFallidosResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductosFallidos createEntity(EntityManager em) {
        ProductosFallidos productosFallidos = new ProductosFallidos()
            .codProducto(DEFAULT_COD_PRODUCTO)
            .campoOrdenCompra(DEFAULT_CAMPO_ORDEN_COMPRA)
            .tipoFallo(DEFAULT_TIPO_FALLO)
            .idOrdenCompra(DEFAULT_ID_ORDEN_COMPRA);
        return productosFallidos;
    }

    @Before
    public void initTest() {
        productosFallidos = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductosFallidos() throws Exception {
        int databaseSizeBeforeCreate = productosFallidosRepository.findAll().size();

        // Create the ProductosFallidos
        ProductosFallidosDTO productosFallidosDTO = productosFallidosMapper.toDto(productosFallidos);
        restProductosFallidosMockMvc.perform(post("/api/productos-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productosFallidosDTO)))
            .andExpect(status().isCreated());

        // Validate the ProductosFallidos in the database
        List<ProductosFallidos> productosFallidosList = productosFallidosRepository.findAll();
        assertThat(productosFallidosList).hasSize(databaseSizeBeforeCreate + 1);
        ProductosFallidos testProductosFallidos = productosFallidosList.get(productosFallidosList.size() - 1);
        assertThat(testProductosFallidos.getCodProducto()).isEqualTo(DEFAULT_COD_PRODUCTO);
        assertThat(testProductosFallidos.getCampoOrdenCompra()).isEqualTo(DEFAULT_CAMPO_ORDEN_COMPRA);
        assertThat(testProductosFallidos.getTipoFallo()).isEqualTo(DEFAULT_TIPO_FALLO);
        assertThat(testProductosFallidos.getIdOrdenCompra()).isEqualTo(DEFAULT_ID_ORDEN_COMPRA);
    }

    @Test
    @Transactional
    public void createProductosFallidosWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productosFallidosRepository.findAll().size();

        // Create the ProductosFallidos with an existing ID
        productosFallidos.setId(1L);
        ProductosFallidosDTO productosFallidosDTO = productosFallidosMapper.toDto(productosFallidos);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductosFallidosMockMvc.perform(post("/api/productos-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productosFallidosDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProductosFallidos in the database
        List<ProductosFallidos> productosFallidosList = productosFallidosRepository.findAll();
        assertThat(productosFallidosList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodProductoIsRequired() throws Exception {
        int databaseSizeBeforeTest = productosFallidosRepository.findAll().size();
        // set the field null
        productosFallidos.setCodProducto(null);

        // Create the ProductosFallidos, which fails.
        ProductosFallidosDTO productosFallidosDTO = productosFallidosMapper.toDto(productosFallidos);

        restProductosFallidosMockMvc.perform(post("/api/productos-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productosFallidosDTO)))
            .andExpect(status().isBadRequest());

        List<ProductosFallidos> productosFallidosList = productosFallidosRepository.findAll();
        assertThat(productosFallidosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCampoOrdenCompraIsRequired() throws Exception {
        int databaseSizeBeforeTest = productosFallidosRepository.findAll().size();
        // set the field null
        productosFallidos.setCampoOrdenCompra(null);

        // Create the ProductosFallidos, which fails.
        ProductosFallidosDTO productosFallidosDTO = productosFallidosMapper.toDto(productosFallidos);

        restProductosFallidosMockMvc.perform(post("/api/productos-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productosFallidosDTO)))
            .andExpect(status().isBadRequest());

        List<ProductosFallidos> productosFallidosList = productosFallidosRepository.findAll();
        assertThat(productosFallidosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoFalloIsRequired() throws Exception {
        int databaseSizeBeforeTest = productosFallidosRepository.findAll().size();
        // set the field null
        productosFallidos.setTipoFallo(null);

        // Create the ProductosFallidos, which fails.
        ProductosFallidosDTO productosFallidosDTO = productosFallidosMapper.toDto(productosFallidos);

        restProductosFallidosMockMvc.perform(post("/api/productos-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productosFallidosDTO)))
            .andExpect(status().isBadRequest());

        List<ProductosFallidos> productosFallidosList = productosFallidosRepository.findAll();
        assertThat(productosFallidosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdOrdenCompraIsRequired() throws Exception {
        int databaseSizeBeforeTest = productosFallidosRepository.findAll().size();
        // set the field null
        productosFallidos.setIdOrdenCompra(null);

        // Create the ProductosFallidos, which fails.
        ProductosFallidosDTO productosFallidosDTO = productosFallidosMapper.toDto(productosFallidos);

        restProductosFallidosMockMvc.perform(post("/api/productos-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productosFallidosDTO)))
            .andExpect(status().isBadRequest());

        List<ProductosFallidos> productosFallidosList = productosFallidosRepository.findAll();
        assertThat(productosFallidosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProductosFallidos() throws Exception {
        // Initialize the database
        productosFallidosRepository.saveAndFlush(productosFallidos);

        // Get all the productosFallidosList
        restProductosFallidosMockMvc.perform(get("/api/productos-fallidos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productosFallidos.getId().intValue())))
            .andExpect(jsonPath("$.[*].codProducto").value(hasItem(DEFAULT_COD_PRODUCTO.toString())))
            .andExpect(jsonPath("$.[*].campoOrdenCompra").value(hasItem(DEFAULT_CAMPO_ORDEN_COMPRA.toString())))
            .andExpect(jsonPath("$.[*].tipoFallo").value(hasItem(DEFAULT_TIPO_FALLO.toString())))
            .andExpect(jsonPath("$.[*].idOrdenCompra").value(hasItem(DEFAULT_ID_ORDEN_COMPRA.intValue())));
    }
    

    @Test
    @Transactional
    public void getProductosFallidos() throws Exception {
        // Initialize the database
        productosFallidosRepository.saveAndFlush(productosFallidos);

        // Get the productosFallidos
        restProductosFallidosMockMvc.perform(get("/api/productos-fallidos/{id}", productosFallidos.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(productosFallidos.getId().intValue()))
            .andExpect(jsonPath("$.codProducto").value(DEFAULT_COD_PRODUCTO.toString()))
            .andExpect(jsonPath("$.campoOrdenCompra").value(DEFAULT_CAMPO_ORDEN_COMPRA.toString()))
            .andExpect(jsonPath("$.tipoFallo").value(DEFAULT_TIPO_FALLO.toString()))
            .andExpect(jsonPath("$.idOrdenCompra").value(DEFAULT_ID_ORDEN_COMPRA.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingProductosFallidos() throws Exception {
        // Get the productosFallidos
        restProductosFallidosMockMvc.perform(get("/api/productos-fallidos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductosFallidos() throws Exception {
        // Initialize the database
        productosFallidosRepository.saveAndFlush(productosFallidos);

        int databaseSizeBeforeUpdate = productosFallidosRepository.findAll().size();

        // Update the productosFallidos
        ProductosFallidos updatedProductosFallidos = productosFallidosRepository.findById(productosFallidos.getId()).get();
        // Disconnect from session so that the updates on updatedProductosFallidos are not directly saved in db
        em.detach(updatedProductosFallidos);
        updatedProductosFallidos
            .codProducto(UPDATED_COD_PRODUCTO)
            .campoOrdenCompra(UPDATED_CAMPO_ORDEN_COMPRA)
            .tipoFallo(UPDATED_TIPO_FALLO)
            .idOrdenCompra(UPDATED_ID_ORDEN_COMPRA);
        ProductosFallidosDTO productosFallidosDTO = productosFallidosMapper.toDto(updatedProductosFallidos);

        restProductosFallidosMockMvc.perform(put("/api/productos-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productosFallidosDTO)))
            .andExpect(status().isOk());

        // Validate the ProductosFallidos in the database
        List<ProductosFallidos> productosFallidosList = productosFallidosRepository.findAll();
        assertThat(productosFallidosList).hasSize(databaseSizeBeforeUpdate);
        ProductosFallidos testProductosFallidos = productosFallidosList.get(productosFallidosList.size() - 1);
        assertThat(testProductosFallidos.getCodProducto()).isEqualTo(UPDATED_COD_PRODUCTO);
        assertThat(testProductosFallidos.getCampoOrdenCompra()).isEqualTo(UPDATED_CAMPO_ORDEN_COMPRA);
        assertThat(testProductosFallidos.getTipoFallo()).isEqualTo(UPDATED_TIPO_FALLO);
        assertThat(testProductosFallidos.getIdOrdenCompra()).isEqualTo(UPDATED_ID_ORDEN_COMPRA);
    }

    @Test
    @Transactional
    public void updateNonExistingProductosFallidos() throws Exception {
        int databaseSizeBeforeUpdate = productosFallidosRepository.findAll().size();

        // Create the ProductosFallidos
        ProductosFallidosDTO productosFallidosDTO = productosFallidosMapper.toDto(productosFallidos);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProductosFallidosMockMvc.perform(put("/api/productos-fallidos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productosFallidosDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProductosFallidos in the database
        List<ProductosFallidos> productosFallidosList = productosFallidosRepository.findAll();
        assertThat(productosFallidosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductosFallidos() throws Exception {
        // Initialize the database
        productosFallidosRepository.saveAndFlush(productosFallidos);

        int databaseSizeBeforeDelete = productosFallidosRepository.findAll().size();

        // Get the productosFallidos
        restProductosFallidosMockMvc.perform(delete("/api/productos-fallidos/{id}", productosFallidos.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ProductosFallidos> productosFallidosList = productosFallidosRepository.findAll();
        assertThat(productosFallidosList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductosFallidos.class);
        ProductosFallidos productosFallidos1 = new ProductosFallidos();
        productosFallidos1.setId(1L);
        ProductosFallidos productosFallidos2 = new ProductosFallidos();
        productosFallidos2.setId(productosFallidos1.getId());
        assertThat(productosFallidos1).isEqualTo(productosFallidos2);
        productosFallidos2.setId(2L);
        assertThat(productosFallidos1).isNotEqualTo(productosFallidos2);
        productosFallidos1.setId(null);
        assertThat(productosFallidos1).isNotEqualTo(productosFallidos2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductosFallidosDTO.class);
        ProductosFallidosDTO productosFallidosDTO1 = new ProductosFallidosDTO();
        productosFallidosDTO1.setId(1L);
        ProductosFallidosDTO productosFallidosDTO2 = new ProductosFallidosDTO();
        assertThat(productosFallidosDTO1).isNotEqualTo(productosFallidosDTO2);
        productosFallidosDTO2.setId(productosFallidosDTO1.getId());
        assertThat(productosFallidosDTO1).isEqualTo(productosFallidosDTO2);
        productosFallidosDTO2.setId(2L);
        assertThat(productosFallidosDTO1).isNotEqualTo(productosFallidosDTO2);
        productosFallidosDTO1.setId(null);
        assertThat(productosFallidosDTO1).isNotEqualTo(productosFallidosDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(productosFallidosMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(productosFallidosMapper.fromId(null)).isNull();
    }
}
