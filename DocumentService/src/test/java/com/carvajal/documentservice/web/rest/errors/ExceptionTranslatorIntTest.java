package com.carvajal.documentservice.web.rest.errors;

import com.carvajal.documentservice.DocumentServiceApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.zalando.problem.spring.web.advice.MediaTypes;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for the ExceptionTranslator controller advice.
 *
 * @see ExceptionTranslator
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DocumentServiceApp.class)
public class ExceptionTranslatorIntTest {

    @Autowired
    private ExceptionTranslatorTestController controller;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter)
            .build();
    }

    @Test
    public void testConcurrencyFailure() {
        try {
			mockMvc.perform(get("/test/concurrency-failure"))
			    .andExpect(status().isConflict())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value(ErrorConstants.ERR_CONCURRENCY_FAILURE));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testMethodArgumentNotValid() {
         try {
			mockMvc.perform(post("/test/method-argument").content("{}").contentType(MediaType.APPLICATION_JSON))
			     .andExpect(status().isBadRequest())
			     .andExpect(content().contentType(MediaTypes.PROBLEM))
			     .andExpect(jsonPath("$.message").value(ErrorConstants.ERR_VALIDATION))
			     .andExpect(jsonPath("$.fieldErrors.[0].objectName").value("testDTO"))
			     .andExpect(jsonPath("$.fieldErrors.[0].field").value("test"))
			     .andExpect(jsonPath("$.fieldErrors.[0].message").value("NotNull"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testParameterizedError() {
        try {
			mockMvc.perform(get("/test/parameterized-error"))
			    .andExpect(status().isBadRequest())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value("test parameterized error"))
			    .andExpect(jsonPath("$.params.param0").value("param0_value"))
			    .andExpect(jsonPath("$.params.param1").value("param1_value"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testParameterizedError2() {
        try {
			mockMvc.perform(get("/test/parameterized-error2"))
			    .andExpect(status().isBadRequest())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value("test parameterized error"))
			    .andExpect(jsonPath("$.params.foo").value("foo_value"))
			    .andExpect(jsonPath("$.params.bar").value("bar_value"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testMissingServletRequestPartException()  {
        try {
			mockMvc.perform(get("/test/missing-servlet-request-part"))
			    .andExpect(status().isBadRequest())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value("error.http.400"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testMissingServletRequestParameterException() {
        try {
			mockMvc.perform(get("/test/missing-servlet-request-parameter"))
			    .andExpect(status().isBadRequest())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value("error.http.400"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testAccessDenied() {
        try {
			mockMvc.perform(get("/test/access-denied"))
			    .andExpect(status().isForbidden())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value("error.http.403"))
			    .andExpect(jsonPath("$.detail").value("test access denied!"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testUnauthorized() {
        try {
			mockMvc.perform(get("/test/unauthorized"))
			    .andExpect(status().isUnauthorized())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value("error.http.401"))
			    .andExpect(jsonPath("$.path").value("/test/unauthorized"))
			    .andExpect(jsonPath("$.detail").value("test authentication failed!"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testMethodNotSupported() {
        try {
			mockMvc.perform(post("/test/access-denied"))
			    .andExpect(status().isMethodNotAllowed())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value("error.http.405"))
			    .andExpect(jsonPath("$.detail").value("Request method 'POST' not supported"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testExceptionWithResponseStatus() {
        try {
			mockMvc.perform(get("/test/response-status"))
			    .andExpect(status().isBadRequest())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value("error.http.400"))
			    .andExpect(jsonPath("$.title").value("test response status"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Test
    public void testInternalServerError() {
        try {
			mockMvc.perform(get("/test/internal-server-error"))
			    .andExpect(status().isInternalServerError())
			    .andExpect(content().contentType(MediaTypes.PROBLEM))
			    .andExpect(jsonPath("$.message").value("error.http.500"))
			    .andExpect(jsonPath("$.title").value("Internal Server Error"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
