package com.carvajal.documentservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.documentservice.service.ProductosFallidosService;
import com.carvajal.documentservice.service.dto.ProductosFallidosDTO;
import com.carvajal.documentservice.web.rest.errors.BadRequestAlertException;
import com.carvajal.documentservice.web.rest.util.HeaderUtil;
import com.carvajal.documentservice.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing ProductosFallidos.
 */
@RestController
@RequestMapping("/api")
public class ProductosFallidosResource {

    private final Logger log = LoggerFactory.getLogger(ProductosFallidosResource.class);

    private static final String ENTITY_NAME = "productosFallidos";

    private final ProductosFallidosService productosFallidosService;

    public ProductosFallidosResource(ProductosFallidosService productosFallidosService) {
        this.productosFallidosService = productosFallidosService;
    }

    /**
     * POST  /productos-fallidos : Create a new productosFallidos.
     *
     * @param productosFallidosDTO the productosFallidosDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productosFallidosDTO, or with status 400 (Bad Request) if the productosFallidos has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/productos-fallidos")
    @Timed
    public ResponseEntity<ProductosFallidosDTO> createProductosFallidos(@Valid @RequestBody ProductosFallidosDTO productosFallidosDTO) throws URISyntaxException {
        log.debug("REST request to save ProductosFallidos : {}", productosFallidosDTO);
        if (productosFallidosDTO.getId() != null) {
            throw new BadRequestAlertException("A new productosFallidos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductosFallidosDTO result = productosFallidosService.save(productosFallidosDTO);
        return ResponseEntity.created(new URI("/api/productos-fallidos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /productos-fallidos : Updates an existing productosFallidos.
     *
     * @param productosFallidosDTO the productosFallidosDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productosFallidosDTO,
     * or with status 400 (Bad Request) if the productosFallidosDTO is not valid,
     * or with status 500 (Internal Server Error) if the productosFallidosDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/productos-fallidos")
    @Timed
    public ResponseEntity<ProductosFallidosDTO> updateProductosFallidos(@Valid @RequestBody ProductosFallidosDTO productosFallidosDTO) throws URISyntaxException {
        log.debug("REST request to update ProductosFallidos : {}", productosFallidosDTO);
        if (productosFallidosDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductosFallidosDTO result = productosFallidosService.save(productosFallidosDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productosFallidosDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /productos-fallidos : get all the productosFallidos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productosFallidos in body
     */
    @GetMapping("/productos-fallidos")
    @Timed
    public ResponseEntity<List<ProductosFallidosDTO>> getAllProductosFallidos(Pageable pageable) {
        log.debug("REST request to get a page of ProductosFallidos");
        Page<ProductosFallidosDTO> page = productosFallidosService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/productos-fallidos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /productos-fallidos/:id : get the "id" productosFallidos.
     *
     * @param id the id of the productosFallidosDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productosFallidosDTO, or with status 404 (Not Found)
     */
    @GetMapping("/productos-fallidos/{id}")
    @Timed
    public ResponseEntity<ProductosFallidosDTO> getProductosFallidos(@PathVariable Long id) {
        log.debug("REST request to get ProductosFallidos : {}", id);
        Optional<ProductosFallidosDTO> productosFallidosDTO = productosFallidosService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productosFallidosDTO);
    }

    /**
     * DELETE  /productos-fallidos/:id : delete the "id" productosFallidos.
     *
     * @param id the id of the productosFallidosDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/productos-fallidos/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductosFallidos(@PathVariable Long id) {
        log.debug("REST request to delete ProductosFallidos : {}", id);
        productosFallidosService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
