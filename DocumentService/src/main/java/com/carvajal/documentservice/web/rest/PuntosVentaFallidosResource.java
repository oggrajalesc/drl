package com.carvajal.documentservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.documentservice.service.PuntosVentaFallidosService;
import com.carvajal.documentservice.service.dto.PuntosVentaFallidosDTO;
import com.carvajal.documentservice.web.rest.errors.BadRequestAlertException;
import com.carvajal.documentservice.web.rest.util.HeaderUtil;
import com.carvajal.documentservice.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PuntosVentaFallidos.
 */
@RestController
@RequestMapping("/api")
public class PuntosVentaFallidosResource {

    private final Logger log = LoggerFactory.getLogger(PuntosVentaFallidosResource.class);

    private static final String ENTITY_NAME = "puntosVentaFallidos";

    private final PuntosVentaFallidosService puntosVentaFallidosService;

    public PuntosVentaFallidosResource(PuntosVentaFallidosService puntosVentaFallidosService) {
        this.puntosVentaFallidosService = puntosVentaFallidosService;
    }

    /**
     * POST  /puntos-venta-fallidos : Create a new puntosVentaFallidos.
     *
     * @param puntosVentaFallidosDTO the puntosVentaFallidosDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new puntosVentaFallidosDTO, or with status 400 (Bad Request) if the puntosVentaFallidos has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/puntos-venta-fallidos")
    @Timed
    public ResponseEntity<PuntosVentaFallidosDTO> createPuntosVentaFallidos(@Valid @RequestBody PuntosVentaFallidosDTO puntosVentaFallidosDTO) throws URISyntaxException {
        log.debug("REST request to save PuntosVentaFallidos : {}", puntosVentaFallidosDTO);
        if (puntosVentaFallidosDTO.getId() != null) {
            throw new BadRequestAlertException("A new puntosVentaFallidos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PuntosVentaFallidosDTO result = puntosVentaFallidosService.save(puntosVentaFallidosDTO);
        return ResponseEntity.created(new URI("/api/puntos-venta-fallidos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /puntos-venta-fallidos : Updates an existing puntosVentaFallidos.
     *
     * @param puntosVentaFallidosDTO the puntosVentaFallidosDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated puntosVentaFallidosDTO,
     * or with status 400 (Bad Request) if the puntosVentaFallidosDTO is not valid,
     * or with status 500 (Internal Server Error) if the puntosVentaFallidosDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/puntos-venta-fallidos")
    @Timed
    public ResponseEntity<PuntosVentaFallidosDTO> updatePuntosVentaFallidos(@Valid @RequestBody PuntosVentaFallidosDTO puntosVentaFallidosDTO) throws URISyntaxException {
        log.debug("REST request to update PuntosVentaFallidos : {}", puntosVentaFallidosDTO);
        if (puntosVentaFallidosDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PuntosVentaFallidosDTO result = puntosVentaFallidosService.save(puntosVentaFallidosDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, puntosVentaFallidosDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /puntos-venta-fallidos : get all the puntosVentaFallidos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of puntosVentaFallidos in body
     */
    @GetMapping("/puntos-venta-fallidos")
    @Timed
    public ResponseEntity<List<PuntosVentaFallidosDTO>> getAllPuntosVentaFallidos(Pageable pageable) {
        log.debug("REST request to get a page of PuntosVentaFallidos");
        Page<PuntosVentaFallidosDTO> page = puntosVentaFallidosService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/puntos-venta-fallidos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /puntos-venta-fallidos/:id : get the "id" puntosVentaFallidos.
     *
     * @param id the id of the puntosVentaFallidosDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the puntosVentaFallidosDTO, or with status 404 (Not Found)
     */
    @GetMapping("/puntos-venta-fallidos/{id}")
    @Timed
    public ResponseEntity<PuntosVentaFallidosDTO> getPuntosVentaFallidos(@PathVariable Long id) {
        log.debug("REST request to get PuntosVentaFallidos : {}", id);
        Optional<PuntosVentaFallidosDTO> puntosVentaFallidosDTO = puntosVentaFallidosService.findOne(id);
        return ResponseUtil.wrapOrNotFound(puntosVentaFallidosDTO);
    }

    /**
     * DELETE  /puntos-venta-fallidos/:id : delete the "id" puntosVentaFallidos.
     *
     * @param id the id of the puntosVentaFallidosDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/puntos-venta-fallidos/{id}")
    @Timed
    public ResponseEntity<Void> deletePuntosVentaFallidos(@PathVariable Long id) {
        log.debug("REST request to delete PuntosVentaFallidos : {}", id);
        puntosVentaFallidosService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
