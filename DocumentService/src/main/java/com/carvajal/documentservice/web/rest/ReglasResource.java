package com.carvajal.documentservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.documentservice.service.ReglasService;
import com.carvajal.documentservice.service.dto.ReglasDTO;
import com.carvajal.documentservice.web.rest.errors.BadRequestAlertException;
import com.carvajal.documentservice.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Reglas.
 */
@RestController
@RequestMapping("/api")
public class ReglasResource {

    private final Logger log = LoggerFactory.getLogger(ReglasResource.class);

    private static final String ENTITY_NAME = "reglas";

    private final ReglasService reglasService;

    public ReglasResource(ReglasService reglasService) {
        this.reglasService = reglasService;
    }

    /**
     * POST  /reglas : Create a new reglas.
     *
     * @param reglasDTO the reglasDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reglasDTO, or with status 400 (Bad Request) if the reglas has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reglas")
    @Timed
    public ResponseEntity<ReglasDTO> createReglas(@Valid @RequestBody ReglasDTO reglasDTO) throws URISyntaxException {
        log.debug("REST request to save Reglas : {}", reglasDTO);
        if (reglasDTO.getId() != null) {
            throw new BadRequestAlertException("A new reglas cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReglasDTO result = reglasService.save(reglasDTO);
        return ResponseEntity.created(new URI("/api/reglas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /reglas : Updates an existing reglas.
     *
     * @param reglasDTO the reglasDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reglasDTO,
     * or with status 400 (Bad Request) if the reglasDTO is not valid,
     * or with status 500 (Internal Server Error) if the reglasDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/reglas")
    @Timed
    public ResponseEntity<ReglasDTO> updateReglas(@Valid @RequestBody ReglasDTO reglasDTO) throws URISyntaxException {
        log.debug("REST request to update Reglas : {}", reglasDTO);
        if (reglasDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ReglasDTO result = reglasService.save(reglasDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, reglasDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /reglas : get all the reglas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of reglas in body
     */
    @GetMapping("/reglas")
    @Timed
    public List<ReglasDTO> getAllReglas() {
        log.debug("REST request to get all Reglas");
        return reglasService.findAll();
    }

    /**
     * GET  /reglas/:id : get the "id" reglas.
     *
     * @param id the id of the reglasDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reglasDTO, or with status 404 (Not Found)
     */
    @GetMapping("/reglas/{id}")
    @Timed
    public ResponseEntity<ReglasDTO> getReglas(@PathVariable Long id) {
        log.debug("REST request to get Reglas : {}", id);
        Optional<ReglasDTO> reglasDTO = reglasService.findOne(id);
        return ResponseUtil.wrapOrNotFound(reglasDTO);
    }

    /**
     * DELETE  /reglas/:id : delete the "id" reglas.
     *
     * @param id the id of the reglasDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/reglas/{id}")
    @Timed
    public ResponseEntity<Void> deleteReglas(@PathVariable Long id) {
        log.debug("REST request to delete Reglas : {}", id);
        reglasService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
