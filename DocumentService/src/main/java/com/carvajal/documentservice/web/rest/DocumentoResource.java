package com.carvajal.documentservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.documentservice.service.DocumentoService;
import com.carvajal.documentservice.service.dto.DocumentoDTO;
import com.carvajal.documentservice.web.rest.errors.BadRequestAlertException;
import com.carvajal.documentservice.web.rest.util.HeaderUtil;
import com.carvajal.documentservice.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Documento.
 */
@RestController
@RequestMapping("/api")
public class DocumentoResource {

    private final Logger log = LoggerFactory.getLogger(DocumentoResource.class);

    private static final String ENTITY_NAME = "documento";

    private final DocumentoService documentoService;

    public DocumentoResource(DocumentoService documentoService) {
        this.documentoService = documentoService;
    }

    /**
     * POST  /documentos : Create a new documento.
     *
     * @param documentoDTO the documentoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documentoDTO, or with status 400 (Bad Request) if the documento has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/documentos")
    @Timed
    public ResponseEntity<DocumentoDTO> createDocumento(@Valid @RequestBody DocumentoDTO documentoDTO) throws URISyntaxException {
        log.debug("REST request to save Documento : {}", documentoDTO);
        if (documentoDTO.getId() != null) {
            throw new BadRequestAlertException("A new documento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DocumentoDTO result = documentoService.save(documentoDTO);
        return ResponseEntity.created(new URI("/api/documentos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /documentos : Updates an existing documento.
     *
     * @param documentoDTO the documentoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated documentoDTO,
     * or with status 400 (Bad Request) if the documentoDTO is not valid,
     * or with status 500 (Internal Server Error) if the documentoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/documentos")
    @Timed
    public ResponseEntity<DocumentoDTO> updateDocumento(@Valid @RequestBody DocumentoDTO documentoDTO) throws URISyntaxException {
        log.debug("REST request to update Documento : {}", documentoDTO);
        if (documentoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DocumentoDTO result = documentoService.save(documentoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, documentoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /documentos : get all the documentos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of documentos in body
     */
    @GetMapping("/documentos")
    @Timed
    public ResponseEntity<List<DocumentoDTO>> getAllDocumentos(Pageable pageable) {
        log.debug("REST request to get a page of Documentos");
        Page<DocumentoDTO> page = documentoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/documentos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    
    /**
     * GET  /documentos/:id : get the "id" documento.
     *
     * @param id the id of the documentoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the documentoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/documentos/{id}")
    @Timed
    public ResponseEntity<DocumentoDTO> getDocumento(@PathVariable Long id) {
    	log.debug("REST request to get Documento : {}", id);
    	Optional<DocumentoDTO> documentoDTO = documentoService.findOne(id);
    	return ResponseUtil.wrapOrNotFound(documentoDTO);
    }

    /**
     * GET  /documentos/:id : get the "id" documento.
     *
     * @param nameFile the id of the documentoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the documentoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/documentos/nameFile/{nameFile}")
    @Timed
    public ResponseEntity<DocumentoDTO> getDocumento(@PathVariable String nameFile) {
        log.debug("REST request to get Documento : {}", nameFile);
        Optional<DocumentoDTO> documentoDTO = documentoService.findByNombreDocumento(nameFile);
        return ResponseUtil.wrapOrNotFound(documentoDTO);
    }

    /**
     * DELETE  /documentos/:id : delete the "id" documento.
     *
     * @param id the id of the documentoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/documentos/{id}")
    @Timed
    public ResponseEntity<Void> deleteDocumento(@PathVariable Long id) {
        log.debug("REST request to delete Documento : {}", id);
        documentoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
