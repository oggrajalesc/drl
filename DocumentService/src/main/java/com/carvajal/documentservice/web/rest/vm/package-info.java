/**
 * View Models used by Spring MVC REST controllers.
 */
package com.carvajal.documentservice.web.rest.vm;
