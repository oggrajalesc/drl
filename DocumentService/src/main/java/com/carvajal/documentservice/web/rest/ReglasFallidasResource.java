package com.carvajal.documentservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.documentservice.service.ReglasFallidasService;
import com.carvajal.documentservice.service.dto.ReglasFallidasDTO;
import com.carvajal.documentservice.web.rest.errors.BadRequestAlertException;
import com.carvajal.documentservice.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing ReglasFallidas.
 */
@RestController
@RequestMapping("/api")
public class ReglasFallidasResource {

    private final Logger log = LoggerFactory.getLogger(ReglasFallidasResource.class);

    private static final String ENTITY_NAME = "reglasFallidas";

    private final ReglasFallidasService reglasFallidasService;

    public ReglasFallidasResource(ReglasFallidasService reglasFallidasService) {
        this.reglasFallidasService = reglasFallidasService;
    }

    /**
     * POST  /reglas-fallidas : Create a new reglasFallidas.
     *
     * @param reglasFallidasDTO the reglasFallidasDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reglasFallidasDTO, or with status 400 (Bad Request) if the reglasFallidas has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reglas-fallidas")
    @Timed
    public ResponseEntity<ReglasFallidasDTO> createReglasFallidas(@Valid @RequestBody ReglasFallidasDTO reglasFallidasDTO) throws URISyntaxException {
        log.debug("REST request to save ReglasFallidas : {}", reglasFallidasDTO);
        if (reglasFallidasDTO.getId() != null) {
            throw new BadRequestAlertException("A new reglasFallidas cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReglasFallidasDTO result = reglasFallidasService.save(reglasFallidasDTO);
        return ResponseEntity.created(new URI("/api/reglas-fallidas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /reglas-fallidas : Updates an existing reglasFallidas.
     *
     * @param reglasFallidasDTO the reglasFallidasDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reglasFallidasDTO,
     * or with status 400 (Bad Request) if the reglasFallidasDTO is not valid,
     * or with status 500 (Internal Server Error) if the reglasFallidasDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/reglas-fallidas")
    @Timed
    public ResponseEntity<ReglasFallidasDTO> updateReglasFallidas(@Valid @RequestBody ReglasFallidasDTO reglasFallidasDTO) throws URISyntaxException {
        log.debug("REST request to update ReglasFallidas : {}", reglasFallidasDTO);
        if (reglasFallidasDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ReglasFallidasDTO result = reglasFallidasService.save(reglasFallidasDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, reglasFallidasDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /reglas-fallidas : get all the reglasFallidas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of reglasFallidas in body
     */
    @GetMapping("/reglas-fallidas")
    @Timed
    public List<ReglasFallidasDTO> getAllReglasFallidas() {
        log.debug("REST request to get all ReglasFallidas");
        return reglasFallidasService.findAll();
    }

    /**
     * GET  /reglas-fallidas/:id : get the "id" reglasFallidas.
     *
     * @param id the id of the reglasFallidasDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reglasFallidasDTO, or with status 404 (Not Found)
     */
    @GetMapping("/reglas-fallidas/{id}")
    @Timed
    public ResponseEntity<ReglasFallidasDTO> getReglasFallidas(@PathVariable Long id) {
        log.debug("REST request to get ReglasFallidas : {}", id);
        Optional<ReglasFallidasDTO> reglasFallidasDTO = reglasFallidasService.findOne(id);
        return ResponseUtil.wrapOrNotFound(reglasFallidasDTO);
    }

    /**
     * DELETE  /reglas-fallidas/:id : delete the "id" reglasFallidas.
     *
     * @param id the id of the reglasFallidasDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/reglas-fallidas/{id}")
    @Timed
    public ResponseEntity<Void> deleteReglasFallidas(@PathVariable Long id) {
        log.debug("REST request to delete ReglasFallidas : {}", id);
        reglasFallidasService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
