package com.carvajal.documentservice.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A PuntosVentaFallidos.
 */
@Entity
@Table(name = "puntos_venta_fallidos")
public class PuntosVentaFallidos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "cod_comercio", nullable = false)
    private String codComercio;

    @NotNull
    @Column(name = "campo_orden_compra", nullable = false)
    private String campoOrdenCompra;

    @NotNull
    @Column(name = "tipo_fallo", nullable = false)
    private String tipoFallo;

    @NotNull
    @Column(name = "id_orden_compra", nullable = false)
    private Long idOrdenCompra;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Documento ordenCompra;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodComercio() {
        return codComercio;
    }

    public PuntosVentaFallidos codComercio(String codComercio) {
        this.codComercio = codComercio;
        return this;
    }

    public void setCodComercio(String codComercio) {
        this.codComercio = codComercio;
    }

    public String getCampoOrdenCompra() {
        return campoOrdenCompra;
    }

    public PuntosVentaFallidos campoOrdenCompra(String campoOrdenCompra) {
        this.campoOrdenCompra = campoOrdenCompra;
        return this;
    }

    public void setCampoOrdenCompra(String campoOrdenCompra) {
        this.campoOrdenCompra = campoOrdenCompra;
    }

    public String getTipoFallo() {
        return tipoFallo;
    }

    public PuntosVentaFallidos tipoFallo(String tipoFallo) {
        this.tipoFallo = tipoFallo;
        return this;
    }

    public void setTipoFallo(String tipoFallo) {
        this.tipoFallo = tipoFallo;
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public PuntosVentaFallidos idOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
        return this;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Documento getOrdenCompra() {
        return ordenCompra;
    }

    public PuntosVentaFallidos ordenCompra(Documento documento) {
        this.ordenCompra = documento;
        return this;
    }

    public void setOrdenCompra(Documento documento) {
        this.ordenCompra = documento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PuntosVentaFallidos puntosVentaFallidos = (PuntosVentaFallidos) o;
        if (puntosVentaFallidos.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), puntosVentaFallidos.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PuntosVentaFallidos{" +
            "id=" + getId() +
            ", codComercio='" + getCodComercio() + "'" +
            ", campoOrdenCompra='" + getCampoOrdenCompra() + "'" +
            ", tipoFallo='" + getTipoFallo() + "'" +
            ", idOrdenCompra=" + getIdOrdenCompra() +
            "}";
    }
}
