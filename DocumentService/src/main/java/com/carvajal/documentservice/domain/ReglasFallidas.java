package com.carvajal.documentservice.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A ReglasFallidas.
 */
@Entity
@Table(name = "reglas_fallidas")
public class ReglasFallidas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "fecha", nullable = false)
    private Instant fecha;

    @NotNull
    @Column(name = "tipo", nullable = false)
    private String tipo;

    @NotNull
    @Column(name = "id_orden_compra", nullable = false)
    private Long idOrdenCompra;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Documento ordenCompra;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFecha() {
        return fecha;
    }

    public ReglasFallidas fecha(Instant fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(Instant fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public ReglasFallidas tipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public ReglasFallidas idOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
        return this;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Documento getOrdenCompra() {
        return ordenCompra;
    }

    public ReglasFallidas ordenCompra(Documento documento) {
        this.ordenCompra = documento;
        return this;
    }

    public void setOrdenCompra(Documento documento) {
        this.ordenCompra = documento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReglasFallidas reglasFallidas = (ReglasFallidas) o;
        if (reglasFallidas.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reglasFallidas.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReglasFallidas{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", idOrdenCompra=" + getIdOrdenCompra() +
            "}";
    }
}
