package com.carvajal.documentservice.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;

/**
 * not an ignored comment
 */
@ApiModel(description = "not an ignored comment")
@Entity
@Table(name = "documento")
public class Documento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_documento_padre")
    private Long idDocumentoPadre;

    @NotNull
    @Column(name = "nombre_documento", nullable = false)
    private String nombreDocumento;

    @Column(name = "tipo_documento")
    private String tipoDocumento;

    @NotNull
    @Column(name = "fecha_documento", nullable = false)
    private Date fechaDocumento;

    @Column(name = "fecha_entrega", nullable = false)
    private Date fechaEntrega;

    @Column(name = "cod_pais", nullable = false)
    private String codPais;

    @Column(name = "emisor", nullable = false)
    private String emisor;

    @Column(name = "receptor", nullable = false)
    private String receptor;

    @NotNull
    @Column(name = "estado", nullable = false)
    private String estado;

    @Column(name = "id_document_storage")
    private String idDocumentStorage;

    @Column(name = "id_dynamo")
    private String idDynamo;

    @Column(name = "formato")
    private String formato;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Documento documentoPadre;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdDocumentoPadre() {
        return idDocumentoPadre;
    }

    public Documento idDocumentoPadre(Long idDocumentoPadre) {
        this.idDocumentoPadre = idDocumentoPadre;
        return this;
    }

    public void setIdDocumentoPadre(Long idDocumentoPadre) {
        this.idDocumentoPadre = idDocumentoPadre;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public Documento nombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
        return this;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public Documento tipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
        return this;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public Documento fechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
        return this;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public Documento fechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
        return this;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getCodPais() {
        return codPais;
    }

    public Documento codPais(String codPais) {
        this.codPais = codPais;
        return this;
    }

    public void setCodPais(String codPais) {
        this.codPais = codPais;
    }

    public String getEmisor() {
        return emisor;
    }

    public Documento emisor(String emisor) {
        this.emisor = emisor;
        return this;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getReceptor() {
        return receptor;
    }

    public Documento receptor(String receptor) {
        this.receptor = receptor;
        return this;
    }

    public void setReceptor(String receptor) {
        this.receptor = receptor;
    }

    public String getEstado() {
        return estado;
    }

    public Documento estado(String estado) {
        this.estado = estado;
        return this;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdDocumentStorage() {
        return idDocumentStorage;
    }

    public Documento idDocumentStorage(String idDocumentStorage) {
        this.idDocumentStorage = idDocumentStorage;
        return this;
    }

    public void setIdDocumentStorage(String idDocumentStorage) {
        this.idDocumentStorage = idDocumentStorage;
    }

    public String getIdDynamo() {
        return idDynamo;
    }

    public Documento idDynamo(String idDynamo) {
        this.idDynamo = idDynamo;
        return this;
    }

    public void setIdDynamo(String idDynamo) {
        this.idDynamo = idDynamo;
    }

    public String getFormato() {
        return formato;
    }

    public Documento formato(String formato) {
        this.formato = formato;
        return this;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public Documento getDocumentoPadre() {
        return documentoPadre;
    }

    public Documento documentoPadre(Documento documento) {
        this.documentoPadre = documento;
        return this;
    }

    public void setDocumentoPadre(Documento documento) {
        this.documentoPadre = documento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Documento documento = (Documento) o;
        if (documento.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documento.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Documento{" +
            "id=" + getId() +
            ", idDocumentoPadre=" + getIdDocumentoPadre() +
            ", nombreDocumento='" + getNombreDocumento() + "'" +
            ", tipoDocumento='" + getTipoDocumento() + "'" +
            ", fechaDocumento='" + getFechaDocumento() + "'" +
            ", fechaEntrega='" + getFechaEntrega() + "'" +
            ", codPais='" + getCodPais() + "'" +
            ", emisor='" + getEmisor() + "'" +
            ", receptor='" + getReceptor() + "'" +
            ", estado='" + getEstado() + "'" +
            ", idDocumentStorage='" + getIdDocumentStorage() + "'" +
            ", idDynamo='" + getIdDynamo() + "'" +
            ", formato='" + getFormato() + "'" +
            "}";
    }
}
