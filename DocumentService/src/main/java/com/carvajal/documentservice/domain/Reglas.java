package com.carvajal.documentservice.domain;


import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Reglas.
 */
@Entity
@Table(name = "reglas")
public class Reglas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "cod_regla", nullable = false)
    private String codRegla;

    @NotNull
    @Column(name = "descripcion", nullable = false)
    private String descripcion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodRegla() {
        return codRegla;
    }

    public Reglas codRegla(String codRegla) {
        this.codRegla = codRegla;
        return this;
    }

    public void setCodRegla(String codRegla) {
        this.codRegla = codRegla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Reglas descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reglas reglas = (Reglas) o;
        if (reglas.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reglas.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Reglas{" +
            "id=" + getId() +
            ", codRegla='" + getCodRegla() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            "}";
    }
}
