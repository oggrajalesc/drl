package com.carvajal.documentservice.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.carvajal.documentservice.domain.Documento;
import com.carvajal.documentservice.service.dto.DocumentoDTO;

/**
 * Mapper for the entity Documento and its DTO DocumentoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DocumentoMapper extends EntityMapper<DocumentoDTO, Documento> {

    @Mapping(source = "documentoPadre.id", target = "documentoPadreId")
    DocumentoDTO toDto(Documento documento);

    @Mapping(source = "documentoPadreId", target = "documentoPadre")
    Documento toEntity(DocumentoDTO documentoDTO);

    default Documento fromId(Long id) {
        if (id == null) {
            return null;
        }
        Documento documento = new Documento();
        documento.setId(id);
        return documento;
    }
}
