package com.carvajal.documentservice.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carvajal.documentservice.domain.Reglas;
import com.carvajal.documentservice.repository.ReglasRepository;
import com.carvajal.documentservice.service.ReglasService;
import com.carvajal.documentservice.service.dto.ReglasDTO;
import com.carvajal.documentservice.service.mapper.ReglasMapper;
/**
 * Service Implementation for managing Reglas.
 */
@Service
@Transactional
public class ReglasServiceImpl implements ReglasService {

    private final Logger log = LoggerFactory.getLogger(ReglasServiceImpl.class);

    private final ReglasRepository reglasRepository;

    private final ReglasMapper reglasMapper;

    public ReglasServiceImpl(ReglasRepository reglasRepository, ReglasMapper reglasMapper) {
        this.reglasRepository = reglasRepository;
        this.reglasMapper = reglasMapper;
    }

    /**
     * Save a reglas.
     *
     * @param reglasDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ReglasDTO save(ReglasDTO reglasDTO) {
        log.debug("Request to save Reglas : {}", reglasDTO);
        Reglas reglas = reglasMapper.toEntity(reglasDTO);
        reglas = reglasRepository.save(reglas);
        return reglasMapper.toDto(reglas);
    }

    /**
     * Get all the reglas.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ReglasDTO> findAll() {
        log.debug("Request to get all Reglas");
        return reglasRepository.findAll().stream()
            .map(reglasMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one reglas by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ReglasDTO> findOne(Long id) {
        log.debug("Request to get Reglas : {}", id);
        return reglasRepository.findById(id)
            .map(reglasMapper::toDto);
    }

    /**
     * Delete the reglas by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Reglas : {}", id);
        reglasRepository.deleteById(id);
    }
}
