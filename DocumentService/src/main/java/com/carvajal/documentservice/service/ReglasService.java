package com.carvajal.documentservice.service;

import java.util.List;
import java.util.Optional;

import com.carvajal.documentservice.service.dto.ReglasDTO;

/**
 * Service Interface for managing Reglas.
 */
public interface ReglasService {

    /**
     * Save a reglas.
     *
     * @param reglasDTO the entity to save
     * @return the persisted entity
     */
    ReglasDTO save(ReglasDTO reglasDTO);

    /**
     * Get all the reglas.
     *
     * @return the list of entities
     */
    List<ReglasDTO> findAll();


    /**
     * Get the "id" reglas.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ReglasDTO> findOne(Long id);

    /**
     * Delete the "id" reglas.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
