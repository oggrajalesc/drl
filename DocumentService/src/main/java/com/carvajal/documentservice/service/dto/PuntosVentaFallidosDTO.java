package com.carvajal.documentservice.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the PuntosVentaFallidos entity.
 */
public class PuntosVentaFallidosDTO implements Serializable {

    private Long id;

    @NotNull
    private String codComercio;

    @NotNull
    private String campoOrdenCompra;

    @NotNull
    private String tipoFallo;

    @NotNull
    private Long idOrdenCompra;

    private Long ordenCompraId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodComercio() {
        return codComercio;
    }

    public void setCodComercio(String codComercio) {
        this.codComercio = codComercio;
    }

    public String getCampoOrdenCompra() {
        return campoOrdenCompra;
    }

    public void setCampoOrdenCompra(String campoOrdenCompra) {
        this.campoOrdenCompra = campoOrdenCompra;
    }

    public String getTipoFallo() {
        return tipoFallo;
    }

    public void setTipoFallo(String tipoFallo) {
        this.tipoFallo = tipoFallo;
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Long getOrdenCompraId() {
        return ordenCompraId;
    }

    public void setOrdenCompraId(Long documentoId) {
        this.ordenCompraId = documentoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PuntosVentaFallidosDTO puntosVentaFallidosDTO = (PuntosVentaFallidosDTO) o;
        if (puntosVentaFallidosDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), puntosVentaFallidosDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PuntosVentaFallidosDTO{" +
            "id=" + getId() +
            ", codComercio='" + getCodComercio() + "'" +
            ", campoOrdenCompra='" + getCampoOrdenCompra() + "'" +
            ", tipoFallo='" + getTipoFallo() + "'" +
            ", idOrdenCompra=" + getIdOrdenCompra() +
            ", ordenCompra=" + getOrdenCompraId() +
            "}";
    }
}
