package com.carvajal.documentservice.service.mapper;

import org.mapstruct.Mapper;

import com.carvajal.documentservice.domain.Reglas;
import com.carvajal.documentservice.service.dto.ReglasDTO;

/**
 * Mapper for the entity Reglas and its DTO ReglasDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReglasMapper extends EntityMapper<ReglasDTO, Reglas> {



    default Reglas fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reglas reglas = new Reglas();
        reglas.setId(id);
        return reglas;
    }
}
