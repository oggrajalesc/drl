package com.carvajal.documentservice.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Reglas entity.
 */
public class ReglasDTO implements Serializable {

    private Long id;

    @NotNull
    private String codRegla;

    @NotNull
    private String descripcion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodRegla() {
        return codRegla;
    }

    public void setCodRegla(String codRegla) {
        this.codRegla = codRegla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReglasDTO reglasDTO = (ReglasDTO) o;
        if (reglasDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reglasDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReglasDTO{" +
            "id=" + getId() +
            ", codRegla='" + getCodRegla() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            "}";
    }
}
