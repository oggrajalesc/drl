package com.carvajal.documentservice.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.carvajal.documentservice.domain.PuntosVentaFallidos;
import com.carvajal.documentservice.service.dto.PuntosVentaFallidosDTO;

/**
 * Mapper for the entity PuntosVentaFallidos and its DTO PuntosVentaFallidosDTO.
 */
@Mapper(componentModel = "spring", uses = {DocumentoMapper.class})
public interface PuntosVentaFallidosMapper extends EntityMapper<PuntosVentaFallidosDTO, PuntosVentaFallidos> {

    @Mapping(source = "ordenCompra.id", target = "ordenCompraId")
    PuntosVentaFallidosDTO toDto(PuntosVentaFallidos puntosVentaFallidos);

    @Mapping(source = "ordenCompraId", target = "ordenCompra")
    PuntosVentaFallidos toEntity(PuntosVentaFallidosDTO puntosVentaFallidosDTO);

    default PuntosVentaFallidos fromId(Long id) {
        if (id == null) {
            return null;
        }
        PuntosVentaFallidos puntosVentaFallidos = new PuntosVentaFallidos();
        puntosVentaFallidos.setId(id);
        return puntosVentaFallidos;
    }
}
