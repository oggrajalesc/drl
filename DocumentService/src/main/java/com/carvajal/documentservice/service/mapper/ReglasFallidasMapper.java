package com.carvajal.documentservice.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.carvajal.documentservice.domain.ReglasFallidas;
import com.carvajal.documentservice.service.dto.ReglasFallidasDTO;

/**
 * Mapper for the entity ReglasFallidas and its DTO ReglasFallidasDTO.
 */
@Mapper(componentModel = "spring", uses = {DocumentoMapper.class})
public interface ReglasFallidasMapper extends EntityMapper<ReglasFallidasDTO, ReglasFallidas> {

    @Mapping(source = "ordenCompra.id", target = "ordenCompraId")
    ReglasFallidasDTO toDto(ReglasFallidas reglasFallidas);

    @Mapping(source = "ordenCompraId", target = "ordenCompra")
    ReglasFallidas toEntity(ReglasFallidasDTO reglasFallidasDTO);

    default ReglasFallidas fromId(Long id) {
        if (id == null) {
            return null;
        }
        ReglasFallidas reglasFallidas = new ReglasFallidas();
        reglasFallidas.setId(id);
        return reglasFallidas;
    }
}
