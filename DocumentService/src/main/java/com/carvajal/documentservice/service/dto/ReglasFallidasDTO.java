package com.carvajal.documentservice.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the ReglasFallidas entity.
 */
public class ReglasFallidasDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant fecha;

    @NotNull
    private String tipo;

    @NotNull
    private Long idOrdenCompra;

    private Long ordenCompraId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFecha() {
        return fecha;
    }

    public void setFecha(Instant fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Long getOrdenCompraId() {
        return ordenCompraId;
    }

    public void setOrdenCompraId(Long documentoId) {
        this.ordenCompraId = documentoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReglasFallidasDTO reglasFallidasDTO = (ReglasFallidasDTO) o;
        if (reglasFallidasDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reglasFallidasDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReglasFallidasDTO{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", idOrdenCompra=" + getIdOrdenCompra() +
            ", ordenCompra=" + getOrdenCompraId() +
            "}";
    }
}
