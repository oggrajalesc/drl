package com.carvajal.documentservice.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.carvajal.documentservice.service.dto.DocumentoDTO;

/**
 * Service Interface for managing Documento.
 */
public interface DocumentoService {

    /**
     * Save a documento.
     *
     * @param documentoDTO the entity to save
     * @return the persisted entity
     */
    DocumentoDTO save(DocumentoDTO documentoDTO);

    /**
     * Get all the documentos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DocumentoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" documento.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DocumentoDTO> findOne(Long id);
    
    /**
     * Get the "nombreDocumento" documento.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DocumentoDTO> findByNombreDocumento(String nombreDocumento);

    /**
     * Delete the "id" documento.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
