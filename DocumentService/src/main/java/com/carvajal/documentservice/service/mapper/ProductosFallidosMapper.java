package com.carvajal.documentservice.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.carvajal.documentservice.domain.ProductosFallidos;
import com.carvajal.documentservice.service.dto.ProductosFallidosDTO;

/**
 * Mapper for the entity ProductosFallidos and its DTO ProductosFallidosDTO.
 */
@Mapper(componentModel = "spring", uses = {DocumentoMapper.class})
public interface ProductosFallidosMapper extends EntityMapper<ProductosFallidosDTO, ProductosFallidos> {

    @Mapping(source = "ordenCompra.id", target = "ordenCompraId")
    ProductosFallidosDTO toDto(ProductosFallidos productosFallidos);

    @Mapping(source = "ordenCompraId", target = "ordenCompra")
    ProductosFallidos toEntity(ProductosFallidosDTO productosFallidosDTO);

    default ProductosFallidos fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProductosFallidos productosFallidos = new ProductosFallidos();
        productosFallidos.setId(id);
        return productosFallidos;
    }
}
