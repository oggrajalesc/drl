package com.carvajal.documentservice.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carvajal.documentservice.domain.ProductosFallidos;
import com.carvajal.documentservice.repository.ProductosFallidosRepository;
import com.carvajal.documentservice.service.ProductosFallidosService;
import com.carvajal.documentservice.service.dto.ProductosFallidosDTO;
import com.carvajal.documentservice.service.mapper.ProductosFallidosMapper;
/**
 * Service Implementation for managing ProductosFallidos.
 */
@Service
@Transactional
public class ProductosFallidosServiceImpl implements ProductosFallidosService {

    private final Logger log = LoggerFactory.getLogger(ProductosFallidosServiceImpl.class);

    private final ProductosFallidosRepository productosFallidosRepository;

    private final ProductosFallidosMapper productosFallidosMapper;

    public ProductosFallidosServiceImpl(ProductosFallidosRepository productosFallidosRepository, ProductosFallidosMapper productosFallidosMapper) {
        this.productosFallidosRepository = productosFallidosRepository;
        this.productosFallidosMapper = productosFallidosMapper;
    }

    /**
     * Save a productosFallidos.
     *
     * @param productosFallidosDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProductosFallidosDTO save(ProductosFallidosDTO productosFallidosDTO) {
        log.debug("Request to save ProductosFallidos : {}", productosFallidosDTO);
        ProductosFallidos productosFallidos = productosFallidosMapper.toEntity(productosFallidosDTO);
        productosFallidos = productosFallidosRepository.save(productosFallidos);
        return productosFallidosMapper.toDto(productosFallidos);
    }

    /**
     * Get all the productosFallidos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProductosFallidosDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductosFallidos");
        return productosFallidosRepository.findAll(pageable)
            .map(productosFallidosMapper::toDto);
    }


    /**
     * Get one productosFallidos by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProductosFallidosDTO> findOne(Long id) {
        log.debug("Request to get ProductosFallidos : {}", id);
        return productosFallidosRepository.findById(id)
            .map(productosFallidosMapper::toDto);
    }

    /**
     * Delete the productosFallidos by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProductosFallidos : {}", id);
        productosFallidosRepository.deleteById(id);
    }
}
