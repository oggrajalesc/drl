package com.carvajal.documentservice.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.carvajal.documentservice.service.dto.PuntosVentaFallidosDTO;

/**
 * Service Interface for managing PuntosVentaFallidos.
 */
public interface PuntosVentaFallidosService {

    /**
     * Save a puntosVentaFallidos.
     *
     * @param puntosVentaFallidosDTO the entity to save
     * @return the persisted entity
     */
    PuntosVentaFallidosDTO save(PuntosVentaFallidosDTO puntosVentaFallidosDTO);

    /**
     * Get all the puntosVentaFallidos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PuntosVentaFallidosDTO> findAll(Pageable pageable);


    /**
     * Get the "id" puntosVentaFallidos.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PuntosVentaFallidosDTO> findOne(Long id);

    /**
     * Delete the "id" puntosVentaFallidos.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
