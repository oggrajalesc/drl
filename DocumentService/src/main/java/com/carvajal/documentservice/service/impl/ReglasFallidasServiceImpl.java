package com.carvajal.documentservice.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carvajal.documentservice.domain.ReglasFallidas;
import com.carvajal.documentservice.repository.ReglasFallidasRepository;
import com.carvajal.documentservice.service.ReglasFallidasService;
import com.carvajal.documentservice.service.dto.ReglasFallidasDTO;
import com.carvajal.documentservice.service.mapper.ReglasFallidasMapper;
/**
 * Service Implementation for managing ReglasFallidas.
 */
@Service
@Transactional
public class ReglasFallidasServiceImpl implements ReglasFallidasService {

    private final Logger log = LoggerFactory.getLogger(ReglasFallidasServiceImpl.class);

    private final ReglasFallidasRepository reglasFallidasRepository;

    private final ReglasFallidasMapper reglasFallidasMapper;

    public ReglasFallidasServiceImpl(ReglasFallidasRepository reglasFallidasRepository, ReglasFallidasMapper reglasFallidasMapper) {
        this.reglasFallidasRepository = reglasFallidasRepository;
        this.reglasFallidasMapper = reglasFallidasMapper;
    }

    /**
     * Save a reglasFallidas.
     *
     * @param reglasFallidasDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ReglasFallidasDTO save(ReglasFallidasDTO reglasFallidasDTO) {
        log.debug("Request to save ReglasFallidas : {}", reglasFallidasDTO);
        ReglasFallidas reglasFallidas = reglasFallidasMapper.toEntity(reglasFallidasDTO);
        reglasFallidas = reglasFallidasRepository.save(reglasFallidas);
        return reglasFallidasMapper.toDto(reglasFallidas);
    }

    /**
     * Get all the reglasFallidas.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ReglasFallidasDTO> findAll() {
        log.debug("Request to get all ReglasFallidas");
        return reglasFallidasRepository.findAll().stream()
            .map(reglasFallidasMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one reglasFallidas by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ReglasFallidasDTO> findOne(Long id) {
        log.debug("Request to get ReglasFallidas : {}", id);
        return reglasFallidasRepository.findById(id)
            .map(reglasFallidasMapper::toDto);
    }

    /**
     * Delete the reglasFallidas by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ReglasFallidas : {}", id);
        reglasFallidasRepository.deleteById(id);
    }
}
