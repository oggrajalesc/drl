package com.carvajal.documentservice.service;

import java.util.List;
import java.util.Optional;

import com.carvajal.documentservice.service.dto.ReglasFallidasDTO;

/**
 * Service Interface for managing ReglasFallidas.
 */
public interface ReglasFallidasService {

    /**
     * Save a reglasFallidas.
     *
     * @param reglasFallidasDTO the entity to save
     * @return the persisted entity
     */
    ReglasFallidasDTO save(ReglasFallidasDTO reglasFallidasDTO);

    /**
     * Get all the reglasFallidas.
     *
     * @return the list of entities
     */
    List<ReglasFallidasDTO> findAll();


    /**
     * Get the "id" reglasFallidas.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ReglasFallidasDTO> findOne(Long id);

    /**
     * Delete the "id" reglasFallidas.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
