package com.carvajal.documentservice.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carvajal.documentservice.domain.PuntosVentaFallidos;
import com.carvajal.documentservice.repository.PuntosVentaFallidosRepository;
import com.carvajal.documentservice.service.PuntosVentaFallidosService;
import com.carvajal.documentservice.service.dto.PuntosVentaFallidosDTO;
import com.carvajal.documentservice.service.mapper.PuntosVentaFallidosMapper;
/**
 * Service Implementation for managing PuntosVentaFallidos.
 */
@Service
@Transactional
public class PuntosVentaFallidosServiceImpl implements PuntosVentaFallidosService {

    private final Logger log = LoggerFactory.getLogger(PuntosVentaFallidosServiceImpl.class);

    private final PuntosVentaFallidosRepository puntosVentaFallidosRepository;

    private final PuntosVentaFallidosMapper puntosVentaFallidosMapper;

    public PuntosVentaFallidosServiceImpl(PuntosVentaFallidosRepository puntosVentaFallidosRepository, PuntosVentaFallidosMapper puntosVentaFallidosMapper) {
        this.puntosVentaFallidosRepository = puntosVentaFallidosRepository;
        this.puntosVentaFallidosMapper = puntosVentaFallidosMapper;
    }

    /**
     * Save a puntosVentaFallidos.
     *
     * @param puntosVentaFallidosDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PuntosVentaFallidosDTO save(PuntosVentaFallidosDTO puntosVentaFallidosDTO) {
        log.debug("Request to save PuntosVentaFallidos : {}", puntosVentaFallidosDTO);
        PuntosVentaFallidos puntosVentaFallidos = puntosVentaFallidosMapper.toEntity(puntosVentaFallidosDTO);
        puntosVentaFallidos = puntosVentaFallidosRepository.save(puntosVentaFallidos);
        return puntosVentaFallidosMapper.toDto(puntosVentaFallidos);
    }

    /**
     * Get all the puntosVentaFallidos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PuntosVentaFallidosDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PuntosVentaFallidos");
        return puntosVentaFallidosRepository.findAll(pageable)
            .map(puntosVentaFallidosMapper::toDto);
    }


    /**
     * Get one puntosVentaFallidos by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PuntosVentaFallidosDTO> findOne(Long id) {
        log.debug("Request to get PuntosVentaFallidos : {}", id);
        return puntosVentaFallidosRepository.findById(id)
            .map(puntosVentaFallidosMapper::toDto);
    }

    /**
     * Delete the puntosVentaFallidos by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PuntosVentaFallidos : {}", id);
        puntosVentaFallidosRepository.deleteById(id);
    }
}
