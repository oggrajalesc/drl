package com.carvajal.documentservice.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the ProductosFallidos entity.
 */
public class ProductosFallidosDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1790613022207442893L;

	private Long id;

    @NotNull
    private String codProducto;

    @NotNull
    private String campoOrdenCompra;

    @NotNull
    private String tipoFallo;

    @NotNull
    private Long idOrdenCompra;

    private Long ordenCompraId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public String getCampoOrdenCompra() {
        return campoOrdenCompra;
    }

    public void setCampoOrdenCompra(String campoOrdenCompra) {
        this.campoOrdenCompra = campoOrdenCompra;
    }

    public String getTipoFallo() {
        return tipoFallo;
    }

    public void setTipoFallo(String tipoFallo) {
        this.tipoFallo = tipoFallo;
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Long getOrdenCompraId() {
        return ordenCompraId;
    }

    public void setOrdenCompraId(Long documentoId) {
        this.ordenCompraId = documentoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductosFallidosDTO productosFallidosDTO = (ProductosFallidosDTO) o;
        if (productosFallidosDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), productosFallidosDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProductosFallidosDTO{" +
            "id=" + getId() +
            ", codProducto='" + getCodProducto() + "'" +
            ", campoOrdenCompra='" + getCampoOrdenCompra() + "'" +
            ", tipoFallo='" + getTipoFallo() + "'" +
            ", idOrdenCompra=" + getIdOrdenCompra() +
            ", ordenCompra=" + getOrdenCompraId() +
            "}";
    }
}
