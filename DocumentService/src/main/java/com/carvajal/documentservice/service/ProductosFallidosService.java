package com.carvajal.documentservice.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.carvajal.documentservice.service.dto.ProductosFallidosDTO;

/**
 * Service Interface for managing ProductosFallidos.
 */
public interface ProductosFallidosService {

    /**
     * Save a productosFallidos.
     *
     * @param productosFallidosDTO the entity to save
     * @return the persisted entity
     */
    ProductosFallidosDTO save(ProductosFallidosDTO productosFallidosDTO);

    /**
     * Get all the productosFallidos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ProductosFallidosDTO> findAll(Pageable pageable);


    /**
     * Get the "id" productosFallidos.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ProductosFallidosDTO> findOne(Long id);

    /**
     * Delete the "id" productosFallidos.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
