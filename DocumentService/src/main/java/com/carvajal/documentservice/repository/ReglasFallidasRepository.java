package com.carvajal.documentservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carvajal.documentservice.domain.ReglasFallidas;


/**
 * Spring Data  repository for the ReglasFallidas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReglasFallidasRepository extends JpaRepository<ReglasFallidas, Long> {

}
