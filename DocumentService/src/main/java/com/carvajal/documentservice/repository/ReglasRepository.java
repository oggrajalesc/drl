package com.carvajal.documentservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carvajal.documentservice.domain.Reglas;


/**
 * Spring Data  repository for the Reglas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReglasRepository extends JpaRepository<Reglas, Long> {

}
