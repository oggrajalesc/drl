package com.carvajal.documentservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carvajal.documentservice.domain.ProductosFallidos;


/**
 * Spring Data  repository for the ProductosFallidos entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductosFallidosRepository extends JpaRepository<ProductosFallidos, Long> {

}
