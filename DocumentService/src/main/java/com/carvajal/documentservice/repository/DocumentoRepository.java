package com.carvajal.documentservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.carvajal.documentservice.domain.Documento;


/**
 * Spring Data  repository for the Documento entity.
 */
@Repository
public interface DocumentoRepository extends JpaRepository<Documento, Long> {
	@Query("select d from Documento d where d.nombreDocumento = ?1")
	Optional<Documento> findByNombreDocumento(String nombreDocumento);
}
