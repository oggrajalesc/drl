package com.carvajal.documentservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carvajal.documentservice.domain.PuntosVentaFallidos;


/**
 * Spring Data  repository for the PuntosVentaFallidos entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PuntosVentaFallidosRepository extends JpaRepository<PuntosVentaFallidos, Long> {

}
