package com.carvajal.platform.inbounDosCero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InbounDosCeroApplication {

	public static void main(String[] args) {
		SpringApplication.run(InbounDosCeroApplication.class, args);
	}
}
