package com.carvajal.platform.inbounDosCero.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.carvajal.platform.core.dto.DocumentoDto;

@Service
public class InboundConsumerService {
    private static final Logger LOG = LoggerFactory.getLogger(InboundConsumerService.class);

    @KafkaListener(topics = "${app.topic.name}")
    public void listen(@Payload DocumentoDto documentoDto,
            @Headers MessageHeaders headers) {
        LOG.info("received ordenCompra='{}'", documentoDto.getEmisor());
        //
        //
    }
}
