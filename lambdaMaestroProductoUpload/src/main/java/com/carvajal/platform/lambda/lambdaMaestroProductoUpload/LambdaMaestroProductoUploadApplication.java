package com.carvajal.platform.lambda.lambdaMaestroProductoUpload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LambdaMaestroProductoUploadApplication {
	public static void main(String[] args) {
		SpringApplication.run(LambdaMaestroProductoUploadApplication.class, args);
	}
}
