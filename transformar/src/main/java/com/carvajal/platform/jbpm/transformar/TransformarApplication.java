package com.carvajal.platform.jbpm.transformar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransformarApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransformarApplication.class, args);
	}
}
