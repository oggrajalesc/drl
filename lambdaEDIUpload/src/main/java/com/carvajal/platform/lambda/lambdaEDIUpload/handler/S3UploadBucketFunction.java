package com.carvajal.platform.lambda.lambdaEDIUpload.handler;

import java.nio.charset.Charset;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.carvajal.platform.lambda.lambdaEDIUpload.service.SenderInBoundService;

@Component("s3UploadBucketEDIFunction")
public class S3UploadBucketFunction implements Function<S3Event, String>{
	private static final Logger LOGGER = LoggerFactory.getLogger(S3UploadBucketFunction.class);
	
	private SenderInBoundService senderInBoundService;
	
	public S3UploadBucketFunction(final SenderInBoundService senderInBoundService) {
		this.senderInBoundService = senderInBoundService;
	}
	
	@Override
	public String apply(S3Event s3Event) {
		try {
			LOGGER.info("------->>>Charset: ", Charset.defaultCharset());
			LOGGER.info("------->>>Argmuentos: {}", s3Event);

			S3EventNotificationRecord record = s3Event.getRecords().get(0);

			String nameFile = record.getS3().getObject().getKey().replace('+', ' ');
			
			LOGGER.info("------->>>namFile: " + nameFile);
			
		    senderInBoundService.send(nameFile);
		    LOGGER.info("------->>>Fin Landa: {}" );
		    
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return "OK";
	}

}
