package com.carvajal.platform.lambda.lambdaEDIUpload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LambdaEDIUploadApplication {
	public static void main(String[] args) {
		SpringApplication.run(LambdaEDIUploadApplication.class, args);
	}
}
