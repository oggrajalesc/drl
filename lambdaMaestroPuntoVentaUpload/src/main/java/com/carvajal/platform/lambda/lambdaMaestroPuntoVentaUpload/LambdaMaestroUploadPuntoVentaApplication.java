package com.carvajal.platform.lambda.lambdaMaestroPuntoVentaUpload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LambdaMaestroUploadPuntoVentaApplication {
	public static void main(String[] args) {
		SpringApplication.run(LambdaMaestroUploadPuntoVentaApplication.class, args);
	}
}
