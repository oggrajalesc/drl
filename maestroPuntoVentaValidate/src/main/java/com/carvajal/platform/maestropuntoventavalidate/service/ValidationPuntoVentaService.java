package com.carvajal.platform.maestropuntoventavalidate.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carvajal.platform.core.dto.MaestroDTO;
import com.carvajal.platform.core.dto.PuntoVentaDTO;
import com.carvajal.platform.core.enums.TiposMaestro;
import com.carvajal.platform.maestropuntoventavalidate.component.FileMaestroManager;
import com.carvajal.platform.maestropuntoventavalidate.component.ValidacionesMaestro;

@Service
public class ValidationPuntoVentaService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationPuntoVentaService.class);
	
	@Autowired
	private ValidacionesMaestro validacionesMaestro;
	
	@Autowired
	private FileMaestroManager fileMaestroManager;

	@Autowired
	private MaestroDocumentosService maestroDocumentosService;
	/**
	 * @see Validar archivo cargado
	 * @param nombreDocumento
	 */
	public boolean validarProceso(String nombreDocumento) {
		LOGGER.info("-----------------------Charset: {}", Charset.defaultCharset());
		LOGGER.info("-----------------------Argumentos: {}", nombreDocumento);
		Boolean finalizaCorrectmente = Boolean.TRUE;
		try {
			
			finalizaCorrectmente = validacionesMaestro.validarExtensionExitosa(nombreDocumento) && validacionesMaestro.validarNombreArchivo(nombreDocumento);
			if(finalizaCorrectmente) {
				LOGGER.info("----------------------->>>VALIDACION EXXITOSA DE LA EXTENSIÓN Y FORMATO DEL ARCHIVO");
				
				List<String> informacionMaestro = fileMaestroManager.leerContenidoArchivo(nombreDocumento);
				if(!Objects.isNull(informacionMaestro) && informacionMaestro.size() > 1) {
					LOGGER.info("----------------------->>>EXISTE INFORMACIÓN PARA INICIAR EL PROCESO DE VALIDACIÓN");
					finalizaCorrectmente = iniciarValidacion(nombreDocumento, informacionMaestro);
				}else{
					LOGGER.info("----------------------->>>NO EXISTE INFORMACIÓN PARA INICIAR EL PROCESO DE VALIDACIÓN");
					finalizaCorrectmente = false;
				}
			}
			return finalizaCorrectmente; 
		}catch(IOException ie) {
			LOGGER.error("------------------------Error: {}", ie.getMessage());
			return false;
		}
	}
	
	private boolean iniciarValidacion(String nombreDocumento, List<String> informacionMaestro) {
		LOGGER.info("----------------------->>>Argumentos: {0},{1}", nombreDocumento,informacionMaestro);
		MaestroDTO maestroRequest= new MaestroDTO();
		maestroRequest.setPais("CO");
		maestroRequest.setCliente("7705326070067");
		maestroRequest.setTipo(TiposMaestro.PUNTO_VENTA.name());
		
		MaestroDTO maestroResponse = maestroDocumentosService.getRestricciones(maestroRequest);
		if(!Objects.isNull(maestroResponse)) {
			if(!Objects.isNull(maestroResponse.getRestricciones()) && !maestroResponse.getRestricciones().isEmpty()) {
				
				List<PuntoVentaDTO> puntosVenta = new ArrayList<>();
				List<String> errores = validacionesMaestro.validaciones(informacionMaestro, maestroResponse,puntosVenta);
				if(!errores.isEmpty()) {
					LOGGER.error("-------------------------------->>>EXISTEN ERRORES----------------------------------------------: {}", errores);
					LOGGER.error("-------------------------------->>>EL ARCHIVO SE MOVERÁ Al BUCKET DE ERRORES----------------------------------------------");
					return Boolean.FALSE;
				}else {
					maestroDocumentosService.guardarPuntoVenta(puntosVenta);
					return Boolean.TRUE;
				}
			}else {
				return Boolean.FALSE;
			}
			
		}else {
			LOGGER.error("-------------------------------->>>NO HAY MESATRO CONFIGURADO----------------------------------------------: {}", maestroRequest);
			return Boolean.FALSE;
		}

	}

	public void copiarArchivo(String nombreDocumento) {
		LOGGER.info("-------------------------->>>>INICIAR copiado: {}", nombreDocumento);
		fileMaestroManager.copiarArchivo(nombreDocumento);
		LOGGER.info("-------------------------->>>>TERMINO copiado: {}", nombreDocumento);
	}

}
