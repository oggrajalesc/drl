package com.carvajal.platform.maestropuntoventavalidate.kafka.service;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class kafkaManager {
	private static final Logger LOGGER = LoggerFactory.getLogger(kafkaManager.class);
	
    @Autowired
    private KafkaMaestroPuntoVenta<String> kafkaInterfaceDocumento;
    
    @KafkaListener(topics = "${app.topicConsumer.name}")
    public void listen(@Payload String nombreDocumento,
            @Headers MessageHeaders headers) {
		LOGGER.info("Charset: {}", Charset.defaultCharset());
		LOGGER.info("Argmuentos: {}", nombreDocumento);
        
        kafkaInterfaceDocumento.kafkaConsumer(nombreDocumento);
    }	

}
