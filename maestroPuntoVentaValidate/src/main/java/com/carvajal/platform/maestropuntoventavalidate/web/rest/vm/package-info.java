/**
 * View Models used by Spring MVC REST controllers.
 */
package com.carvajal.platform.maestropuntoventavalidate.web.rest.vm;
