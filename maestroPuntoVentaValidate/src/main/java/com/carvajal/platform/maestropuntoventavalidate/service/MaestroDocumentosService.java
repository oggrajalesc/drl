package com.carvajal.platform.maestropuntoventavalidate.service;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.carvajal.platform.core.dto.MaestroDTO;
import com.carvajal.platform.core.dto.PuntoVentaDTO;

@Service
public class MaestroDocumentosService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MaestroDocumentosService.class);
	
	//8999/api/maestro-documentos 
	
	@Value("${app.maestrodocument-service.url}")
	private String url;

	@Value("${app.maestrodocument-service.port}")
	private String port;
	
	/**
	 * @see obtener restricciones
	 * @param maestro
	 */
	public MaestroDTO getRestricciones(MaestroDTO maestro) {
		String endPoint = "/api/maestro-documentos";
		try {
			RestTemplate restTemplate = new RestTemplate();

			URI uriService = new URI(url+":"+port+endPoint);
			
			MaestroDTO maestro2 = restTemplate.postForObject(
					uriService,
					  maestro,
					  MaestroDTO.class);

			return maestro2;
		} catch (HttpClientErrorException e1) {
			LOGGER.error("----------------------->>>error: {}", e1.getMessage());
		} catch (Exception e2) {
			LOGGER.error("----------------------->>>error: {} ", e2.getMessage());
		}
		return null;
	}

	/**
	 * @see guardar puntos de venta
	 * @param puntosVenta
	 */
	public void guardarPuntoVenta(List<PuntoVentaDTO> puntosVenta) {
		String endPoint = "/api/punto-ventas/saveAll";
		try {
			RestTemplate restTemplate = new RestTemplate();

			URI uriService = new URI(url+":"+port+endPoint);

			HttpEntity<List<PuntoVentaDTO>> request = new HttpEntity<>(puntosVenta);
			Object response = restTemplate
			  .exchange(uriService, HttpMethod.POST, request, Object.class);			

			LOGGER.info("----------------------->>>response: {}", response);
		} catch (HttpClientErrorException e) {
			LOGGER.error("----------------------->>>error: {}", e.getMessage());
		} catch (Exception e) {
			LOGGER.error("----------------------->>>error: {}", e.getMessage());
		}
	}	

}
