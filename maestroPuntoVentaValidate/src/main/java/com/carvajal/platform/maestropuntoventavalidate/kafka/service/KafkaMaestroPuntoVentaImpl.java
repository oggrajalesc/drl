package com.carvajal.platform.maestropuntoventavalidate.kafka.service;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carvajal.platform.maestropuntoventavalidate.service.ValidationPuntoVentaService;

@Service
public class KafkaMaestroPuntoVentaImpl<T> implements KafkaMaestroPuntoVenta<T> {
	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaMaestroPuntoVentaImpl.class);
	
	@Autowired
	private ValidationPuntoVentaService validationPuntoVentaService;
	
	@Override
	public void kafkaConsumer(T object) {
		LOGGER.info("-----------------------Charset: {}", Charset.defaultCharset());
		LOGGER.info("-----------------------Argumentos: {}", object);
		String nombreDocumento = (String)object;
		boolean resultado = validationPuntoVentaService.validarProceso(nombreDocumento);
		
		if(!resultado) {
			validationPuntoVentaService.copiarArchivo(nombreDocumento);
		}
	}
}
