package com.carvajal.platform.maestropuntoventavalidate.kafka.service;

public interface KafkaMaestroPuntoVenta<T> {
	void kafkaConsumer(T object);
}
