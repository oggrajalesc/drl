package com.carvajal.platform.maestropuntoventavalidate.component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest.KeyVersion;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

@Component
public class FileMaestroManager {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileMaestroManager.class);
	
	@Value("${app.bucketFiles.name}")
	private String bucketName;
	
	@Value("${app.bucketFilesError.name}")
	private String bucketNameError;

	/**
	 * @see Lee el archivo copiado al bucket
	 * @param nombreDocumento
	 * @return
	 */
	public List<String> leerContenidoArchivo(String nombreDocumento)  throws IOException{
		LOGGER.info("Charset: {}", Charset.defaultCharset());
		LOGGER.info("Argumentos: {}", nombreDocumento);
		AmazonS3 s3Client = obtenerInstanciaS3();

		List<String> lineasDelArchivo = new ArrayList<>();
    	S3Object object = s3Client.getObject(new GetObjectRequest(bucketName, nombreDocumento));
    	InputStream objectDataInput = object.getObjectContent();
    	        	
        try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(objectDataInput));

            obtenerListadoDeLineasDelArchivo(bReader,lineasDelArchivo);
            LOGGER.info("fin: {}", nombreDocumento);
        } finally {
            try { objectDataInput.close(); } catch (RuntimeException e) {
            	LOGGER.error("error: {}", e.getMessage());
            }
        }    	
        
        return lineasDelArchivo;
	}
	
	/**
	 * @see Mueve el archivo al bucker {bucketNameError}
	 * @param nombreDocumento
	 */
	public void copiarArchivo(String nombreDocumento) {
		LOGGER.info("------------------------------->>>Charset {}", Charset.defaultCharset());
		LOGGER.info("------------------------------->>>Argumentos {}", nombreDocumento);		
		AmazonS3 s3Client = obtenerInstanciaS3();
		LOGGER.info("------------------------------->>>Mover archivo inicio: {}",nombreDocumento);
		CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, nombreDocumento, bucketNameError, nombreDocumento);
		try{
			
			s3Client.copyObject(copyObjRequest); 
			
			DeleteObjectsRequest delObjRequest = new DeleteObjectsRequest(bucketName);
			List<KeyVersion> keys = new ArrayList<KeyVersion>();
			keys.add(new KeyVersion(nombreDocumento));
			delObjRequest.setKeys(keys);
			s3Client.deleteObjects(delObjRequest);
			LOGGER.info("------------------------------->>>Mover archivo fin: {}",nombreDocumento);
		}catch(AmazonS3Exception aex) {
			LOGGER.error("------------------------------->>>Error Mover archivo inicio: {}",aex.getErrorMessage());
		}
	}

	/**
	 * 
	 * @return
	 */
	private AmazonS3 obtenerInstanciaS3() throws com.amazonaws.services.s3.model.AmazonS3Exception{
    	BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIYE25NQHTDOQO4UQ", "BSg+ZDneXNz13WQFTlbe190fbDsukZDqj4RWPlFc");
    	AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
    	                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
    	                        .withRegion(Regions.US_WEST_2)
    	                        .build();
// AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();    	
    	return s3Client;
	}
	
	private static void obtenerListadoDeLineasDelArchivo(BufferedReader bufferRAWS, List<String> lineasDelArchivo) {
		try (BufferedReader bufferR = bufferRAWS) {
			String linea;
			Pattern pat = Pattern.compile("[A-Za-z0-9 |/]*");
			while ((linea = bufferR.readLine()) != null) {
				linea = linea.trim();
				Matcher mat = pat.matcher(linea);
				if (mat.matches()  && linea.length()>0) {
					lineasDelArchivo.add(linea.concat(" "));
				}
			}
			bufferRAWS.close();
		} catch (IOException e) {e.printStackTrace();
			LOGGER.error("Error: {}", e.getMessage());
		} 
	}	

}
