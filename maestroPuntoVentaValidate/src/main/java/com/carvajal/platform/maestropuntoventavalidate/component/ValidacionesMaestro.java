package com.carvajal.platform.maestropuntoventavalidate.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.carvajal.platform.core.dto.MaestroDTO;
import com.carvajal.platform.core.dto.PuntoVentaDTO;
import com.carvajal.platform.core.dto.RestriccionDTO;

@Service
public class ValidacionesMaestro {
	private static final String EXTENSION = "txt";

	private static final String ERROR_PARTE_5 = "---------";
	private static final String ERROR_PARTE_4 = "  datoAEvaluar: ";
	private static final String ERROR_PARTE_3 = "Campo: ";
	private static final String ERROR_PARTE_2 = "] Columna [";
	private static final String ERROR_PARTE_1 = "La fila [";
	private static final String NUMERICO = "N";
	private static final String VACIO = "";
	
	private static final String PREFIJO="(Maestro_Puntos_Venta_{1})";
	private static final String ANNO="([1-9]{1}[0-9]{1,3})";
	private static final String MES="([0]{1}[0-9]{1}|[1]{1}[0-2]{1})";
	private static final String DIA="([0]{1}[0-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-1]{1})";
	private static final String HORA="([0]{1}[0-9]{1}|[1]{1}[0-9]{1}|[2]{1}[0-3]{1})";
	private static final String MINUTO="([0]{1}[0-9]{1}|[1-5]{1}[0-9]{1})";
	private static final String SEPARADOR = "([-]{1})";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidacionesMaestro.class);
	
	/**
	 * 
	 * @param nombreDelArchivoAValidar
	 * @return
	 */
	public boolean validarExtensionExitosa(String nombreDelArchivoAValidar) {
		boolean result = false;
		if(EXTENSION.equalsIgnoreCase(nombreDelArchivoAValidar.substring(nombreDelArchivoAValidar.length()-3, nombreDelArchivoAValidar.length()))){
			result = true;
		}else {
			LOGGER.error("\"---------------------------------------------------------------->>>>Archivo con formato inválido<<<----------------------------------");
		}
		return result;
	}
	
	/**
	 * 
	 * @param nombreDelArchivoAValidar
	 * @return
	 */
	public boolean validarNombreArchivo(String nombreDelArchivoAValidar) {
		boolean result = false;
		
		String patronNombreArchivo =  PREFIJO.concat(ANNO).concat(SEPARADOR).concat(MES).concat(SEPARADOR).concat(DIA).concat(SEPARADOR).concat(HORA).concat(MINUTO);

		Pattern pat = Pattern.compile(patronNombreArchivo);
		Matcher mat = pat.matcher(nombreDelArchivoAValidar.substring(0,nombreDelArchivoAValidar.length()-4));
		if (mat.matches()) {
			result = true;
		}else {
			LOGGER.error("---------------------------------------------------------------->>>>Archivo con el nombre incorrecto<<<--------------------");
		}
		
		return result;
	}

	/**
	 * 
	 * @param listadoDeErrores
	 * @param numeroDeLaFilaEvaluando
	 * @param restriccionConfiguradaEnDBDelComponenteDeLaLinea
	 * @param datoAEvaluar
	 */
	public void validarPatron(List<String> listadoDeErrores, int numeroDeLaFilaEvaluando, RestriccionDTO restriccionConfiguradaEnDBDelComponenteDeLaLinea, String datoAEvaluar) {
		Pattern pat = Pattern.compile(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPatron());
		String error;
		Matcher mat = pat.matcher(datoAEvaluar);
		if (!mat.matches()) {
			error = " valores no permitidos";
			listadoDeErrores.add(ERROR_PARTE_1+numeroDeLaFilaEvaluando+ERROR_PARTE_2+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()+"] "+ error);
			LOGGER.error("---------------------------------------------------------------->>>>Error en validar Patron--------------------------------------->>: {}", new StringBuilder(ERROR_PARTE_3).append(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()).append(ERROR_PARTE_4).append(datoAEvaluar).append(ERROR_PARTE_5).append(error));
		}		
	}
	
	/**
	 * 
	 * @param numeroAEvaluar
	 * @return
	 */
	private boolean esNumerico(String numeroAEvaluar) {		
		boolean response = Boolean.TRUE;
		Pattern pat = Pattern.compile("^[0-9]*$");
		Matcher mat = pat.matcher(numeroAEvaluar);
		if (!mat.matches()) {
			response = Boolean.FALSE;
		}
		
		return response;
	}	
	
	/**
	 * 
	 * @param lineasDelArchivo
	 * @param restricciones
	 */
	public List<String> validaciones(List<String> lineasDelArchivo,MaestroDTO maestroInfo,List<PuntoVentaDTO> puntosVenta) {
		LOGGER.info("---------------------------------------------------------------->>>>INICIO DE LAS VALIDACIONES DEL ARCHIVO Argumentos: {}", maestroInfo);
		List<String> listadoDeErrores =  new ArrayList<>();

		for (int i=1; i<lineasDelArchivo.size(); i++) {
			int numeroDeLaFilaEvaluando = i;

			List<String> componentesDeLaLinea = new ArrayList<>();
			componentesDeLaLinea.addAll(Arrays.asList(lineasDelArchivo.get(i).split(Pattern.quote("|"))));

			if('|' == lineasDelArchivo.get(i).charAt(lineasDelArchivo.get(i).length()-1)) {
				componentesDeLaLinea.add(VACIO);
			}				
			
			String error;
			//numero de cols de la linea VS num de restricciones 
			if(componentesDeLaLinea.size() != maestroInfo.getRestricciones().size()){
				error = "------------------------------------->>> ERRORRRRR por el numero de columnas enviados";
				listadoDeErrores.add(String.format("%s %s %s %s", "----------------------------------------------------------------La fila ", numeroDeLaFilaEvaluando," ", error));
				LOGGER.error(error);
				continue;
			}
			
			PuntoVentaDTO pVenta = new PuntoVentaDTO();
			for (int indiceComponenteDeLaLinea=0; indiceComponenteDeLaLinea<componentesDeLaLinea.size(); indiceComponenteDeLaLinea++) {					
				
				String posicionDelComponenteEnLaLinea =  (Integer.valueOf(indiceComponenteDeLaLinea+1)).toString();
				RestriccionDTO restriccionConfiguradaEnDBDelComponenteDeLaLinea = maestroInfo.getRestricciones().stream().filter(x -> posicionDelComponenteEnLaLinea.equals(x.getPosicion())).findAny().orElse(null);
				if(!Objects.isNull(restriccionConfiguradaEnDBDelComponenteDeLaLinea)) {
					String datoAEvaluar = componentesDeLaLinea.get(indiceComponenteDeLaLinea).trim();
					
					//Obligatoriedad
					if("R".equalsIgnoreCase(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getObligatoriedad()) && datoAEvaluar.isEmpty()){
						error = " esta vacío.";
						listadoDeErrores.add("La fila ["+lineasDelArchivo+"] Columna ["+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()+"] "+ error);
						LOGGER.error("---------------------------------------------------------------->>>>Información: {}", new StringBuilder("La fila [").append(lineasDelArchivo).append("]").append(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()).append("  datoAEvaluar: ").append(datoAEvaluar).append("---------").append(error));
					}else {
						if(!VACIO.equals(datoAEvaluar)) {
							if(NUMERICO.equalsIgnoreCase(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getTipoDato())){
								if(!esNumerico(datoAEvaluar)){
									error = " contiene un tipo de dato no válido.";
									listadoDeErrores.add("La fila ["+numeroDeLaFilaEvaluando+"] Columna "+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()+ error);
									LOGGER.error("---------------------------------------------------------------->>>>Información: {}",   new StringBuilder("La fila [").append(lineasDelArchivo).append("]").append(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()).append("  datoAEvaluar: ").append(datoAEvaluar).append( "---------").append(error));
								}else {
									if(!Objects.isNull(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPatron()) && !VACIO.equals(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPatron().trim())) {
										validarPatron(listadoDeErrores,indiceComponenteDeLaLinea,restriccionConfiguradaEnDBDelComponenteDeLaLinea,datoAEvaluar);	
									}else {
										if(datoAEvaluar.length() > Integer.parseInt(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getLongitud())) {
											error = " Longitud sobrepasa el tamaño permitido.";
											listadoDeErrores.add("La fila ["+numeroDeLaFilaEvaluando+"] Columna "+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()+ error);
											LOGGER.error("---------------------------------------------------------------->>>Información: {}" , new StringBuilder("La fila [").append(lineasDelArchivo).append("]").append(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()).append("  datoAEvaluar: ").append(datoAEvaluar).append("---------").append(error));
										}
									}
								}
							}else{
								if(!Objects.isNull(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPatron()) && !VACIO.equals(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPatron().trim())) {
									validarPatron(listadoDeErrores,indiceComponenteDeLaLinea,restriccionConfiguradaEnDBDelComponenteDeLaLinea,datoAEvaluar);	
								}else {
									if(datoAEvaluar.length() > Integer.parseInt(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getLongitud())) {
										error = " Longitud sobrepasa el tamaño permitido.";
										listadoDeErrores.add("La fila ["+numeroDeLaFilaEvaluando+"] Columna "+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()+ error);
										LOGGER.error("---------------------------------------------------------------->>>Información: {}", new StringBuilder("La fila [").append(lineasDelArchivo).append("]").append(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getPosicion()).append("  datoAEvaluar: ").append(datoAEvaluar).append("---------").append(error));
									}
								}								
							}
						}
						
					}
				}
				
			}
			if(listadoDeErrores.isEmpty()) {
				pVenta = new PuntoVentaDTO();
				pVenta.setCodigo(VACIO.equals(componentesDeLaLinea.get(0).trim())?null:componentesDeLaLinea.get(0).trim() );
				pVenta.setPuntoVentaEAN(VACIO.equals(componentesDeLaLinea.get(1).trim())?null:componentesDeLaLinea.get(1).trim());
				pVenta.setNombre(VACIO.equals(componentesDeLaLinea.get(2).trim())?null:componentesDeLaLinea.get(2).trim());
				pVenta.setCodigoCentroVenta(VACIO.equals(componentesDeLaLinea.get(3).trim())?null:componentesDeLaLinea.get(3).trim());
				pVenta.setFrecuenciaPedido(VACIO.equals(componentesDeLaLinea.get(4).trim())?null:componentesDeLaLinea.get(4).trim());
				pVenta.setIncluyeImpuesto(VACIO.equals(componentesDeLaLinea.get(5).trim())?null:componentesDeLaLinea.get(5).trim());
				pVenta.setIdMaestro(VACIO.equals(maestroInfo.getId())?null:maestroInfo.getId());
				puntosVenta.add(pVenta);
			}
		}
		
		LOGGER.info("----------------------------FIN DE LAS VALIDACIONES DEL ARCHIVO");
		return listadoDeErrores;
	
	}
}
