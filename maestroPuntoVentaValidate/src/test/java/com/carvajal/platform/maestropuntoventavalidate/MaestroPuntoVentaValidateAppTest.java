package com.carvajal.platform.maestropuntoventavalidate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MaestroPuntoVentaValidateApp.class)
public class MaestroPuntoVentaValidateAppTest {
	
	@Autowired
	private MaestroPuntoVentaValidateApp maestroPuntoVentaValidateApp;
	
	@Test
	public void initApplicationTest() {
		maestroPuntoVentaValidateApp.initApplication();
	}
	
	@Test
	public void mainTest() {
		String[] args = {""};
		MaestroPuntoVentaValidateApp.main(args);
	}
}
