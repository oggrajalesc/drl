package com.carvajal.platform.maestropuntoventavalidate.service;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.carvajal.platform.core.dto.MaestroDTO;
import com.carvajal.platform.core.dto.RestriccionDTO;
import com.carvajal.platform.core.enums.TiposMaestro;
import com.carvajal.platform.maestropuntoventavalidate.MaestroPuntoVentaValidateApp;
import com.carvajal.platform.maestropuntoventavalidate.component.FileMaestroManager;
import com.carvajal.platform.maestropuntoventavalidate.component.ValidacionesMaestro;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MaestroPuntoVentaValidateApp.class)
public class ValidationPuntoVentaServiceTest {

	private ValidationPuntoVentaService validationPuntoVentaService = new ValidationPuntoVentaService();
	
	@Mock
	private FileMaestroManager fileMaestroManager;	
	
	@InjectMocks private ValidacionesMaestro validacionesMaestro;
	
	@Mock  private MaestroDocumentosService maestroDocumentosService;
	
	@Before
	public void init() {
		 ReflectionTestUtils.setField(validationPuntoVentaService, "fileMaestroManager", fileMaestroManager);
		 ReflectionTestUtils.setField(validationPuntoVentaService, "validacionesMaestro", validacionesMaestro);
		 ReflectionTestUtils.setField(validationPuntoVentaService, "maestroDocumentosService", maestroDocumentosService);
	}
	
	public List<RestriccionDTO> getRestricciones() {
		List<RestriccionDTO> restriccionDTOesSimuladas = new ArrayList<>();
		RestriccionDTO r1 = new RestriccionDTO();
		
//		restriccionDTOesSimuladas.add(new RestriccionDTO("Código interno del punto de venta", "1", "R", "AN", "18", "", Arrays.asList(new String[] {}), ""));
		r1.setPosicion("1");
	    r1.setObligatoriedad("R");
	    r1.setTipoDato("AN");
	    r1.setLongitud("18");
	    r1.setPatron(""); 
	    restriccionDTOesSimuladas.add(r1);
	    
		RestriccionDTO r2 = new RestriccionDTO();
//		restriccionDTOesSimuladas.add(new RestriccionDTO("EAN Punto de venta", "2", "0", "AN", "18", "", Arrays.asList(new String[] {}), ""));		
		r2.setPosicion("2");
	    r2.setObligatoriedad("O");
	    r2.setTipoDato("AN");
	    r2.setLongitud("18");
	    r2.setPatron(""); 		
	    restriccionDTOesSimuladas.add(r2);
	    
		// Codigo de producto (ERP)
//		restriccionDTOesSimuladas.add(new RestriccionDTO("Nombre punto de venta", "3", "0", "AN", "35", "",Arrays.asList(new String[] {}), ""));	    
	    RestriccionDTO r3 = new RestriccionDTO();
		r3.setPosicion("3");
	    r3.setObligatoriedad("O");
	    r3.setTipoDato("AN");
	    r3.setLongitud("35");
	    r3.setPatron(""); 		
	    restriccionDTOesSimuladas.add(r3);
	    
		// Descripción producto
//		restriccionDTOesSimuladas.add(new RestriccionDTO("Codigo centro de venta", "4", "O", "AN", "10", "",Arrays.asList(new String[] {}), ""));		
	    RestriccionDTO r4 = new RestriccionDTO();
		r4.setPosicion("4");
	    r4.setObligatoriedad("O");
	    r4.setTipoDato("AN");
	    r4.setLongitud("10");
	    r4.setPatron("");
	    restriccionDTOesSimuladas.add(r4);
		 	    
		// Estado de producto (activo / inactivo)
//		restriccionDTOesSimuladas.add(new RestriccionDTO("Frecuencia de montaje de pedido", "5", "O", "AN", "7", "([l,L]{0,1}[m,M]{0,1}[w,W]{0,1}[j,J]{0,1}[v,V]{0,1}[s,S]{0,1}[d,D]{0,1})",Arrays.asList(new String[] { }), ""));		
	    RestriccionDTO r5 = new RestriccionDTO();
		r5.setPosicion("5");
	    r5.setObligatoriedad("O");
	    r5.setTipoDato("AN");
	    r5.setLongitud("7");
	    r5.setPatron("([l,L]{0,1}[m,M]{0,1}[w,W]{0,1}[j,J]{0,1}[v,V]{0,1}[s,S]{0,1}[d,D]{0,1})"); 		
	    restriccionDTOesSimuladas.add(r5);
	    
		// Organización de ventas
//		restriccionDTOesSimuladas.add(new RestriccionDTO("Incluye IGV / IVA", "6", "O", "N", "1", "[0,1]{1}", Arrays.asList(new String[] {}), ""));
		RestriccionDTO r6 = new RestriccionDTO();
		r6.setPosicion("6");
	    r6.setObligatoriedad("O");
	    r6.setTipoDato("N");
	    r6.setLongitud("1");
	    r6.setPatron("[0,1]{1}"); 		
		
	    restriccionDTOesSimuladas.add(r6);

		return restriccionDTOesSimuladas;
	}	
	
	@Test
	public void validarProcesoOKTest() throws IOException {
		String nombreDocumento =  "Maestro_Puntos_Venta_2018-10-22-1011.txt";
		List<String> contenido= new ArrayList<>();
		contenido.add("681100873|86053341404|Corral Calle 122|14211|MJS| ");
		contenido.add("2420900001|83005564355|Cinemark San Pedro Neiva|14209|MJS| ");
		
		Mockito.when(fileMaestroManager.leerContenidoArchivo(nombreDocumento)).thenReturn(contenido);

		MaestroDTO maestroRequest = new MaestroDTO(); 
		maestroRequest.setPais("CO");
		maestroRequest.setCliente("7705326070067");
		maestroRequest.setTipo(TiposMaestro.PUNTO_VENTA.name());
		
		MaestroDTO maestroResponse = new MaestroDTO();
		maestroResponse.setPais("CO");
		maestroResponse.setCliente("7705326070067");
		maestroResponse.setTipo(TiposMaestro.PUNTO_VENTA.name());
		maestroResponse.setId("1");
		maestroResponse.setRestricciones(getRestricciones());
		
		Mockito.when(maestroDocumentosService.getRestricciones(Mockito.anyObject())).thenReturn(maestroResponse);
		
		Boolean validarResponse = validationPuntoVentaService.validarProceso(nombreDocumento);
		assertEquals(validarResponse, Boolean.TRUE);
//
	} 
	
	@Test
	public void validarProcesoFailFrecuenciaTest() throws IOException {
		String nombreDocumento =  "Maestro_Puntos_Venta_2018-10-22-1011.txt";
		List<String> contenido= new ArrayList<>();
		contenido.add("Codigo interno del punto de venta|EAN Punto de venta|Nombre punto de venta|Codigo centro de venta|Frecuencia de montaje de pedido|Incluye IGV / IVA		681100870|86053341434|Corral Esso Calle100|14211|LMWJVS|												 ");
		contenido.add("681100873|86053341404|Corral Calle 122|14211|MJSX| ");
		contenido.add("2420900001|83005564355|Cinemark San Pedro Neiva|14209|MJS| ");
		
		Mockito.when(fileMaestroManager.leerContenidoArchivo(nombreDocumento)).thenReturn(contenido);
		
		MaestroDTO maestroRequest = new MaestroDTO(); 
		maestroRequest.setPais("CO");
		maestroRequest.setCliente("7705326070067");
		maestroRequest.setTipo(TiposMaestro.PUNTO_VENTA.name());
		
		MaestroDTO maestroResponse = new MaestroDTO();
		maestroResponse.setPais("CO");
		maestroResponse.setCliente("7705326070067");
		maestroResponse.setTipo(TiposMaestro.PUNTO_VENTA.name());
		maestroResponse.setId("1");
		maestroResponse.setRestricciones(getRestricciones());
		
		Mockito.when(maestroDocumentosService.getRestricciones(Mockito.anyObject())).thenReturn(maestroResponse);
		
		Boolean validarResponse = validationPuntoVentaService.validarProceso(nombreDocumento);
		assertEquals(validarResponse, Boolean.FALSE);
//
	} 
	
	@Test
	public void validarProcesoFailContenidoTest() throws IOException {
		String nombreDocumento =  "Maestro_Puntos_Venta_2018-10-22-1011.txt";
		List<String> contenido= new ArrayList<>();
		contenido.add("Codigo interno del punto de venta|EAN Punto de venta|Nombre punto de venta|Codigo centro de venta|Frecuencia de montaje de pedido|Incluye IGV / IVA		681100870|86053341434|Corral Esso Calle100|14211|LMWJVS|												 ");
		
		Mockito.when(fileMaestroManager.leerContenidoArchivo(nombreDocumento)).thenReturn(contenido);
		
		MaestroDTO maestroRequest = new MaestroDTO(); 
		maestroRequest.setPais("CO");
		maestroRequest.setCliente("7705326070067");
		maestroRequest.setTipo(TiposMaestro.PUNTO_VENTA.name());
		
		MaestroDTO maestroResponse = new MaestroDTO();
		maestroResponse.setPais("CO");
		maestroResponse.setCliente("7705326070067");
		maestroResponse.setTipo(TiposMaestro.PUNTO_VENTA.name());
		maestroResponse.setId("1");
		maestroResponse.setRestricciones(getRestricciones());
		
		Mockito.when(maestroDocumentosService.getRestricciones(Mockito.anyObject())).thenReturn(maestroResponse);
		
		Boolean validarResponse = validationPuntoVentaService.validarProceso(nombreDocumento);
		assertEquals(validarResponse, Boolean.FALSE);
//
	} 
	
	@Test
	public void validarProcesoNoRestriccionest() throws IOException {
		String nombreDocumento =  "Maestro_Puntos_Venta_2018-10-22-1011.txt";
		List<String> contenido= new ArrayList<>();
		contenido.add("Codigo interno del punto de venta|EAN Punto de venta|Nombre punto de venta|Codigo centro de venta|Frecuencia de montaje de pedido|Incluye IGV / IVA		681100870|86053341434|Corral Esso Calle100|14211|LMWJVS|												 ");
		
		Mockito.when(fileMaestroManager.leerContenidoArchivo(nombreDocumento)).thenReturn(contenido);
		
		MaestroDTO maestroRequest = new MaestroDTO(); 
		maestroRequest.setPais("CO");
		maestroRequest.setCliente("7705326070067");
		maestroRequest.setTipo(TiposMaestro.PUNTO_VENTA.name());
		
		MaestroDTO maestroResponse = new MaestroDTO();
		maestroResponse.setPais("CO");
		maestroResponse.setCliente("7705326070067");
		maestroResponse.setTipo(TiposMaestro.PUNTO_VENTA.name());
		maestroResponse.setId("1");
		maestroResponse.setRestricciones(null);
		
		Mockito.when(maestroDocumentosService.getRestricciones(Mockito.anyObject())).thenReturn(maestroResponse);
		
		Boolean validarResponse = validationPuntoVentaService.validarProceso(nombreDocumento);
		assertEquals(validarResponse, Boolean.FALSE);
//
	} 
}
