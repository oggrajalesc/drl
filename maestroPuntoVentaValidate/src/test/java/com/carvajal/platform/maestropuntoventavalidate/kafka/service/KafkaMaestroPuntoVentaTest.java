package com.carvajal.platform.maestropuntoventavalidate.kafka.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carvajal.platform.maestropuntoventavalidate.MaestroPuntoVentaValidateApp;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MaestroPuntoVentaValidateApp.class)
public class KafkaMaestroPuntoVentaTest {

	@Autowired
	private KafkaMaestroPuntoVenta<String> kafkaMaestroPuntoVenta;
	
	@Test
	public void validarProcesoOKTest() {
		kafkaMaestroPuntoVenta.kafkaConsumer("Maestro_Puntos_Venta_2018-10-21.tx");
	}
}
