package com.carvajal.masterdocumentservice.web.rest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ResourceInUseException;
import com.carvajal.masterdocumentservice.domain.Producto;
import com.carvajal.masterdocumentservice.rule.LocalDynamoDBCreationRule;
import com.carvajal.masterdocumentservice.service.ProductoService;

/**
 * Test class for the MaestroDocumentoResource REST controller.
 *
 * @see MaestroResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles("local")
@TestPropertySource(properties = { "amazon.dynamodb.endpoint=http://localhost:8000/", "amazon.aws.accesskey=test1",
		"amazon.aws.secretkey=test231" })
public class ProductoDocumentoResourceIntTest {

	@ClassRule
	public static LocalDynamoDBCreationRule dynamoDB = new LocalDynamoDBCreationRule();

	private DynamoDBMapper dynamoDBMapper;

	@Autowired
	private AmazonDynamoDB amazonDynamoDB;

	@Autowired
	ProductoService productoService;

	private static final String EXPECTED_MAESTRO = "1";

	@Before
	public void setup() throws Exception {

		try {
			dynamoDBMapper = new DynamoDBMapper(amazonDynamoDB);
			CreateTableRequest tableRequest = dynamoDBMapper.generateCreateTableRequest(Producto.class);
			tableRequest.setProvisionedThroughput(new ProvisionedThroughput(1L, 1L));
			KeySchemaElement hashKey = new KeySchemaElement("id", KeyType.HASH);
			List<KeySchemaElement> keySchema = new ArrayList<>();
			keySchema.add(hashKey);
			tableRequest.setKeySchema(keySchema);
			amazonDynamoDB.createTable(tableRequest);
		} catch (ResourceInUseException e) {
			// Do nothing, table already created
		}

		dynamoDBMapper.batchDelete((List<Producto>) productoService.findAll());
	}
	
	@Test
	public void saveList() {

//		ProductoDTO producto1 = new ProductoDTO();
//		producto1.setIdMaestro(EXPECTED_MAESTRO);
//		
//		ProductoDTO producto2 = new ProductoDTO();
//		producto2.setIdMaestro(EXPECTED_MAESTRO);
//		
//		List<ProductoDTO> productos = new ArrayList<>();
//		productos.add(producto1);
//		productos.add(producto2);
//
//		productoService.save(productos);
//		
//		List<Producto> productosBD = productoService.findAll();
//		assertEquals(productos.size(), productosBD.size());

	}

}
