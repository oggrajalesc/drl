package com.carvajal.masterdocumentservice.web.rest;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ResourceInUseException;
import com.carvajal.masterdocumentservice.domain.Maestro;
import com.carvajal.masterdocumentservice.rule.LocalDynamoDBCreationRule;
import com.carvajal.masterdocumentservice.service.MaestroService;
import com.carvajal.masterdocumentservice.service.dto.MaestroDTO;

/**
 * Test class for the MaestroDocumentoResource REST controller.
 *
 * @see MaestroResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles("local")
@TestPropertySource(properties = { "amazon.dynamodb.endpoint=http://localhost:8000/", "amazon.aws.accesskey=test1",
		"amazon.aws.secretkey=test231" })
public class PuntoVentaDocumentoResourceIntTest {

	@ClassRule
	public static LocalDynamoDBCreationRule dynamoDB = new LocalDynamoDBCreationRule();

	private DynamoDBMapper dynamoDBMapper;

	@Autowired
	private AmazonDynamoDB amazonDynamoDB;

//	@Autowired
//	MaestroRepository maestroRepository;
	@Autowired
	MaestroService maestroService;

	private static final String EXPECTED_CLIENTE = "BIMBO";
	private static final String EXPECTED_PAIS = "CO";
	private static final String EXPECTED_TIPO = "PUNTO_VENTA";

	@Before
	public void setup() throws Exception {

		try {
			dynamoDBMapper = new DynamoDBMapper(amazonDynamoDB);
			CreateTableRequest tableRequest = dynamoDBMapper.generateCreateTableRequest(Maestro.class);
			tableRequest.setProvisionedThroughput(new ProvisionedThroughput(1L, 1L));
			KeySchemaElement hashKey = new KeySchemaElement("id", KeyType.HASH);
			List<KeySchemaElement> keySchema = new ArrayList<>();
			keySchema.add(hashKey);
			tableRequest.setKeySchema(keySchema);
			amazonDynamoDB.createTable(tableRequest);
		} catch (ResourceInUseException e) {
			// Do nothing, table already created
		}

		dynamoDBMapper.batchDelete((List<Maestro>) maestroService.findAll());
	}
	
	@Test
	public void findByClienteAndPaisAndTipo() {

		MaestroDTO maestroBimbo = new MaestroDTO();
		maestroBimbo.setTipo(EXPECTED_TIPO);
		maestroBimbo.setCliente(EXPECTED_CLIENTE);
		maestroBimbo.setPais(EXPECTED_PAIS);
		maestroService.save(maestroBimbo);

		Optional<MaestroDTO> result = maestroService.findByClientePaisTipo(EXPECTED_CLIENTE, EXPECTED_PAIS, EXPECTED_TIPO);

		assertTrue(result.isPresent());
		assertTrue("Contains item with expected cost", result.get().getCliente().equals(EXPECTED_CLIENTE));

	}

}
