package com.carvajal.masterdocumentservice.web.rest.util;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.junit.Test;

/**
 * Tests based on parsing algorithm in app/components/util/pagination-util.service.js
 *
 * @see PaginationUtil
 */
public class MaestroUtilUnitTest {
	
    @Test(expected = IllegalArgumentException.class)
    public void validateNull() {
        MaestroUtil.getDay(null);
    }

    @Test
    public void validateMonday() {
        Calendar c = Calendar.getInstance();
        c.setWeekDate(1900, 2, 2);
        String day = MaestroUtil.getDay(c.getTime());
        assertEquals("L", day);
    }
    
    @Test
    public void validateTuesday() {
        Calendar c = Calendar.getInstance();
        c.setWeekDate(1900, 2, 3);
        String day = MaestroUtil.getDay(c.getTime());
        assertEquals("M", day);
    }
    
    @Test
    public void validateWednesday() {
        Calendar c = Calendar.getInstance();
        c.setWeekDate(1900, 2, 4);
        String day = MaestroUtil.getDay(c.getTime());
        assertEquals("W", day);
    }
    
    @Test
    public void validateThursday() {
        Calendar c = Calendar.getInstance();
        c.setWeekDate(1900, 2, 5);
        String day = MaestroUtil.getDay(c.getTime());
        assertEquals("J", day);
    }

    @Test
    public void validateFriday() {
        Calendar c = Calendar.getInstance();
        c.setWeekDate(1900, 2, 6);
        String day = MaestroUtil.getDay(c.getTime());
        assertEquals("V", day);
    }

    @Test
    public void validateSaturday() {
        Calendar c = Calendar.getInstance();
        c.setWeekDate(1900, 2, 7);
        String day = MaestroUtil.getDay(c.getTime());
        assertEquals("S", day);
    }
    
    @Test
    public void validateSunday() {
        Calendar c = Calendar.getInstance();
        c.setWeekDate(1900, 2, 1);
        String day = MaestroUtil.getDay(c.getTime());
        assertEquals("D", day);
    }

}
