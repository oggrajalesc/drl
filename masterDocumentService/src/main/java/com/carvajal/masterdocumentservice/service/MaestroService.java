package com.carvajal.masterdocumentservice.service;

import java.util.List;
import java.util.Optional;

import com.carvajal.masterdocumentservice.domain.Maestro;
import com.carvajal.masterdocumentservice.service.dto.MaestroDTO;

/**
 * Service Interface for managing MaestroDocumento.
 */
public interface MaestroService {

	/**
	 * Save a maestroDocumento.
	 *
	 * @param maestroDocumentoDTO the entity to save
	 * @return the persisted entity
	 */
	MaestroDTO save(MaestroDTO maestroDocumentoDTO);

	/**
	 * Get all the maestroDocumentos.
	 *
	 * @return the list of entities
	 */
	List<Maestro> findAll();

	/**
	 * Get the "id" maestroDocumento.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	Optional<MaestroDTO> findOne(String id);

	/**
	 * Delete the "id" maestroDocumento.
	 *
	 * @param id the id of the entity
	 */
	void delete(String id);

	/**
	 * Obtener maestro por cliente pais y tipo
	 * @return
	 */
	Optional<MaestroDTO> findByClientePaisTipo(String cliente, String pais, String tipo);
}
