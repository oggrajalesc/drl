package com.carvajal.masterdocumentservice.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carvajal.masterdocumentservice.domain.Maestro;
import com.carvajal.masterdocumentservice.domain.PuntoVenta;
import com.carvajal.masterdocumentservice.repositories.MaestroRepository;
import com.carvajal.masterdocumentservice.repositories.PuntoVentaRepository;
import com.carvajal.masterdocumentservice.service.PuntoVentaService;
import com.carvajal.masterdocumentservice.service.dto.PuntoVentaDTO;
import com.carvajal.masterdocumentservice.service.mapper.PuntoVentaMapper;
import com.carvajal.masterdocumentservice.web.rest.util.MaestroUtil;
/**
 * Service Implementation for managing PuntoVenta.
 */
@Service
public class PuntoVentaServiceImpl implements PuntoVentaService {

    private final Logger log = LoggerFactory.getLogger(PuntoVentaServiceImpl.class);
    @Autowired
    private PuntoVentaRepository puntoVentaRepository;
    @Autowired
    private PuntoVentaMapper puntoVentaMapper;
    @Autowired
    private MaestroRepository maestroRepository;


    /**
     * Save a puntoVenta.
     *
     * @param puntoVentaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PuntoVentaDTO save(PuntoVentaDTO puntoVentaDTO) {
        log.debug("Request to save PuntoVenta : {}", puntoVentaDTO);
        PuntoVenta puntoVenta = puntoVentaMapper.toEntity(puntoVentaDTO);
        puntoVenta = puntoVentaRepository.save(puntoVenta);
        return puntoVentaMapper.toDto(puntoVenta);
    }
    
    /**
     * Save a  list of puntos de venta
     * 
     * @param productosDTO
     */
    public void save(List<PuntoVentaDTO> puntoVentaDTOs) {
        log.debug("Request to save PuntoVentas : {}", puntoVentaDTOs);
        PuntoVenta puntoVenta;
        
        if(Objects.isNull(puntoVentaDTOs) ||  puntoVentaDTOs.isEmpty()) {
        	return;
        }
        List<PuntoVenta> puntoVentas = new ArrayList<>();
        for(PuntoVentaDTO dto : puntoVentaDTOs) {
        	puntoVenta = puntoVentaMapper.toEntity(dto);
        	puntoVentas.add(puntoVenta);
        }
        
        puntoVentaRepository.deleteAll(puntoVentaRepository.findByIdMaestro(puntoVentas.get(0).getIdMaestro()));
        puntoVentaRepository.saveAll(puntoVentas);
    }
    
    /**
     * 
     * @param pais
     * @param cliente
     * @return
     */
    @Override
    public List<PuntoVenta> findPuntosVentaByPaisCliente(String pais, String cliente, String tipo) {
    	List<Maestro> maestros = maestroRepository.findByClienteAndPaisAndTipo(cliente, pais, tipo);
    	List<PuntoVenta> puntosVenta = null;
    	if(maestros != null && !maestros.isEmpty()) {
    		puntosVenta = puntoVentaRepository.findByIdMaestro(maestros.get(0).getId());
    		if(puntosVenta != null && !puntosVenta.isEmpty()) {
        		String day = MaestroUtil.getDay(new Date());  
        		log.info("current date {}", new Date() );
        		log.info("current day {}", day );
        		Stream<PuntoVenta> puntosVentasFrecuencia = puntosVenta.stream().filter(p -> p.getFrecuenciaPedido() != null && p.getFrecuenciaPedido().contains(day));
        		return puntosVentasFrecuencia.collect(Collectors.toList());
    		}
    	} 	
    	return puntosVenta;
    }
    

    /**
     * Get one puntoVenta by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<PuntoVentaDTO> findOne(String id) {
        log.debug("Request to get PuntoVenta : {}", id);
        return puntoVentaRepository.findById(id)
            .map(puntoVentaMapper::toDto);
    }

    /**
     * Delete the puntoVenta by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete PuntoVenta : {}", id);
        puntoVentaRepository.deleteById(id);
    }
}
