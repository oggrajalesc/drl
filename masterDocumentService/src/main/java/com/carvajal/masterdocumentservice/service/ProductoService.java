package com.carvajal.masterdocumentservice.service;

import java.util.List;
import java.util.Optional;

import com.carvajal.masterdocumentservice.domain.Producto;
import com.carvajal.masterdocumentservice.service.dto.ProductoDTO;

/**
 * Service Interface for managing Producto.
 */
public interface ProductoService {

    /**
     * Save a producto.
     *
     * @param productoDTO the entity to save
     * @return the persisted entity
     */
    ProductoDTO save(ProductoDTO productoDTO);


    /**
     * Get the "id" producto.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ProductoDTO> findOne(String id);

    /**
     * Delete the "id" producto.
     *
     * @param id the id of the entity
     */
    void delete(String id);

	List<Producto> findAll();

	void save(List<ProductoDTO> productoDTO);
	
}
