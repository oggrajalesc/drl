package com.carvajal.masterdocumentservice.service.dto;

import java.io.Serializable;

/**
 * A DTO for the PuntoVenta entity.
 */
public class PuntoVentaDTO implements Serializable{
	
	private static final long serialVersionUID = -8726682168649036071L;
	
    private String id;

    private String codigo;

    private String puntoVentaEAN;

    private String nombre;

    private String codigoCentroVenta;

    private String frecuenciaPedido;

    private String incluyeImpuesto;
    
    private String idMaestro;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getPuntoVentaEAN() {
		return puntoVentaEAN;
	}

	public void setPuntoVentaEAN(String puntoVentaEAN) {
		this.puntoVentaEAN = puntoVentaEAN;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoCentroVenta() {
		return codigoCentroVenta;
	}

	public void setCodigoCentroVenta(String codigoCentroVenta) {
		this.codigoCentroVenta = codigoCentroVenta;
	}

	public String getFrecuenciaPedido() {
		return frecuenciaPedido;
	}

	public void setFrecuenciaPedido(String frecuenciaPedido) {
		this.frecuenciaPedido = frecuenciaPedido;
	}

	public String getIncluyeImpuesto() {
		return incluyeImpuesto;
	}

	public void setIncluyeImpuesto(String incluyeImpuesto) {
		this.incluyeImpuesto = incluyeImpuesto;
	}

	public String getIdMaestro() {
		return idMaestro;
	}

	public void setIdMaestro(String idMaestro) {
		this.idMaestro = idMaestro;
	}

	@Override
	public String toString() {
		return "PuntoVentaDTO [id=" + id + ", codigo=" + codigo + ", puntoVentaEAN=" + puntoVentaEAN + ", nombre="
				+ nombre + ", codigoCentroVenta=" + codigoCentroVenta + ", frecuenciaPedido=" + frecuenciaPedido
				+ ", incluyeImpuesto=" + incluyeImpuesto + ", idMaestro=" + idMaestro + "]";
	}
}
