package com.carvajal.masterdocumentservice.service.mapper;

import org.mapstruct.Mapper;

import com.carvajal.masterdocumentservice.domain.Maestro;
import com.carvajal.masterdocumentservice.service.dto.MaestroDTO;

/**
 * Mapper for the entity MaestroDocumento and its DTO MaestroDocumentoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MaestroMapper extends EntityMapper<MaestroDTO, Maestro> {

	@Override
	default MaestroDTO toDto(Maestro entity) {
		MaestroDTO dto = new MaestroDTO();
		dto.setCliente(entity.getCliente());
		dto.setFecha(entity.getFecha());
		dto.setId(entity.getId());
		dto.setPais(entity.getPais());
		dto.setTipo(entity.getTipo());
		dto.setRestricciones(entity.getRestricciones());
		return dto;
	}

}
