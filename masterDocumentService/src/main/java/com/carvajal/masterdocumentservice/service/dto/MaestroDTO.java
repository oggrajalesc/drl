package com.carvajal.masterdocumentservice.service.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.carvajal.masterdocumentservice.domain.Maestro.Restriccion;

/**
 * A DTO for the MaestroDocumento entity.
 */
public class MaestroDTO implements Serializable {

	private static final long serialVersionUID = 6753347108600552303L;
	private String id;
	private String pais;
	private String cliente;
	private String tipo;
	private Date fecha;
    private List<Restriccion> restricciones;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<Restriccion> getRestricciones() {
		return restricciones;
	}

	public void setRestricciones(List<Restriccion> restricciones) {
		this.restricciones = restricciones;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MaestroDTO maestroDocumentoDTO = (MaestroDTO) o;
        if (maestroDocumentoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), maestroDocumentoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MaestroDocumentoDTO{" +
            "id=" + getId() +
            ", pais='" + getPais() + "'" +
            ", cliente='" + getCliente() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", fecha='" + getFecha() + "'" +
            "}";
    }
}
