package com.carvajal.masterdocumentservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carvajal.masterdocumentservice.domain.Producto;
import com.carvajal.masterdocumentservice.repositories.ProductoRepository;
import com.carvajal.masterdocumentservice.service.ProductoService;
import com.carvajal.masterdocumentservice.service.dto.ProductoDTO;
import com.carvajal.masterdocumentservice.service.mapper.ProductoMapper;
import com.google.common.collect.Lists;
/**
 * Service Implementation for managing Producto.
 */
@Service
public class ProductoServiceImpl implements ProductoService {

    private final Logger log = LoggerFactory.getLogger(ProductoServiceImpl.class);
    
    @Autowired
    private ProductoRepository productoRepository;
   
    private ProductoMapper productoMapper;



    /**
     * Save a producto.
     *
     * @param productoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProductoDTO save(ProductoDTO productoDTO) {
        log.debug("Request to save Producto : {}", productoDTO);
        Producto producto = productoMapper.toEntity(productoDTO);
        producto = productoRepository.save(producto);
        return productoMapper.toDto(producto);
    }
    
    /**
     * Save a producto.
     *
     * @param productoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public void save(List<ProductoDTO> productosDTO) {
        log.debug("Request to save Producto : {}", productosDTO);
        if(Objects.isNull(productosDTO) ||  productosDTO.isEmpty()) {
        	return;
        }
        Producto producto;
        List<Producto> productos = new ArrayList<>();
        for(ProductoDTO dto : productosDTO) {
        	producto = productoMapper.toEntity(dto);
        	productos.add(producto);
        }
        List<Producto> productosBD = productoRepository.findByIdMaestro(productos.get(0).getIdMaestro());
        if(productosBD != null && !productosBD.isEmpty()) {
            productoRepository.deleteAll(productosBD);
        }
        productoRepository.saveAll(productos);
    }

    /**
     * Get all the productos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public List<Producto> findAll() {
        log.debug("Request to get all Productos");
        return  Lists.newArrayList(productoRepository.findAll());
    }


    /**
     * Get one producto by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<ProductoDTO> findOne(String id) {
        log.debug("Request to get Producto : {}", id);
        return productoRepository.findById(id)
            .map(productoMapper::toDto);
    }

    /**
     * Delete the producto by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Producto : {}", id);
        productoRepository.deleteById(id);
    }
}
