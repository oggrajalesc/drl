package com.carvajal.masterdocumentservice.service;

import java.util.List;
import java.util.Optional;

import com.carvajal.masterdocumentservice.domain.PuntoVenta;
import com.carvajal.masterdocumentservice.service.dto.PuntoVentaDTO;

/**
 * Service Interface for managing PuntoVenta.
 */
public interface PuntoVentaService {

    /**
     * Save a puntoVenta.
     *
     * @param puntoVentaDTO the entity to save
     * @return the persisted entity
     */
    PuntoVentaDTO save(PuntoVentaDTO puntoVentaDTO);

    /**
     * Get the "id" puntoVenta.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PuntoVentaDTO> findOne(String id);

    /**
     * Delete the "id" puntoVenta.
     *
     * @param id the id of the entity
     */
    void delete(String id);

	void save(List<PuntoVentaDTO> puntoVentaDTOs);

	List<PuntoVenta> findPuntosVentaByPaisCliente(String pais, String cliente, String tipo);
    
    
}
