package com.carvajal.masterdocumentservice.service.mapper;

import org.mapstruct.Mapper;

import com.carvajal.masterdocumentservice.domain.Producto;
import com.carvajal.masterdocumentservice.service.dto.ProductoDTO;

/**
 * Mapper for the entity Producto and its DTO ProductoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProductoMapper extends EntityMapper<ProductoDTO, Producto> {

}
