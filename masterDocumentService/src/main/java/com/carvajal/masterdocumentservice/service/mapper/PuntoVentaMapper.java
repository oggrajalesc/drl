package com.carvajal.masterdocumentservice.service.mapper;

import org.mapstruct.Mapper;

import com.carvajal.masterdocumentservice.domain.PuntoVenta;
import com.carvajal.masterdocumentservice.service.dto.PuntoVentaDTO;

/**
 * Mapper for the entity PuntoVenta and its DTO PuntoVentaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PuntoVentaMapper extends EntityMapper<PuntoVentaDTO, PuntoVenta> {


}
