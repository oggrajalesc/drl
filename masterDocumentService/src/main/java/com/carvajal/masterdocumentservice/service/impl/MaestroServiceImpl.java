package com.carvajal.masterdocumentservice.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carvajal.masterdocumentservice.domain.Maestro;
import com.carvajal.masterdocumentservice.repositories.MaestroRepository;
import com.carvajal.masterdocumentservice.service.MaestroService;
import com.carvajal.masterdocumentservice.service.dto.MaestroDTO;
import com.carvajal.masterdocumentservice.service.mapper.MaestroMapper;
import com.google.common.collect.Lists;

/**
 * Service Implementation for managing MaestroDocumento.
 */
@Service
public class MaestroServiceImpl implements MaestroService {

	private final Logger log = LoggerFactory.getLogger(MaestroServiceImpl.class);

	@Autowired
	private MaestroRepository maestroRepository;
	
	@Autowired
	private MaestroMapper maestroMapper;


	/**
	 * Save a maestroDocumento.
	 *
	 * @param maestroDocumentoDTO the entity to save
	 * @return the persisted entity
	 */
	@Override
	public MaestroDTO save(MaestroDTO maestroDocumentoDTO) {
		log.debug("Request to save MaestroDocumento : {}", maestroDocumentoDTO);
		Maestro maestroDocumento = maestroMapper.toEntity(maestroDocumentoDTO);
		maestroDocumento = maestroRepository.save(maestroDocumento);
		return maestroMapper.toDto(maestroDocumento);
	}

	/**
	 * Get all the maestroDocumentos.
	 *
	 * @return the list of entities
	 */
	@Override
	public List<Maestro> findAll() {
		log.debug("Request to get all MaestroDocumentos");
		return Lists.newArrayList(maestroRepository.findAll());
	}

	/**
	 * Get one maestroDocumento by id.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	@Override
	public Optional<MaestroDTO> findOne(String id) {
		log.debug("Request to get MaestroDocumento : {}", id);
		return maestroRepository.findById(id).map(maestroMapper::toDto);
	}

	/**
	 * Delete the maestroDocumento by id.
	 *
	 * @param id the id of the entity
	 */
	@Override
	public void delete(String id) {
		log.debug("Request to delete MaestroDocumento : {}", id);
		maestroRepository.deleteById(id);
	}
	
	/**
	 * Obtener las restricciones de un maestro.
	 *
	 * @return the list of entities
	 */
	@Override
	public Optional<MaestroDTO> findByClientePaisTipo(String cliente, String pais, String tipo) {
		log.debug("Request to get all Restrictions by cliente pais y tipo");
		List<Maestro> maestros = Lists.newArrayList(maestroRepository.findByClienteAndPaisAndTipo(cliente, pais, tipo));
		log.debug("Resultado: {}", maestros);
		MaestroDTO maestroDTO = maestros !=null && !maestros.isEmpty() ? maestroMapper.toDto(maestros.get(0)) : null;
		return Optional.ofNullable(maestroDTO);
	}
	
}
