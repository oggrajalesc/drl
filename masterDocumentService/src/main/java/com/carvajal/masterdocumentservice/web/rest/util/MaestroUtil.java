package com.carvajal.masterdocumentservice.web.rest.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Utility class for business generic methods
 * 
 * @author Djaramillo
 */
public final class MaestroUtil {

	private MaestroUtil() {

	}

	/**
	 * Metodo para obtener el dia segun la
	 * 
	 * @param fecha
	 * @return
	 */
	public static String getDay(Date fecha) {
		
		if(fecha == null) {
			throw new IllegalArgumentException("Date can't be null");
		}
				
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

		String mappedDay;
		switch (dayOfWeek) {
		case 1 :
			mappedDay = "D";
			break;
		case 2 :
			mappedDay = "L";
			break;
		case 3 :
			mappedDay = "M";
			break;
		case 4 :
			mappedDay = "W";
			break;
		case 5 :
			mappedDay = "J";
			break;
		case 6 :
			mappedDay = "V";
			break;
		case 7 :
			mappedDay = "S";
			break;
		default:
			throw new IllegalArgumentException("Invalid day of the week: " + dayOfWeek);
		}

		return mappedDay;
	}

}
