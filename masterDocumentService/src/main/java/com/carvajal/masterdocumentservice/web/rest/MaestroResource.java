package com.carvajal.masterdocumentservice.web.rest;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.masterdocumentservice.service.MaestroService;
import com.carvajal.masterdocumentservice.service.dto.MaestroDTO;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing MaestroDocumento.
 */
@RestController
@RequestMapping("/api")
public class MaestroResource {

    private final Logger log = LoggerFactory.getLogger(MaestroResource.class);

    private final MaestroService maestroDocumentoService;

    public MaestroResource(MaestroService maestroDocumentoService) {
        this.maestroDocumentoService = maestroDocumentoService;
    }

    /**
     * GET  /maestro-documentos/:id : get the "id" maestroDocumento.
     *
     * @param id the id of the maestroDocumentoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the maestroDocumentoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/maestro-documentos/{id}")
    @Timed
    public ResponseEntity<MaestroDTO> getMaestroDocumento(@PathVariable String id) {
        log.debug("REST request to get MaestroDocumento : {}", id);
        Optional<MaestroDTO> maestroDocumentoDTO = maestroDocumentoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(maestroDocumentoDTO);
    }
    
    /**
     * GET  /maestro-documentos/:maestro : get the "id" maestroDocumento.
     *
     * @param id the id of the maestroDocumentoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the maestroDocumentoDTO, or with status 404 (Not Found)
     */
    @PostMapping("/maestro-documentos")
    @Timed
    public ResponseEntity<MaestroDTO> getMaestroDocumento(@Valid @RequestBody MaestroDTO maestro) {
        log.debug("REST request to get MaestroDocumento : {}", maestro);
        Optional<MaestroDTO> maestroDocumentoDTO = maestroDocumentoService.findByClientePaisTipo(maestro.getCliente(), maestro.getPais(), maestro.getTipo());
        return ResponseUtil.wrapOrNotFound(maestroDocumentoDTO);
    }

}
