/**
 * View Models used by Spring MVC REST controllers.
 */
package com.carvajal.masterdocumentservice.web.rest.vm;
