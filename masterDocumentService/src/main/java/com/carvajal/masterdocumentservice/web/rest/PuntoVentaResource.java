package com.carvajal.masterdocumentservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.masterdocumentservice.domain.PuntoVenta;
import com.carvajal.masterdocumentservice.service.PuntoVentaService;
import com.carvajal.masterdocumentservice.service.dto.MaestroDTO;
import com.carvajal.masterdocumentservice.service.dto.PuntoVentaDTO;
import com.carvajal.masterdocumentservice.web.rest.errors.BadRequestAlertException;
import com.carvajal.masterdocumentservice.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PuntoVenta.
 */
@RestController
@RequestMapping("/api")
public class PuntoVentaResource {

    private final Logger log = LoggerFactory.getLogger(PuntoVentaResource.class);

    private static final String ENTITY_NAME = "puntoVenta";

    private final PuntoVentaService puntoVentaService;

    public PuntoVentaResource(PuntoVentaService puntoVentaService) {
        this.puntoVentaService = puntoVentaService;
    }

    /**
     * POST  /punto-ventas : Create a new puntoVenta.
     *
     * @param puntoVentaDTO the puntoVentaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new puntoVentaDTO, or with status 400 (Bad Request) if the puntoVenta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/punto-ventas")
    @Timed
    public ResponseEntity<PuntoVentaDTO> createPuntoVenta(@Valid @RequestBody PuntoVentaDTO puntoVentaDTO) throws URISyntaxException {
        log.debug("REST request to save PuntoVenta : {}", puntoVentaDTO);
        if (puntoVentaDTO.getId() != null) {
            throw new BadRequestAlertException("A new puntoVenta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PuntoVentaDTO result = puntoVentaService.save(puntoVentaDTO);
        return ResponseEntity.created(new URI("/api/punto-ventas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }
    
    /**
     * POST  /productos : Create a new list of producto.
     *
     * @param productoDTO the productoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productoDTO, or with status 400 (Bad Request) if the producto has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/punto-ventas/saveAll")
    @Timed
    public ResponseEntity<Object> createPuntoVentas(@Valid @RequestBody List<PuntoVentaDTO> puntoVentaDTOs) throws URISyntaxException {
        log.debug("REST request to save puntoVentas : {}", puntoVentaDTOs);
        puntoVentaService.save(puntoVentaDTOs);
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /punto-ventas : Updates an existing puntoVenta.
     *
     * @param puntoVentaDTO the puntoVentaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated puntoVentaDTO,
     * or with status 400 (Bad Request) if the puntoVentaDTO is not valid,
     * or with status 500 (Internal Server Error) if the puntoVentaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/punto-ventas")
    @Timed
    public ResponseEntity<PuntoVentaDTO> updatePuntoVenta(@Valid @RequestBody PuntoVentaDTO puntoVentaDTO) throws URISyntaxException {
        log.debug("REST request to update PuntoVenta : {}", puntoVentaDTO);
        if (puntoVentaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PuntoVentaDTO result = puntoVentaService.save(puntoVentaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, puntoVentaDTO.getId()))
            .body(result);
    }
    
    /**
     * GET  /punto-ventas : get all the puntoVentas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of puntoVentas in body
     */
    @PostMapping("/punto-ventas-frecuencia")
    @Timed
    public ResponseEntity<List<PuntoVenta>> getPuntoVentasByFilter(@Valid @RequestBody MaestroDTO maestroDTO) {
        log.debug("REST request to get a page of PuntoVentas");
        List<PuntoVenta> puntosVenta = puntoVentaService.findPuntosVentaByPaisCliente(maestroDTO.getPais(), maestroDTO.getCliente(), maestroDTO.getTipo());
        return new ResponseEntity<>(puntosVenta, HttpStatus.OK);
    }

    /**
     * GET  /punto-ventas/:id : get the "id" puntoVenta.
     *
     * @param id the id of the puntoVentaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the puntoVentaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/punto-ventas/{id}")
    @Timed
    public ResponseEntity<PuntoVentaDTO> getPuntoVenta(@PathVariable String id) {
        log.debug("REST request to get PuntoVenta : {}", id);
        Optional<PuntoVentaDTO> puntoVentaDTO = puntoVentaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(puntoVentaDTO);
    }

    /**
     * DELETE  /punto-ventas/:id : delete the "id" puntoVenta.
     *
     * @param id the id of the puntoVentaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/punto-ventas/{id}")
    @Timed
    public ResponseEntity<Void> deletePuntoVenta(@PathVariable String id) {
        log.debug("REST request to delete PuntoVenta : {}", id);
        puntoVentaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
