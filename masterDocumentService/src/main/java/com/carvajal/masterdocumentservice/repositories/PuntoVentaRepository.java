package com.carvajal.masterdocumentservice.repositories;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.carvajal.masterdocumentservice.domain.PuntoVenta;

/**
 * Repositorio para hacer las operaciones sobre documentos Maestro
 * 
 * @author Djaramillo
 */
@EnableScan
public interface PuntoVentaRepository extends CrudRepository<PuntoVenta, String>{

	List<PuntoVenta> findByIdMaestro(String idMaestro);
	
}
