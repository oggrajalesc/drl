package com.carvajal.masterdocumentservice.repositories;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.carvajal.masterdocumentservice.domain.Maestro;

/**
 * Repositorio para hacer las operaciones sobre documentos Maestro
 * 
 * @author Djaramillo
 */
@EnableScan
public interface MaestroRepository extends CrudRepository<Maestro, String>{
	
	List<Maestro> findByClienteAndPaisAndTipo(String cliente, String pais, String tipo);

}
