package com.carvajal.masterdocumentservice.repositories;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.carvajal.masterdocumentservice.domain.Producto;

/**
 * Repositorio para hacer las operaciones sobre documentos Maestro
 * 
 * @author Djaramillo
 */
@EnableScan
public interface ProductoRepository extends CrudRepository<Producto, String>{
	
	List<Producto> findByIdMaestro(String idMaestro);

}
