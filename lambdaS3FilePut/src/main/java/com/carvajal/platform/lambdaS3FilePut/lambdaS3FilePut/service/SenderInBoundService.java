package com.carvajal.platform.lambdaS3FilePut.lambdaS3FilePut.service;

import java.nio.charset.Charset;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class SenderInBoundService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SenderInBoundService.class);
	    
    @Value("${app.topicSender.name}")
    private String topic;
    
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;
    
    public void send(String nameFile){
		LOGGER.info("Charset: ", Charset.defaultCharset());
		LOGGER.info("Argmuentos: {}", nameFile);
		
        Properties props = new Properties();
        props.put("bootstrap.servers", bootstrapServers);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);
        LOGGER.info("Inicio Send topico {}", topic);
        producer.send(new ProducerRecord<String, String>(topic, "SenderInBoundService", nameFile));
        LOGGER.info("Fin Send Mensage{}", nameFile);
        producer.close();

    }

    
}
