package com.carvajal.platform.lambdaS3FilePut.lambdaS3FilePut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LambdaS3FilePutApplication {

	public static void main(String[] args) {
		SpringApplication.run(LambdaS3FilePutApplication.class, args);
	}
}
