package com.carvajal.platform.lambdaS3FilePut.lambdaS3FilePut.handler;

import java.nio.charset.Charset;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.carvajal.platform.lambdaS3FilePut.lambdaS3FilePut.service.SenderInBoundService;

@Component("s3BucketFunction")
public class S3BucketFunction implements Function<S3Event, String>{
	private static final Logger LOGGER = LoggerFactory.getLogger(S3BucketFunction.class);
	
	private SenderInBoundService senderInBoundService;
	
	public S3BucketFunction(final SenderInBoundService senderInBoundService) {
		this.senderInBoundService = senderInBoundService;
	}
	
	@Override
	public String apply(S3Event s3Event) {
		try {
			LOGGER.info("Charset: ", Charset.defaultCharset());
			LOGGER.info("Argmuentos: {}", s3Event);

			S3EventNotificationRecord record = s3Event.getRecords().get(0);
			//record.getS3().getBucket().getName();  --> bucket
			// Remove any spaces or unicode non-ASCII characters.
			String nameFile = record.getS3().getObject().getKey().replace('+', ' ');
			
			LOGGER.info("namFile: " + nameFile);
			
		    senderInBoundService.send(nameFile);
		    LOGGER.info("Fin Landa: {}" );
		    
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return "OK";
	}

}
