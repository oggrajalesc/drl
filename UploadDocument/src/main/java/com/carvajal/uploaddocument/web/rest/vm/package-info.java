/**
 * View Models used by Spring MVC REST controllers.
 */
package com.carvajal.uploaddocument.web.rest.vm;
