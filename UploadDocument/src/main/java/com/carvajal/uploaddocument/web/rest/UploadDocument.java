package com.carvajal.uploaddocument.web.rest;

import java.net.URISyntaxException;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.platform.core.dto.DocumentoS3DTO;
import com.carvajal.uploaddocument.service.UploadDocumentService;
import com.codahale.metrics.annotation.Timed;

/**
 * Controller for uploading a document
 */
@RestController
@RequestMapping("/api")
public class UploadDocument {

    private final UploadDocumentService uploadDocumentService;

    public UploadDocument(UploadDocumentService uploadDocumentService) {
        this.uploadDocumentService = uploadDocumentService;
    }

	/**
	 * POST /uploadDocument : Upload a given document into S3.
	 *
	 * @param Document the document to upload
	 * @return the ResponseEntity with status 201 (Uploaded) and with body the new
	 *         document, or with status 400 (Bad Request) if the document doesn't meet its requirements
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/uploadDocument")
	@Timed
	public ResponseEntity<Void> uploadDocument(@Valid @RequestBody DocumentoS3DTO document) throws URISyntaxException {
		try {
			uploadDocumentService.uploadDocument(document);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

}
