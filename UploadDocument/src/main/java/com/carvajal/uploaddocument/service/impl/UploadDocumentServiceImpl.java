package com.carvajal.uploaddocument.service.impl;

import java.io.File;

import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.carvajal.platform.core.dto.DocumentoS3DTO;
import com.carvajal.uploaddocument.service.UploadDocumentService;

/**
 * 
 * 
 * @author Djaramillo
 *
 */
@Service
public class UploadDocumentServiceImpl implements UploadDocumentService {

	/**
	 * @see UploadDocumentService#uploadDocument(DocumentoS3DTO)
	 */
	@Override
	public void uploadDocument(DocumentoS3DTO document) { 
		File file = getFile(document.getRutaArchivo() + document.getNombreDocumento());
		if (file != null) {
			AWSCredentials credentials = getCredentials();
			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(getRegion(document))
					.build();
			s3client.putObject(document.getNombreBucket(), document.getNombreDocumento(), file);
		}
	}

	/**
	 * Metodo para obtener un archivo en base a su path
	 * @param path
	 * @return
	 */
	private File getFile(String path) {
		File file = new File(path);
		if (file.exists() && !file.isDirectory()) {
			return file;
		} else {
			return null;
		}
	}

	/**
	 * Metodo para obtener las credenciales de aws
	 * @return
	 */
	private AWSCredentials getCredentials() {
		return new BasicAWSCredentials("AKIAJ6BXZ4WF7BEFBIZQ", "AQumv8pWqYzDrYBFp9YrCqTN9EkIFkT4O8giQ9IN");
	}

	/**
	 * Metodo para obtener una region de aws en base a su nombre
	 * @param document
	 * @return
	 */
	private Regions getRegion(DocumentoS3DTO document) {
		Regions region;
		switch (document.getRegionName()) {
		case "us-east-1":
			region = Regions.US_EAST_1;
			break;
		case "us-west-1":
			region = Regions.US_WEST_1;
			break;
		case "eu-west-1":
			region = Regions.EU_WEST_1;
			break;
		case "ap-southeast-1":
			region = Regions.AP_SOUTH_1;
			break;
		case "ap-southeast-2":
			region = Regions.AP_SOUTHEAST_2;
			break;
		case "ap-northeast-1":
			region = Regions.AP_NORTHEAST_1;
			break;
		case "sa-east-1":
			region = Regions.SA_EAST_1;
			break;
		default:
			region = Regions.US_WEST_2;
		}
		return region;
	}

}
