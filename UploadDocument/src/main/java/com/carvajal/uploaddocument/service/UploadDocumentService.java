package com.carvajal.uploaddocument.service;

import com.carvajal.platform.core.dto.DocumentoS3DTO;

/**
 * 
 * 
 * @author Djaramillo
 *
 */
public interface UploadDocumentService {

	/**
	 * Metodo para subir un documento a S3
	 * @param document
	 * @throws Exception
	 */
	void uploadDocument(DocumentoS3DTO document) throws Exception;
	
}
