package com.carvajal.mrndaemon.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

public class OrdenCompraDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2212333955166014601L;

	private Long id;

	private Long idOrdenCompraPadre;

	@NotNull
	private String codigo;

	@NotNull
	private Date fechaOrdenCompra;

	@NotNull
	private Date fechaEntrega;

	@NotNull
	private String codPais;

	@NotNull
	private String codCliente;

	@NotNull
	private String nombreCliente;

	@NotNull
	private String estado;

	private String idFile;

	private String idJson;

	private Long ordenComprapadreId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdOrdenCompraPadre() {
		return idOrdenCompraPadre;
	}

	public void setIdOrdenCompraPadre(Long idOrdenCompraPadre) {
		this.idOrdenCompraPadre = idOrdenCompraPadre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Date getFechaOrdenCompra() {
		return fechaOrdenCompra;
	}

	public void setFechaOrdenCompra(Date fechaOrdenCompra) {
		this.fechaOrdenCompra = fechaOrdenCompra;
	}

	public Date getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public String getCodPais() {
		return codPais;
	}

	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}

	public String getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getIdFile() {
		return idFile;
	}

	public void setIdFile(String idFile) {
		this.idFile = idFile;
	}

	public String getIdJson() {
		return idJson;
	}

	public void setIdJson(String idJson) {
		this.idJson = idJson;
	}

	public Long getOrdenComprapadreId() {
		return ordenComprapadreId;
	}

	public void setOrdenComprapadreId(Long ordenCompraId) {
		this.ordenComprapadreId = ordenCompraId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		OrdenCompraDTO ordenCompraDTO = (OrdenCompraDTO) o;
		if (ordenCompraDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), ordenCompraDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "OrdenCompraDTO{" + "id=" + getId() + ", idOrdenCompraPadre=" + getIdOrdenCompraPadre() + ", codigo='"
				+ getCodigo() + "'" + ", fechaOrdenCompra='" + getFechaOrdenCompra() + "'" + ", fechaEntrega='"
				+ getFechaEntrega() + "'" + ", codPais='" + getCodPais() + "'" + ", codCliente='" + getCodCliente()
				+ "'" + ", nombreCliente='" + getNombreCliente() + "'" + ", estado='" + getEstado() + "'" + ", idFile='"
				+ getIdFile() + "'" + ", idJson='" + getIdJson() + "'" + ", ordenComprapadre=" + getOrdenComprapadreId()
				+ "}";
	}
}
