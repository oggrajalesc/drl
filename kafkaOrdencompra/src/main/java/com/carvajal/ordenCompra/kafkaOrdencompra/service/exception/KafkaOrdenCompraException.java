package com.carvajal.ordenCompra.kafkaOrdencompra.service.exception;

public class KafkaOrdenCompraException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4877272990897425727L;

	public KafkaOrdenCompraException() {
		super();
	}
	
	public KafkaOrdenCompraException(String message) {
		super("----"+message+"----");
	}
}
