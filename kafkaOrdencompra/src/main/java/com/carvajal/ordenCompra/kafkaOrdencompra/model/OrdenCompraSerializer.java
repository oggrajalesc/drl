package com.carvajal.ordenCompra.kafkaOrdencompra.model;

import java.util.Map;

import org.apache.kafka.common.serialization.Serializer;

import com.carvajal.mrndaemon.model.OrdenCompraDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OrdenCompraSerializer implements Serializer<OrdenCompraDTO> {
	 
		@Override
		public byte[] serialize(String arg0, OrdenCompraDTO ordenCommpra) {
			byte[] serializedBytes = null;
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				serializedBytes = objectMapper.writeValueAsString(ordenCommpra).getBytes();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return serializedBytes;
		}
	 
		@Override
		public void close() {
		}
	 
		@Override
		public void configure(Map<String, ?> arg0, boolean arg1) {
		}
}
