package com.carvajal.ordenCompra.kafkaOrdencompra;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carvajal.ordenCompra.kafkaOrdencompra.newOrden.ProducerOrden;

@Component
public class ScheduleProducerOrdenNueva {
    private static final Logger log = LoggerFactory.getLogger(ScheduleProducerOrdenNueva.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private ProducerOrden producerOrden;   
   
//    @Scheduled(fixedRate = 5000)
//    public void reportCurrentTime() {
//        log.info("The time is now {}", dateFormat.format(new Date()));
//        
//		OrdenCompraDTO myOrden = new OrdenCompraDTO();
//		myOrden.setId(1L);
//		myOrden.setCodigo("C");
//		myOrden.setFechaOrdenCompra(new Date());
//		myOrden.setFechaEntrega(new Date());
//		myOrden.setCodPais("123");
//		myOrden.setCodCliente("1234");
//		myOrden.setNombreCliente("bimbo");
//		myOrden.setEstado("ACT");
//		producerOrden.send(myOrden);
//		
//    }
}
