package com.carvajal.ordenCompra.kafkaOrdencompra.newOrden;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.carvajal.mrndaemon.model.OrdenCompraDTO;
import com.carvajal.ordenCompra.kafkaOrdencompra.service.JbpmClientConsumeService;
import com.carvajal.ordenCompra.kafkaOrdencompra.service.exception.KafkaOrdenCompraException;

@Service
public class ConsumerOrden {
    private static final Logger LOG = LoggerFactory.getLogger(ConsumerOrden.class);
	
    @Autowired
    private JbpmClientConsumeService jbpmClientConsumeService;

    @KafkaListener(topics = "${app.topic.foo}")
    public void listen(OrdenCompraDTO ordenCompra) {
        LOG.info("received ordenCompra='{}'", ordenCompra.getId());
        if(!Objects.isNull(ordenCompra)) {
        	try {
    			jbpmClientConsumeService.callJbpm(ordenCompra);
    		} catch (KafkaOrdenCompraException e) {
    			e.printStackTrace();
    		}
        }
    }
}
