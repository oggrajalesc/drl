package com.carvajal.ordenCompra.kafkaOrdencompra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class KafkaOrdencompraApplication{

	public static void main(String[] args) {
		SpringApplication.run(KafkaOrdencompraApplication.class, args);
	}

}
