package com.carvajal.ordenCompra.kafkaOrdencompra;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ScheduleConsumerOrdenNueva {
    private static final Logger log = LoggerFactory.getLogger(ScheduleConsumerOrdenNueva.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
}
