package com.carvajal.ordenCompra.kafkaOrdencompra.newOrden;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.carvajal.mrndaemon.model.OrdenCompraDTO;

@Service
public class ProducerOrden {
    private static final Logger LOG = LoggerFactory.getLogger(ProducerOrden.class);

    @Autowired
    private KafkaTemplate<String, OrdenCompraDTO> kafkaTemplate;

    @Value("${app.topic.foo}")
    private String topic;
    
    public void send(OrdenCompraDTO oc){
    	LOG.info("msg:"+oc.getId());
    	kafkaTemplate.send(new ProducerRecord<String, OrdenCompraDTO>(topic, oc));
    }
    
}
