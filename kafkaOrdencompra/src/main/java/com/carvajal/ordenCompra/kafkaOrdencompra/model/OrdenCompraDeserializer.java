package com.carvajal.ordenCompra.kafkaOrdencompra.model;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;

import com.carvajal.mrndaemon.model.OrdenCompraDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OrdenCompraDeserializer implements Deserializer<OrdenCompraDTO>{
	@Override
	public OrdenCompraDTO deserialize(String arg0, byte[] devBytes) {
		ObjectMapper mapper = new ObjectMapper();
		OrdenCompraDTO ordenCompra = null;
		try {
			ordenCompra = mapper.readValue(devBytes, OrdenCompraDTO.class);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return ordenCompra;
	}

	@Override
	public void close() {
	}

	@Override
	public void configure(Map<String, ?> arg0, boolean arg1) {
	}
}
