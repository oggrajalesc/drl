package com.carvajal.ordenCompra.kafkaOrdencompra.service.impl;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.carvajal.mrndaemon.model.OrdenCompraDTO;
import com.carvajal.ordenCompra.kafkaOrdencompra.service.JbpmClientConsumeService;
import com.carvajal.ordenCompra.kafkaOrdencompra.service.exception.KafkaOrdenCompraException;

@Service
public class JbpmClientConsumeServiceImpl implements JbpmClientConsumeService{
	
	private static final String URL_INICiAR_JBPM = "http://localhost:8094/iniciarOrden";
	
    private static final Logger log = LoggerFactory.getLogger(JbpmClientConsumeServiceImpl.class);
	
    
    @Override
    /**
     * @see
     * @throws
     */
	public void callJbpm(OrdenCompraDTO ordenCompra) throws KafkaOrdenCompraException {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Content-Type", "application/json");

//		JSONObject json = new JSONObject();
//		json.put("ordenCompra", ordenCompra);
		HttpEntity <OrdenCompraDTO> httpEntity = new HttpEntity <OrdenCompraDTO> (ordenCompra, httpHeaders);        
		
		RestTemplate restTemplate = new RestTemplate();
//		restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        
        Integer responseJbpm = restTemplate.postForObject(URL_INICiAR_JBPM, httpEntity, Integer.class);
        log.info(responseJbpm.toString());
		
	}

}
