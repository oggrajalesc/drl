package com.carvajal.ordenCompra.kafkaOrdencompra.service;

import com.carvajal.mrndaemon.model.OrdenCompraDTO;
import com.carvajal.ordenCompra.kafkaOrdencompra.service.exception.KafkaOrdenCompraException;

public interface JbpmClientConsumeService {
	void callJbpm(OrdenCompraDTO ordenCompra) throws KafkaOrdenCompraException;
}
