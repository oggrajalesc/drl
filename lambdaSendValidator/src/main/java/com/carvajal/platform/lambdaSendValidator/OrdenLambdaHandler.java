package com.carvajal.platform.lambdaSendValidator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.carvajal.platform.lambdaSendValidator.kafka.sender.service.SenderInBoundService;

public class OrdenLambdaHandler implements RequestHandler<S3Event, String> {

	public OrdenLambdaHandler() {
		
	}
	
	private ApplicationContext springContext;
	
	private SenderInBoundService senderInBoundService = new SenderInBoundService();
    
	@Override
	public String handleRequest(S3Event s3Event, Context context) {
		try {
			context.getLogger().log(senderInBoundService.toString());
			context.getLogger().log("Input: " + s3Event);

			S3EventNotificationRecord record = s3Event.getRecords().get(0);
			String srcBucket = record.getS3().getBucket().getName();
			// Remove any spaces or unicode non-ASCII characters.
			String nameFile = record.getS3().getObject().getKey().replace('+', ' ');
			
			context.getLogger().log("srcKey: " + nameFile);
//			
//			BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIYE25NQHTDOQO4UQ", "BSg+ZDneXNz13WQFTlbe190fbDsukZDqj4RWPlFc");
//        	AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
//        	                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
//        	                        .withRegion(Regions.US_EAST_2)
//        	                        .build(); 		
        	
//		    S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcBucket, nameFile));
		    
		    senderInBoundService.send(nameFile);

//		    String metaData= s3Object.getObjectMetadata().getContentType();
//		    context.getLogger().log("metaData: " + metaData);
		    
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return "OK";
	}

}
