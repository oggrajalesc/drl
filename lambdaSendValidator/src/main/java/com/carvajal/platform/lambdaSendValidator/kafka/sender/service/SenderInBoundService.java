package com.carvajal.platform.lambdaSendValidator.kafka.sender.service;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class SenderInBoundService {
	private static final Logger LOG = LoggerFactory.getLogger(SenderInBoundService.class);

//    private KafkaTemplate<String, String> kafkaTemplate = kafkaTemplate();
    
    private String bootstrapServers = "ec2-54-190-123-99.us-west-2.compute.amazonaws.com:9092";

//    @Value("${app.topic.name}")
    private String topic = "s3-lambda-push";
    
    public void send(String nameFile){

    	
    	LOG.info("msg:"+nameFile);
//    	KafkaTemplate<String, String> kafkaTemplate = kafkaTemplate();
//        Message<String> message = MessageBuilder
//                .withPayload(nameFile)
//                .setHeader(KafkaHeaders.TOPIC, topic)
//                .build();
//    	
//    	kafkaTemplate.send(message);
    	Producer<String,String> producer = createProducer();
    		long time = System.currentTimeMillis(); 

    	ProducerRecord<String, String> record =
                new ProducerRecord<>(topic,nameFile+time,nameFile);    	
    	
    	try {
			RecordMetadata metadata = producer.send(record).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	LOG.info("msg:"+nameFile);
    }

	private static Producer<String, String> createProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
        		"ec2-54-190-123-99.us-west-2.compute.amazonaws.com:9092");
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "SenderInBoundService");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
        		StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                                    StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }
    
//    public Map<String, Object> producerConfigs() {
//        Map<String, Object> props = new HashMap<>();
//        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "ec2-54-190-123-99.us-west-2.compute.amazonaws.com:9092");
//        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        return props;
//    }
//
//    public ProducerFactory<String, String> producerFactory() {
//        return new DefaultKafkaProducerFactory<>(producerConfigs());
//    }
//
//
//    public KafkaTemplate<String, String> kafkaTemplate() {
//        return new KafkaTemplate<>(producerFactory());
//    }  
    
}
