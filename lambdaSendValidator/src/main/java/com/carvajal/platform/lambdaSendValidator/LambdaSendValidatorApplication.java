package com.carvajal.platform.lambdaSendValidator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class LambdaSendValidatorApplication extends SpringBootServletInitializer{
	
	public static void main(String[] args) {
		SpringApplication.run(LambdaSendValidatorApplication.class, args);
	}
	
}
