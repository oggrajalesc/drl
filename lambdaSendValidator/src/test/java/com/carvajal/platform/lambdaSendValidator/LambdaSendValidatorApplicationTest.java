package com.carvajal.platform.lambdaSendValidator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.carvajal.platform.lambdaSendValidator.kafka.sender.service.SenderInBoundService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class LambdaSendValidatorApplicationTest {
	
	@Test
	public void testkafkaProducer() {
		SenderInBoundService order= new SenderInBoundService();
		order.send("mifile3");
	}	

}
