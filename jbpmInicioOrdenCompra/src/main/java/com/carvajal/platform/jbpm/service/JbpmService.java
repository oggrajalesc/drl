package com.carvajal.platform.jbpm.service;

import com.carvajal.platform.jbpm.service.exception.JbpmException;

public interface JbpmService {
	void inicaiarJbpm() throws JbpmException;
}
