package com.carvajal.platform.jbpm.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carvajal.platform.jbpm.config.JpmClientConfig;
import com.carvajal.platform.jbpm.service.JbpmService;
import com.carvajal.platform.jbpm.service.exception.JbpmException;

@Service
public class JbpmServiceImpl implements JbpmService{

	@Autowired
	private JpmClientConfig jpmClientConfig;
	
	@Override
	public void inicaiarJbpm() throws JbpmException {
		jpmClientConfig.init();
	}

}
