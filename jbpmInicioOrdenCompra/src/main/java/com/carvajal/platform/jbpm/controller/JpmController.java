package com.carvajal.platform.jbpm.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carvajal.platform.jbpm.dtos.OrdenCompraDTO;
import com.carvajal.platform.jbpm.service.JbpmService;
import com.carvajal.platform.jbpm.service.exception.JbpmException;

@RestController
public class JpmController {
    private static final Logger log = LoggerFactory.getLogger(JpmController.class);
	
	@Autowired
	private JbpmService jbpmService;

	@RequestMapping(value="/iniciarOrden", method = RequestMethod.POST)
    public int iniciarOrden(@RequestBody OrdenCompraDTO ordenCompra) {
		log.info("Orden compra ini:"+ordenCompra.getId());
		try {
			jbpmService.inicaiarJbpm();
		} catch (JbpmException e) {
			// TODO Auto-generated catch block
			 return -1;
		}
        return 0;
    }
}
