package com.carvajal.platform.jbpm.config;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.definition.ProcessDefinition;
import org.kie.server.api.model.instance.NodeInstance;
import org.kie.server.api.model.instance.ProcessInstance;
import org.kie.server.api.model.instance.VariableInstance;
import org.kie.server.api.model.instance.WorkItemInstance;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.ProcessServicesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class JpmClientConfig {
    private static final Logger log = LoggerFactory.getLogger(JpmClientConfig.class);
    
	private static final String URL = "http://p-dhernandezm2:8080/kie-server/services/rest/server";
	private static final String user = System.getProperty("username","daniel");
	private static final String password = System.getProperty("password","daniel");
	private static final String CONTAINER = "orden_compa_concepto_1.0.0";											 
	private static final String PROCESS_ID = "src.ordem-compra-ppal2";
	
	public static void main(String[] args) {
		JpmClientConfig jb = new JpmClientConfig();
		jb.init();
	}
	
	public void init(){
		log.info("init JBPM");
		launchProcess();
//		datosclient();
	}
	
	public void launchProcess() {
		log.info("lauch Procces JBPM");
		try{
			KieServicesClient client = getClient();
			ProcessServicesClient processclient = client.getServicesClient(ProcessServicesClient.class);
			Map<String, Object> variables = new HashMap<>();
			
//			variables.put("employee", "jack");
			variables.put("id", 1);
			variables.put("numeroOrden", "11122233");
			variables.put("descripcion", "my Despction");
			variables.put("idCliente", "01");
			
			processclient.startProcess(CONTAINER, PROCESS_ID, variables);
			log.info("End Procces JBPM");
		}catch (Exception e){
			e.printStackTrace();
		}		
	}
	
	public void datosclient(){
		try{
			KieServicesClient client = getClient();
			ProcessServicesClient processclient = client.getServicesClient(ProcessServicesClient.class);
			
			ProcessDefinition pd = processclient.getProcessDefinition(CONTAINER, PROCESS_ID);
			
			System.out.println("____ id del proceso "+pd.getId()+" ---- "+pd.getName());
			
			Map<String, String> mpVar = pd.getProcessVariables();
			Map<String, String> mpTask = pd.getServiceTasks();
					
//			println(mpVar);
			
			System.out.println("  -----------  "); 
//			println(mpTask);
			
			System.out.println("____instacia de procesos ----------- ");
			List<ProcessInstance> lstPI = processclient.findProcessInstances(CONTAINER, 0, 10);
			for(ProcessInstance pi:lstPI){
				System.out.println("id  "+ pi.getId()+"  --  "+pi.getCorrelationKey()+"  --  "+pi.getProcessInstanceDescription()
				+"   --  "+pi.getState());
				
				System.out.println("____nodos activos o pendientes ----------- ");
				List<NodeInstance> lstNI = processclient.findActiveNodeInstances(CONTAINER, pi.getId(), 0, 10);
				for(NodeInstance ni:lstNI){
					System.out.println("-------  "+ ni.getId()+"  --  "+ni.getNodeId()+"  --  "+ni.getName()
					+"   --  "+ni.getNodeType()+"  --  "+ni.getWorkItemId());
					
					System.out.println("____trabajo activos o pendientes ----------- ");
					WorkItemInstance wi = processclient.getWorkItem(CONTAINER, pi.getId(), +ni.getWorkItemId());
					System.out.println("-------  "+ wi.getId()+"  --  "+wi.getName()+"  --  "+wi.getState());
					
				}
				
				
				
				System.out.println("____variables de instancia ----------- ");
				List<VariableInstance> lstVI = processclient.findVariablesCurrentState(CONTAINER, pi.getId());
				for(VariableInstance vi:lstVI){
					System.out.println("-------  "+ vi.getVariableName()+"  --  "+vi.getValue()+"  --  "+vi.getOldValue());
				}
				
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
	}	
	
	private KieServicesClient getClient(){
		KieServicesConfiguration config= KieServicesFactory.newRestConfiguration(URL, user, password);
//		Marshalling
		Set<Class<?>> extraClasses = new HashSet<Class<?>>();
		
		Map<String, String> headers = null;
		config.setHeaders(headers);		
		KieServicesClient client = KieServicesFactory.newKieServicesClient(config);
		return client;
	}
	
	
}
