package com.carvajal.platform.jbpm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @see  Inicio Bpm
 * @author dsarmientoa
 *
 */
@SpringBootApplication
public class JbpmApplication {

	public static void main(String[] args) {
		SpringApplication.run(JbpmApplication.class, args);
	}
}
