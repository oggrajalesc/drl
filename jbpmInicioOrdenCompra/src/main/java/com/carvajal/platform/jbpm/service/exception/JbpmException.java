package com.carvajal.platform.jbpm.service.exception;

public class JbpmException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4754669118106523879L;

	public JbpmException() {
		super();
	}
	
	public JbpmException(String message) {
		super("---"+message+"---");
	}
}
