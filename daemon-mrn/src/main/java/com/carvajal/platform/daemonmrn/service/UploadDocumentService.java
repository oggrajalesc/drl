package com.carvajal.platform.daemonmrn.service;

import com.carvajal.platform.core.dto.DocumentoS3DTO;
import com.carvajal.platform.daemonmrn.error.DaemonMRNException;

/**
 * 
 * 
 * @author Djaramillo
 *
 */
public interface UploadDocumentService {

	/**
	 * Metodo para subir un documento a S3
	 * @param document
	 * @throws Exception
	 */
	void uploadDocument(DocumentoS3DTO document) throws DaemonMRNException;
	
}
