package com.carvajal.platform.daemonmrn.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.carvajal.platform.core.dto.DocumentoDTO;

/**
 * Clase encargada de consumir el servicio de documentos
 * 
 * @author Djaramillo
 *
 */
@Component
public class MasterDocumentClient {

	@Value("${masterDocumentServiceUrl}")
	private String documentServiceUrl;

	/**
	 * Metodo para crear un nuevo documento mediante el consumo de DocumentService
	 * 
	 * @param order
	 * @return
	 */
	public DocumentoDTO createOrder(DocumentoDTO order) {
		DocumentoDTO orderSaved = null;
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<DocumentoDTO> request = new HttpEntity<>(order);
		orderSaved = restTemplate.postForObject(documentServiceUrl + "/api/documentos", request, DocumentoDTO.class);
		return orderSaved;
	}

}
