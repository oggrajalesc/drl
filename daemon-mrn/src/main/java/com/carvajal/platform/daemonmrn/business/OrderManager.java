package com.carvajal.platform.daemonmrn.business;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.carvajal.platform.core.dto.DocumentoDTO;
import com.carvajal.platform.core.dto.DocumentoS3DTO;
import com.carvajal.platform.core.enums.Estados;
import com.carvajal.platform.daemonmrn.client.MasterDocumentClient;
import com.carvajal.platform.daemonmrn.client.OrdersClient;
import com.carvajal.platform.daemonmrn.config.Constants;
import com.carvajal.platform.daemonmrn.error.DaemonMRNException;
import com.carvajal.platform.daemonmrn.service.UploadDocumentService;

/**
 * Clase encargada de manejar la logica del demonio
 * 
 * @author Djaramillo
 */
@Component
public class OrderManager {

	private final Logger log = LoggerFactory.getLogger(OrderManager.class);

	@Autowired
	UploadDocumentService uploadDocumentService;
	@Autowired
	OrdersClient ordersClient;
	@Autowired
	MasterDocumentClient masterDocumentClient;

	/**
	 * Metodo encargado de escuchar los cambios sobre una carpeta y disparar el
	 * procesamiento sobre los nuevos archivos
	 * 
	 * @param basepath   carpeta a monitorear
	 * @param bucketName nombre del bucket al que se subiran los archivos
	 * @param regionName nombre de la region en la que se encuentra el bucket
	 */
	@Async
	public void startDaemon(String basepath, String bucketName, String regionName, String type) {
		log.info("daemon is monitoring: {}", basepath);
		try (WatchService watchService = FileSystems.getDefault().newWatchService()) {
			Path path = Paths.get(basepath);
			path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE,
					StandardWatchEventKinds.ENTRY_MODIFY);
			WatchKey key;
			while ((key = watchService.take()) != null) {
				for (WatchEvent<?> event : key.pollEvents()) {
					if (StandardWatchEventKinds.ENTRY_MODIFY.equals(event.kind())) {
						final Path eventPath = (Path) event.context();
						log.info("New file detected: {}", eventPath);
						processFile(eventPath, basepath, bucketName, regionName, type);
					} else {
						log.info("Event kind: {}", event.kind());
						log.info(". File affected: {}", event.context());
					}
				}
				key.reset();
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			log.error(e.getMessage(), e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * Metodo encargado de orquestar el procesamiento de un archivo
	 * 
	 * @param path
	 * @param basepath
	 * @param bucketName
	 * @param regionName
	 * @param type
	 */
	private void processFile(Path path, String basepath, String bucketName, String regionName, String type) {
		log.info("Starting to process file: {}", path);
		try {
			uploadFile(path, basepath, bucketName, regionName, path.getFileName().toString());
			if (Constants.ORDER_TYPE.equals(type)) {
				DocumentoDTO orderSaved = ordersClient.createOrder(buildDocument(path.getFileName().toString()));
				if (orderSaved == null) {
					log.error("the order was not created!!");
				} else {
					log.info("the order was created : {}", orderSaved);
				}
			}
			log.info("The file has finished its process: {}", path);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	/**
	 * Metodo encargado de hacer el flujo para subir un archivo a S3
	 * 
	 * @param path
	 * @param basepath
	 * @param bucketName
	 * @param regionName
	 * @param nombreDocumento
	 * @throws Exception
	 */
	public void uploadFile(Path path, String basepath, String bucketName, String regionName, String nombreDocumento)
			throws DaemonMRNException {
		if (!validatePath(path)) {
			DocumentoS3DTO documentoS3 = new DocumentoS3DTO();
			documentoS3.setNombreBucket(bucketName);
			documentoS3.setNombreDocumento(nombreDocumento);
			documentoS3.setRegionName(regionName);
			documentoS3.setRutaArchivo(basepath);
			uploadDocumentService.uploadDocument(documentoS3);
		} else {
			throw new DaemonMRNException("the found path is not valid!!");
		}
	}

	/**
	 * Metodo para validar que la ruta sea valida y que corresponda a un archivo
	 * 
	 * @param path
	 * @return
	 */
	public boolean validatePath(Path path) {
		if (path == null) {
			return false;
		}
		File file = path.toFile();
		return (file.exists() && !file.isDirectory());
	}

	/**
	 * Construye el Dto inicial del documento
	 * 
	 * @param fileName
	 * @return
	 */
	public DocumentoDTO buildDocument(String fileName) {
		DocumentoDTO newOrder = new DocumentoDTO();
		newOrder.setNombreDocumento(fileName);
		newOrder.setEstado(Estados.PENDIENTE.name());
		newOrder.setFechaDocumento(new Date());
		return newOrder;
	}

}
