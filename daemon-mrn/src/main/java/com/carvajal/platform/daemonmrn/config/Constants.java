package com.carvajal.platform.daemonmrn.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String ORDER_TYPE = "order";
    public static final String MASTER_TYPE = "master";
    
    private Constants() {
    }
}
