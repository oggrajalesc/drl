package com.carvajal.platform.daemonmrn.service.impl;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.carvajal.platform.core.dto.DocumentoS3DTO;
import com.carvajal.platform.daemonmrn.error.DaemonMRNException;
import com.carvajal.platform.daemonmrn.service.UploadDocumentService;

/**
 * Implementación del servicio para cargar archivos en S3
 * 
 * @author Djaramillo
 */
@Service
public class UploadDocumentServiceImpl implements UploadDocumentService {

	private final Logger log = LoggerFactory.getLogger(UploadDocumentServiceImpl.class);

	@Value("${awsAccessKey}")
	private String awsAccessKey;
	@Value("${awsSecretKey}")
	private String awsSecretKey;

	/**
	 * @see UploadDocumentService#uploadDocument(DocumentoS3DTO)
	 */
	@Override
	public void uploadDocument(DocumentoS3DTO document) throws DaemonMRNException {
		log.info("uploading {}!!", document);
		File file = new File(document.getRutaArchivo() + File.separator + document.getNombreDocumento());
		AWSCredentials credentials = getCredentials();
		AmazonS3 s3client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(getRegion(document)).build();
		s3client.putObject(document.getNombreBucket(), document.getNombreDocumento(), file);
		log.info("file uplodaded {}!!", document);
	}

	/**
	 * Metodo para obtener las credenciales de aws
	 * 
	 * @return
	 */
	private AWSCredentials getCredentials() throws DaemonMRNException {
		if (awsAccessKey == null || awsSecretKey == null) {
			log.error("getCredentials() - No aws credentials were found!");
			throw new DaemonMRNException("No aws credentials were found!");
		}
		return new BasicAWSCredentials(awsAccessKey, awsSecretKey);
	}

	/**
	 * Metodo para obtener una region de aws en base a su nombre
	 * 
	 * @param document
	 * @return
	 */
	protected Regions getRegion(DocumentoS3DTO document) {
		Regions region;
		String regionName = document.getRegionName() == null ? "" : document.getRegionName();
		switch (regionName) {
		case "us-east-1":
			region = Regions.US_EAST_1;
			break;
		case "us-east-2":
			region = Regions.US_EAST_2;
			break;
		case "us-west-1":
			region = Regions.US_WEST_1;
			break;
		case "eu-west-1":
			region = Regions.EU_WEST_1;
			break;
		case "ap-southeast-1":
			region = Regions.AP_SOUTH_1;
			break;
		case "ap-southeast-2":
			region = Regions.AP_SOUTHEAST_2;
			break;
		case "ap-northeast-1":
			region = Regions.AP_NORTHEAST_1;
			break;
		case "sa-east-1":
			region = Regions.SA_EAST_1;
			break;
		default:
			region = Regions.US_WEST_2;
		}
		return region;
	}

}
