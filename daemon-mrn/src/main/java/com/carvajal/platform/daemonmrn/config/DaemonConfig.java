package com.carvajal.platform.daemonmrn.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Clase de configuracion de la aplicacion
 * 
 * @author Djaramillo
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class DaemonConfig {

	private String appName;
	private String awsAccessKey;
	private String awsSecretKey;
	private String documentServiceUrl;
	private String masterDocumentServiceUrl;
	private List<FolderConfig> folderConfigurations;

	public String getAppName() {
		return appName;
	}

	public String getAwsAccessKey() {
		return awsAccessKey;
	}

	public void setAwsAccessKey(String awsAccessKey) {
		this.awsAccessKey = awsAccessKey;
	}

	public String getAwsSecretKey() {
		return awsSecretKey;
	}

	public void setAwsSecretKey(String awsSecretKey) {
		this.awsSecretKey = awsSecretKey;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public List<FolderConfig> getFolderConfigurations() {
		return folderConfigurations;
	}

	public void setFolderConfigurations(List<FolderConfig> folderConfigurations) {
		this.folderConfigurations = folderConfigurations;
	}

	public String getDocumentServiceUrl() {
		return documentServiceUrl;
	}

	public void setDocumentServiceUrl(String documentServiceUrl) {
		this.documentServiceUrl = documentServiceUrl;
	}

	public String getMasterDocumentServiceUrl() {
		return masterDocumentServiceUrl;
	}

	public void setMasterDocumentServiceUrl(String masterDocumentServiceUrl) {
		this.masterDocumentServiceUrl = masterDocumentServiceUrl;
	}

	/**
	 * Clase con la configuracion para un folder supervisado por el demonio
	 * 
	 * @author Djaramillo
	 */
	public static class FolderConfig {

		private String basepath;

		private String bucketName;
		private String regionName;
		private String type;

		public String getBasepath() {
			return basepath;
		}

		public void setBasepath(String basepath) {
			this.basepath = basepath;
		}

		public String getBucketName() {
			return bucketName;
		}

		public void setBucketName(String bucketName) {
			this.bucketName = bucketName;
		}

		public String getRegionName() {
			return regionName;
		}

		public void setRegionName(String regionName) {
			this.regionName = regionName;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
		
	    @Override
	    public String toString() {
	        return "DocumentoDTO{" +
	            "basepath=" + getBasepath() +
	            ", bucketName=" + getBucketName() +
	            ", regionName='" + getRegionName() + "'" +
	            ", type='" + getType() + "'" +
	            "}";
	    }

	}

}
