package com.carvajal.platform.daemonmrn.error;

public class DaemonMRNException extends Exception {

    private static final long serialVersionUID = 1L;

    public DaemonMRNException(String message) {
        super(message);
    }
}
