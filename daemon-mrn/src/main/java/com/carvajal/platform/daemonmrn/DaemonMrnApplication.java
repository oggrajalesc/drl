package com.carvajal.platform.daemonmrn;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

import com.carvajal.platform.daemonmrn.business.OrderManager;
import com.carvajal.platform.daemonmrn.config.DaemonConfig;
import com.carvajal.platform.daemonmrn.config.DaemonConfig.FolderConfig;

/**
 * Punto de entrada para la ejecucion del demonio 
 * 
 * 
 * @author Djaramillo
 */
@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan(basePackages="com.carvajal.platform.daemonmrn")
public class DaemonMrnApplication implements ApplicationRunner{
	
	@Autowired
	OrderManager documentManager;
	@Autowired
	DaemonConfig daemonConfig;
	
	/**
	 * Metodo main de la aplicacion
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(DaemonMrnApplication.class, args);
	}

	/**
	 * Metodo encargado de obtener los directorios configurados para ser monitoreados y empezar dicho monitoreo
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {
		List<FolderConfig> folderConfigurations = daemonConfig.getFolderConfigurations();
		for(FolderConfig config : folderConfigurations) {
			documentManager.startDaemon(config.getBasepath(), config.getBucketName(), config.getRegionName() , config.getType());
		}
		
	}
	
}
