package com.carvajal.platform.daemonmrn.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.amazonaws.regions.Regions;
import com.carvajal.platform.core.dto.DocumentoS3DTO;

/**
 * 
 * 
 * @author Djaramillo
 *
 */
public class UploadDocumentServiceImplTest {
	
	UploadDocumentServiceImpl uploadDocumentServiceImpl = new UploadDocumentServiceImpl();
	
	public void uploadDocument() { 
		
	}

	@Test
	public void getRegionWest2RegionNameNull() {
		assertEquals(Regions.US_WEST_2, uploadDocumentServiceImpl.getRegion(new DocumentoS3DTO()));
	}
	
	@Test
	public void getRegionEast2RegionNameUsEast2() {
		DocumentoS3DTO doc = new DocumentoS3DTO();
		doc.setRegionName("us-east-2");
		assertEquals(Regions.US_EAST_2, uploadDocumentServiceImpl.getRegion(doc));
	}

}
