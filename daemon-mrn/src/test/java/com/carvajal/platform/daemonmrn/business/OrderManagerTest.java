package com.carvajal.platform.daemonmrn.business;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 
 * 
 * @author Djaramillo
 *
 */
public class OrderManagerTest {
	
	OrderManager orderManager = new OrderManager();

	@Test
	public void buildDocumentNombreDocumentoNull() {
		assertEquals(null, orderManager.buildDocument(null).getNombreDocumento());
	}
	
	@Test
	public void buildDocumentNombreDocumentoNotNull() {
		assertEquals("nombreDocumento", orderManager.buildDocument("nombreDocumento").getNombreDocumento());
	}
	
	@Test
	public void validatePathNull() {
		assertEquals(false, orderManager.validatePath(null));
	}
	
//	@Test
//	public void validatePathNotNull() {
//		assertEquals(false, orderManager.validatePath(null));
//	}

}
