package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestRegularExpresion {

	public static void main(String[] args) {
		// ESPRESIONES: 
		//1 -> 0 Y 1   -> [0,1]{1}
		//2 -> 5 Enteros,2 decimales  -> ([1-9]{1}[0-9]{1,4}|[0]{1})(\\.[0-9]{2}) 
		//3 -> 12 Enteros,3 decimales  -> ([1-9]{1}[0-9]{1,11}|[0]{1})(\\.[0-9]{3}) 
		//4 -> 11 Enteros,4 decimales  -> ([1-9]{1}[0-9]{1,10}|[0]{1})(\\.[0-9]{4}) 
		//5 -> 3 Enteros,3 decimales  -> ([1-9]{1}[0-9]{1,2}|[0]{1})(\\.[0-9]{3}) 
		//6 -> Lunes a Domingo -> ([L]{0,1}[M]{0,1}[W]{0,1}[J]{0,1}[V]{0,1}[S]{0,1}[D]{0,1})

		//N -> Lunes a Domingo -> ([aL]{0,1}[M]{0,1}[W]{0,1}[J]{0,1}[V]{0,1}[S]{0,1}[D]{0,1})
		
		//co|7705326075758|27030|Pan Blanco 470g CP BIM|1|||10|14218|02833|2833|||1||
		//Codigo interno del punto de venta|EAN Punto de venta|Nombre punto de venta|Codigo centro de venta|Frecuencia de montaje de pedido|Incluye IGV / IVA												
		String cadena ="Maestro_Puntos_Venta_2018-12-11-1212.txt";
//		cadena1 = StringUtils.stripStart(cadena1, null);

		cadena = cadena.trim();


		
//		List<String> data = new ArrayList<>(); 
//		//(List<String>)Arrays.asList(cadena.split("\\|"));
//		data.addAll(Arrays.asList(cadena.split("\\|")));
//		if('|' == cadena.charAt(cadena.length()-1)) {
//			data.add("");
//		}
//
//		System.out.println("data leng:"+data.size());
//		System.out.println("data:"+data.get(0));
//		System.out.println("data:"+data.get(1));
//		System.out.println("data:"+data.get(4));
//		System.out.println("data:"+data.get(5));
		
		
		System.out.println("cadena:"+cadena);
		System.out.println("size:"+cadena.length());
		
		String anno="([1-9]{1}[0-9]{1,3})";
		String mes="([0]{1}[0-9]{1}|[1]{1}[0-2]{1})";
		String dia="([0]{1}[0-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-1]{1})";
		String name="(Maestro_Puntos_Venta_{1})";
		String cadena3 = "Maestro_Puntos_Venta_2018-10-21";
		String linea = "([-]{1})";
		
		
		String patronNombreArchivo =  "(Maestro_Puntos_Venta_)".concat(anno).concat(linea).concat(mes).concat(linea).concat(dia);
		Pattern pat = Pattern.compile(patronNombreArchivo);
//		System.out.println("cadena:"+cadena);
//		System.out.println("size:"+cadena.length());
//		String cadena4 = cadena3.substring(0,cadena3.length()-4);
//		System.out.println("cadena4:"+cadena4);
		Matcher mat = pat.matcher(cadena3);
		if (mat.matches()) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}

	}

}
