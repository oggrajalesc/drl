package com.carvajal.platform.core.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Documento entity.
 */
public class DocumentoDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1138693276082080610L;

    private Long id;

    private Long idDocumentoPadre;

    @NotNull
    private String nombreDocumento;

    private String tipoDocumento;

    @NotNull
    private Date fechaDocumento;

    private Date fechaEntrega;

    private String codPais;

    private String emisor;

    private String receptor;

    @NotNull
    private String estado;

    private String idDocumentStorage;

    private String idDynamo;

    private String formato;

    private Long documentoPadreId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdDocumentoPadre() {
        return idDocumentoPadre;
    }

    public void setIdDocumentoPadre(Long idDocumentoPadre) {
        this.idDocumentoPadre = idDocumentoPadre;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getCodPais() {
        return codPais;
    }

    public void setCodPais(String codPais) {
        this.codPais = codPais;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getReceptor() {
        return receptor;
    }

    public void setReceptor(String receptor) {
        this.receptor = receptor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdDocumentStorage() {
        return idDocumentStorage;
    }

    public void setIdDocumentStorage(String idDocumentStorage) {
        this.idDocumentStorage = idDocumentStorage;
    }

    public String getIdDynamo() {
        return idDynamo;
    }

    public void setIdDynamo(String idDynamo) {
        this.idDynamo = idDynamo;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public Long getDocumentoPadreId() {
        return documentoPadreId;
    }

    public void setDocumentoPadreId(Long documentoId) {
        this.documentoPadreId = documentoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DocumentoDTO documentoDTO = (DocumentoDTO) o;
        if (documentoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documentoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DocumentoDTO{" +
            "id=" + getId() +
            ", idDocumentoPadre=" + getIdDocumentoPadre() +
            ", nombreDocumento='" + getNombreDocumento() + "'" +
            ", tipoDocumento='" + getTipoDocumento() + "'" +
            ", fechaDocumento='" + getFechaDocumento() + "'" +
            ", fechaEntrega='" + getFechaEntrega() + "'" +
            ", codPais='" + getCodPais() + "'" +
            ", emisor='" + getEmisor() + "'" +
            ", receptor='" + getReceptor() + "'" +
            ", estado='" + getEstado() + "'" +
            ", idDocumentStorage='" + getIdDocumentStorage() + "'" +
            ", idDynamo='" + getIdDynamo() + "'" +
            ", formato='" + getFormato() + "'" +
            ", documentoPadre=" + getDocumentoPadreId() +
            "}";
    }
}
