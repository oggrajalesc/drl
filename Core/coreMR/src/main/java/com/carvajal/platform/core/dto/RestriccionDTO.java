package com.carvajal.platform.core.dto;

import java.util.List;

/**
 * A DTO for the Restriccion entity.
 */
public class RestriccionDTO{


    private String id;

    private String maestroId;

    private String posicion;
    
    private String obligatoriedad;
    
    private String nombre;
    
    private String tipoDato;
    
    private String longitud;
    
    private String patron; 
    
    private List<String> dominio;
    
    private String tamanoDecimales;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMaestroId() {
		return maestroId;
	}

	public void setMaestroId(String maestroId) {
		this.maestroId = maestroId;
	}

	public String getPosicion() {
		return posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	public String getObligatoriedad() {
		return obligatoriedad;
	}

	public void setObligatoriedad(String obligatoriedad) {
		this.obligatoriedad = obligatoriedad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getPatron() {
		return patron;
	}

	public void setPatron(String patron) {
		this.patron = patron;
	}

	public List<String> getDominio() {
		return dominio;
	}

	public void setDominio(List<String> dominio) {
		this.dominio = dominio;
	}

	public String getTamanoDecimales() {
		return tamanoDecimales;
	}

	public void setTamanoDecimales(String tamanoDecimales) {
		this.tamanoDecimales = tamanoDecimales;
	}

    
}
