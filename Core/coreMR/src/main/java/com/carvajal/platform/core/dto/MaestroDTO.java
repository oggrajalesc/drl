package com.carvajal.platform.core.dto;

import java.util.List;


public class MaestroDTO{

    private String id;

    private String pais;

    private String cliente;

    private String tipo;

    private String fecha;
    
    private List<RestriccionDTO> restricciones;


    public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public List<RestriccionDTO> getRestricciones() {
		return restricciones;
	}

	public void setRestricciones(List<RestriccionDTO> restricciones) {
		this.restricciones = restricciones;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
