package com.carvajal.platform.core.dto;

import java.io.Serializable;

public class DocumentStorageInfoDTO implements Serializable{
	
	
	private static final long serialVersionUID = 8147861992150017720L;
	
	 private String id;
	 private String properties;
	 private String metadata;


	 // Getter Methods 

	 public String getId() {
	  return id;
	 }

	 public String getProperties() {
	  return properties;
	 }

	 public String getMetadata() {
	  return metadata;
	 }

	 // Setter Methods 

	 public void setId(String id) {
	  this.id = id;
	 }

	 public void setProperties(String properties) {
	  this.properties = properties;
	 }

	 public void setMetadata(String metadata) {
	  this.metadata = metadata;
	 }
	/*
	private String id;
	private String propeties;
	private Date uploadDate;
	private String folderName;
	private String documentName;
	private String hash;
	private String markToDelete;
	private Metadata metadata;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPropeties() {
		return propeties;
	}
	public void setPropeties(String propeties) {
		this.propeties = propeties;
	}
	public Date getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getMarkToDelete() {
		return markToDelete;
	}
	public void setMarkToDelete(String markToDelete) {
		this.markToDelete = markToDelete;
	}
	public Metadata getMetadata() {
		return metadata;
	}
	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Metadata{
		
		@JsonProperty("Account") 
		private String account;
		
		@JsonProperty("CreationDate") 
		private Date creationDate;
		
		@JsonProperty("Company") 
		private String company;
		
		@JsonProperty("FullFilePath") 
		private String fullFilePath;
		
		@JsonProperty("TipoArchivo") 
		private String tipoArchivo;
		
		@JsonProperty("Channel") 
		private String channel;
		
		@JsonProperty("FileName") 
		private String fileName;
		
		@JsonProperty("Country") 
		private String country;
		
		@JsonProperty("RelativeFilePath") 
		private String relativeFilePath;
		
		@JsonProperty("Mimetype") 
		private String mimetype;
		
		@JsonProperty("TransaccionId") 
		private String transaccionId;
		
		@JsonProperty("FileSize") 
		private String fileSize;
		
		@JsonCreator
		public Metadata(
				@JsonProperty("account")  String account,
				@JsonProperty("creationDate")  Date creationDate,
				@JsonProperty("company")  String company,
				@JsonProperty("fullFilePath")  String fullFilePath,
				@JsonProperty("tipoArchivo")  String tipoArchivo,
				@JsonProperty("channel")  String channel,
				@JsonProperty("fileName")  String fileName,
				@JsonProperty("country")  String country,
				@JsonProperty("relativeFilePath")  String relativeFilePath,
				@JsonProperty("mimetype")  String mimetype,
				@JsonProperty("transaccionId")  String transaccionId,
				@JsonProperty("fileSize")  String fileSize			
				) {
			this.account = account;
			this.creationDate = creationDate;
			this.company = company;
			this.fullFilePath = fullFilePath;
			this.tipoArchivo = tipoArchivo;
			this.channel = channel;
			this.fileName = fileName;
			this.country = country;
			this.relativeFilePath = relativeFilePath;
			this.mimetype = mimetype;
			this.transaccionId = transaccionId;
			this.fileSize = fileSize;
			
			
		}
		
		public String getAccount() {
			return account;
		}

		public void setAccount(String account) {
			this.account = account;
		}

		public Date getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(Date creationDate) {
			this.creationDate = creationDate;
		}

		public String getCompany() {
			return company;
		}

		public void setCompany(String company) {
			this.company = company;
		}

		public String getFullFilePath() {
			return fullFilePath;
		}

		public void setFullFilePath(String fullFilePath) {
			this.fullFilePath = fullFilePath;
		}

		public String getTipoArchivo() {
			return tipoArchivo;
		}

		public void setTipoArchivo(String tipoArchivo) {
			this.tipoArchivo = tipoArchivo;
		}

		public String getChannel() {
			return channel;
		}

		public void setChannel(String channel) {
			this.channel = channel;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getRelativeFilePath() {
			return relativeFilePath;
		}

		public void setRelativeFilePath(String relativeFilePath) {
			this.relativeFilePath = relativeFilePath;
		}

		public String getMimetype() {
			return mimetype;
		}

		public void setMimetype(String mimetype) {
			this.mimetype = mimetype;
		}

		public String getTransaccionId() {
			return transaccionId;
		}

		public void setTransaccionId(String transaccionId) {
			this.transaccionId = transaccionId;
		}

		public String getFileSize() {
			return fileSize;
		}

		public void setFileSize(String fileSize) {
			this.fileSize = fileSize;
		}

		@Override
		public String toString() {
			return "Metada{" +
					"Account='"  + "'" +
					", CreationDate='" + getCreationDate() + "'" + 
					", Company='" +  getCompany() + "'" +
					", FullFilePath='" +  getFullFilePath() + "'" +
					", TipoArchivo='" + getTipoArchivo() + "'" + 
					", Channel='" + getChannel() + "'" + 
					", FileName='" + getFileName() + "'" +
					", Country='" + getCountry() + "'" +
					", RelativeFilePath='" + getRelativeFilePath() + "'" + 
					", Mimetype='"  + getMimetype() + "'" +
					", TransaccionId='" +  getTransaccionId() + "'" +
					", FileSize='" + getFileSize() + "'" +				
					"}";
		}		
	}
	
	@Override
	public String toString() {
		return "DocumentStorageInfoDto{" +
				"id=" + getId() +
				", propeties='" + getPropeties() + "'"+
				", uploadDate='" +  getUploadDate() + "'"+
				", folderName='" + getFolderName() + "'"+
				", documentName='" + getDocumentName() + "'"+
				", hash='" +  getHash() +"'"+
				", markToDelete='" +  getMarkToDelete() + "'"+
				", metaData='" + getMetadata() + "'"+				
				"}";
	}
*/
}
