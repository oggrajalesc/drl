package com.carvajal.platform.core.enums;

public enum Estados {
	NO_INICIADA, INICIADA, RADICADA, PENDIENTE, EN_PROCESO, RECHAZADA, COMPLETADA
}
