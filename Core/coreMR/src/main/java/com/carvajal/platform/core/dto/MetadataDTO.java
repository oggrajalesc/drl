package com.carvajal.platform.core.dto;

import java.io.Serializable;

public class MetadataDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String account;
	private String company;
	private String tipoArchivo;
	private String country;
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getTipoArchivo() {
		return tipoArchivo;
	}
	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Override
	public String toString() {
		return "Metada{" +
				"company='" +  getCompany() + "'" +
				", account='"  + getAccount() + "'" +
				", tipoArchivo='" + getTipoArchivo() + "'" + 
				", country='" + getCountry() + "'" +
				"}";
	}
	
}
