package com.carvajal.platform.core.dto;

public class ProductoDTO {
    private String id;

    private String pais;

    private String codigoEAN;

    private String codigoERP;

    private String descripcion;

    private String estado;

    private String organizacionVentas;

    private String codigoInterno;

    private String factorConversion;

    private String codigoCentroVenta;
    
    private String stockSeguridad;
    
    private String precioNeto;
	
	private String precioBruto;

    private String socioComercialEAN;

    private String codigoCiudad;

    private String rotacionProducto;

    private String unidadMedida;

    private String impuesto;
    
    private String idMaestro;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCodigoEAN() {
		return codigoEAN;
	}

	public void setCodigoEAN(String codigoEAN) {
		this.codigoEAN = codigoEAN;
	}

	public String getCodigoERP() {
		return codigoERP;
	}

	public void setCodigoERP(String codigoERP) {
		this.codigoERP = codigoERP;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getOrganizacionVentas() {
		return organizacionVentas;
	}

	public void setOrganizacionVentas(String organizacionVentas) {
		this.organizacionVentas = organizacionVentas;
	}

	public String getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	public String getFactorConversion() {
		return factorConversion;
	}

	public void setFactorConversion(String factorConversion) {
		this.factorConversion = factorConversion;
	}

	public String getCodigoCentroVenta() {
		return codigoCentroVenta;
	}

	public void setCodigoCentroVenta(String codigoCentroVenta) {
		this.codigoCentroVenta = codigoCentroVenta;
	}

	public String getStockSeguridad() {
		return stockSeguridad;
	}

	public void setStockSeguridad(String stockSeguridad) {
		this.stockSeguridad = stockSeguridad;
	}

	public String getPrecioNeto() {
		return precioNeto;
	}

	public void setPrecioNeto(String precioNeto) {
		this.precioNeto = precioNeto;
	}

	public String getPrecioBruto() {
		return precioBruto;
	}

	public void setPrecioBruto(String precioBruto) {
		this.precioBruto = precioBruto;
	}

	public String getSocioComercialEAN() {
		return socioComercialEAN;
	}

	public void setSocioComercialEAN(String socioComercialEAN) {
		this.socioComercialEAN = socioComercialEAN;
	}

	public String getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(String codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	public String getRotacionProducto() {
		return rotacionProducto;
	}

	public void setRotacionProducto(String rotacionProducto) {
		this.rotacionProducto = rotacionProducto;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public String getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}

	public String getIdMaestro() {
		return idMaestro;
	}

	public void setIdMaestro(String idMaestro) {
		this.idMaestro = idMaestro;
	}

	@Override
	public String toString() {
		return "ProductoDTO [id=" + id + ", pais=" + pais + ", codigoEAN=" + codigoEAN + ", codigoERP=" + codigoERP
				+ ", descripcion=" + descripcion + ", estado=" + estado + ", organizacionVentas=" + organizacionVentas
				+ ", codigoInterno=" + codigoInterno + ", factorConversion=" + factorConversion + ", codigoCentroVenta="
				+ codigoCentroVenta + ", stockSeguridad=" + stockSeguridad + ", precioNeto=" + precioNeto
				+ ", precioBruto=" + precioBruto + ", socioComercialEAN=" + socioComercialEAN + ", codigoCiudad="
				+ codigoCiudad + ", rotacionProducto=" + rotacionProducto + ", unidadMedida=" + unidadMedida
				+ ", impuesto=" + impuesto + ", idMaestro=" + idMaestro + "]";
	}
	
    
}
