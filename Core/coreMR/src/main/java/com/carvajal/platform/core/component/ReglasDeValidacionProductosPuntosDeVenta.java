package com.carvajal.platform.core.component;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carvajal.platform.core.dto.RestriccionDTO;

public class ReglasDeValidacionProductosPuntosDeVenta {
	private static final String CONTIENE_UN_TIPO_DE_DATO_NO_VÁLIDO = " contiene un tipo de dato no válido.";
	private static final Logger LOGGER = LoggerFactory.getLogger(ReglasDeValidacionProductosPuntosDeVenta.class);
	// private static final String ERROR_PARTE_5 = "---------";
	// private static final String ERROR_PARTE_4 = "  datoAEvaluar: ";
	// private static final String ERROR_PARTE_3 = "Campo: ";
	private static final String ERROR_PARTE_2 = "] Columna [";
	private static final String ERROR_PARTE_1 = "La fila [";
	
	public static boolean validarExtensionExitosa(String nombreDelArchivoAValidar) {
		/*USING LOGGER*/
		LOGGER.info("Charset: " + Charset.defaultCharset());
		LOGGER.info("				<<<<--validarExtensionExitosa--->>>>");
		LOGGER.info("Argumentos: ");
		LOGGER.info("	nombreDelArchivoAValidar: " + nombreDelArchivoAValidar);
		
		boolean result = false;		
		if("txt".equalsIgnoreCase(nombreDelArchivoAValidar.substring(nombreDelArchivoAValidar.length()-3, nombreDelArchivoAValidar.length()))){
			result = true;
		}
		return result;
	}


	public static boolean validarSeparadoresPorLineaExitosa(List<String> lineasDelArchivo, List<RestriccionDTO> restricciones) {
		/*USING LOGGER*/
		LOGGER.info("Charset: " + Charset.defaultCharset());
		LOGGER.info("				<<<<--validarSeparadoresPorLineaExitosa--->>>>");
		LOGGER.info("Argumentos: ");
		LOGGER.info("	lineasDelArchivo: " + lineasDelArchivo.size());
		LOGGER.info("	restricciones: " 	+ restricciones.size());		
		if(lineasDelArchivo.isEmpty()){
			return false;
		}		
		boolean result = true;
		for (String linea : lineasDelArchivo) {
			if(!linea.isEmpty() && linea.length() > 1){
				List<String> componentesDeLaLinea = new ArrayList<>(Arrays.asList(linea.split(Pattern.quote("|"))));
				if(componentesDeLaLinea.size() != restricciones.size()){
					return false;
				}	
			}
		}
		return result;
	}


	public static void validarNumeroDeColumnasDeLaLinea(List<RestriccionDTO> restricciones, List<String> listadoDeErrores,int numeroDeLaFilaEvaluando, List<String> componentesDeLaLinea) {
		String error;
		//numero de cols de la linea VS num de restricciones 
		if(componentesDeLaLinea.size() != restricciones.size()){
			error = " no cuenta con el número de columnas definidas";
			listadoDeErrores.add("La fila "+numeroDeLaFilaEvaluando + error);
				//LOGGER.error(" Error en validarNumeroDeColumnasDeLaLinea-->> " + error);
		}
		
	}


	public static void validarElTipoDeDatoDelComponenteActualDeLaLinea(List<String> listadoDeErrores,
			int numeroDeLaFilaEvaluando, RestriccionDTO restriccionConfiguradaEnDBDelComponenteDeLaLinea,
			String datoAEvaluar) {
		
		String error;
		//por tipo de dato Alfa o Numerico
		if( "N".equalsIgnoreCase(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getTipoDato())){
			if(!"0".equalsIgnoreCase(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getTamanoDecimales())){
				if (!esNumericoConEneDigitosYConEneDecimales(
						datoAEvaluar,
						restriccionConfiguradaEnDBDelComponenteDeLaLinea.getLongitud(),
						restriccionConfiguradaEnDBDelComponenteDeLaLinea.getTamanoDecimales()
						)){
					error = CONTIENE_UN_TIPO_DE_DATO_NO_VÁLIDO;
					listadoDeErrores.add(ERROR_PARTE_1+numeroDeLaFilaEvaluando+ERROR_PARTE_2+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre()+"] "+ error);
						//LOGGER.error(" Error en validarElTipoDeDatoDelComponenteActualDeLaLinea-->> " + ERROR_PARTE_3 + restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre() + ERROR_PARTE_4 + datoAEvaluar + ERROR_PARTE_5 + error);
				}
			}else{
				//Estado de producto (activo / inactivo o Rotacion de producto [1 ó 0]
				if(!esNumerico(datoAEvaluar)){
					error = CONTIENE_UN_TIPO_DE_DATO_NO_VÁLIDO;
					listadoDeErrores.add(ERROR_PARTE_1+numeroDeLaFilaEvaluando+ERROR_PARTE_2+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre()+"] "+ error);
						//LOGGER.error(" Error en validarElTipoDeDatoDelComponenteActualDeLaLinea-->> " + ERROR_PARTE_3 + restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre() + ERROR_PARTE_4 + datoAEvaluar + ERROR_PARTE_5 + error);
				}
			}
		}else if( "AN".equalsIgnoreCase(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getTipoDato())){
			if(!esAN(datoAEvaluar)){
				error = CONTIENE_UN_TIPO_DE_DATO_NO_VÁLIDO;
				listadoDeErrores.add(ERROR_PARTE_1+numeroDeLaFilaEvaluando+ERROR_PARTE_2+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre()+"] "+ error);
					//LOGGER.error(" Error en validarElTipoDeDatoDelComponenteActualDeLaLinea-->> " + ERROR_PARTE_3 + restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre() + ERROR_PARTE_4 + datoAEvaluar + ERROR_PARTE_5 + error);
			}
		}
		
	}

	public static boolean esAN(String numeroAEvaluar) {		
        // return Pattern.matches("^[A-Za-z0-9_]*$", numeroAEvaluar);
        return Pattern.matches("^[A-Za-z0-9\\s]+$", numeroAEvaluar);        
	}
	
	public static boolean esNumericoConEneDigitosYConEneDecimales(String numeroAEvaluar, String nDigits, String nDecimal) {
		try{
			if (new BigDecimal(numeroAEvaluar).compareTo(BigDecimal.ZERO) < 0){
				return false;
			}
		}catch(NumberFormatException nfe){
			return false;
		}
		String  expresion = "(?<![\\d.])(\\d{1,"+nDigits+"}|\\d{0,"+nDigits+"}\\.\\d{1,"+nDecimal+"})?(?![\\d.])";		
		String numeroAEvaluarInterno = new BigDecimal(numeroAEvaluar).toString();		
		return numeroAEvaluarInterno.matches(expresion);
	}
	
	public static boolean esNumerico(String numeroAEvaluar) {
		try{
			if (new BigDecimal(numeroAEvaluar).compareTo(BigDecimal.ZERO) < 0){
				return false;
			}
		}catch(NumberFormatException nfe){
			return false;
		}
		String  expresion = "^[0-9]*$";
		String numeroAEvaluarInterno = new BigDecimal(numeroAEvaluar).toString();
		return numeroAEvaluarInterno.matches(expresion);
	}

	public static void validarLaObligatoriedadDelComponenteActualDeLaLinea(List<String> listadoDeErrores, RestriccionDTO restriccionConfiguradaEnDBDelComponenteDeLaLinea,
			String datoAEvaluar, int numeroDeLaFilaEvaluando) {
		String error;
		//Obligatoriedad
		if("R".equalsIgnoreCase(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getObligatoriedad()) && datoAEvaluar.isEmpty()){
			error = " esta vacío.";
			listadoDeErrores.add(ERROR_PARTE_1+numeroDeLaFilaEvaluando+ERROR_PARTE_2+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre()+"] "+ error);
				//LOGGER.error(" Error en validarLaObligatoriedadDelComponenteActualDeLaLinea-->> " + ERROR_PARTE_3 + restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre() + ERROR_PARTE_4 + datoAEvaluar + ERROR_PARTE_5 + error);
		}
		
	}


	public static void validarElTamanoDelComponenteActualDeLaLinea(List<String> listadoDeErrores, int numeroDeLaFilaEvaluando,
			RestriccionDTO restriccionConfiguradaEnDBDelComponenteDeLaLinea, String datoAEvaluar) {
		String error;
		//Tamano
		Integer tamanoTotalDeLaRestriccion = Integer.valueOf(Integer.valueOf(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getLongitud()) + Integer.valueOf(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getTamanoDecimales()));
		Integer tamanoDelComponenteDeLaLinea = datoAEvaluar.length();		
		if(!"0".equalsIgnoreCase(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getTamanoDecimales())){
			tamanoTotalDeLaRestriccion = tamanoTotalDeLaRestriccion + 1;
		}
		if(tamanoDelComponenteDeLaLinea > tamanoTotalDeLaRestriccion){
			error = " excede el tamaño permitido.";
			listadoDeErrores.add(ERROR_PARTE_1+numeroDeLaFilaEvaluando+ERROR_PARTE_2+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre()+"] "+ error);
				//LOGGER.error(" Error en validarElTamanoDelComponenteActualDeLaLinea-->> " + ERROR_PARTE_3 + restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre() + ERROR_PARTE_4 + datoAEvaluar + ERROR_PARTE_5 + error);
		}
		
	}


	public static void validarLosDecimalesDelComponenteActualDeLaLinea(List<String> listadoDeErrores,
			int numeroDeLaFilaEvaluando, RestriccionDTO restriccionConfiguradaEnDBDelComponenteDeLaLinea,
			String datoAEvaluar) {
		String error;
		//decimales
		if(	(!"0".equalsIgnoreCase(restriccionConfiguradaEnDBDelComponenteDeLaLinea.getTamanoDecimales())) &&
			(!esNumericoConEneDigitosYConEneDecimales(
						datoAEvaluar,
						restriccionConfiguradaEnDBDelComponenteDeLaLinea.getLongitud(),
						restriccionConfiguradaEnDBDelComponenteDeLaLinea.getTamanoDecimales()
			))				
		){
			error = " tiene un valor que no corresponden a los enteros o a los decimales esperados.";
			listadoDeErrores.add(ERROR_PARTE_1+numeroDeLaFilaEvaluando+ERROR_PARTE_2+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre()+"] "+ error);
				//LOGGER.error(" Error en validarLosDecimalesDelComponenteActualDeLaLinea-->> " + ERROR_PARTE_3 + restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre() + ERROR_PARTE_4 + datoAEvaluar + ERROR_PARTE_5 + error);
			
		}
	}

	public static void validaElValorDelComponenteActualDeLaLinea(List<String> listadoDeErrores, int numeroDeLaFilaEvaluando,
			RestriccionDTO restriccionConfiguradaEnDBDelComponenteDeLaLinea, String datoAEvaluar) {
		String error;
		//Dominio
		if(!restriccionConfiguradaEnDBDelComponenteDeLaLinea.getDominio().isEmpty()){
			String valor = restriccionConfiguradaEnDBDelComponenteDeLaLinea.getDominio().stream().filter(x -> datoAEvaluar.equalsIgnoreCase(x)).findAny().orElse(null);
			if(Objects.isNull(valor) || valor.isEmpty()) {
				error = " cuenta con un valor que no corresponde a los del Dominio del campo.";
				listadoDeErrores.add(ERROR_PARTE_1+numeroDeLaFilaEvaluando+ERROR_PARTE_2+restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre()+"] "+ error);
					//LOGGER.error(" Error en validaElValorDelComponenteActualDeLaLinea-->> " + ERROR_PARTE_3 + restriccionConfiguradaEnDBDelComponenteDeLaLinea.getNombre() + ERROR_PARTE_4 + datoAEvaluar + ERROR_PARTE_5 + error);
			}
		}		
	}
	
    public static boolean validaLaEstructuraDelNombreDelArchivoExitosa(String nombreDelArchivoAValidar, String prefijo) {
    	//	prefijo : Maestro_Productos - Maestro_Puntos_Venta
boolean result = true;
        String anno="([1-9]{1}[0-9]{1,3})";
        String mes="([0]{1}[0-9]{1}|[1]{1}[0-2]{1})";
        String dia="([0]{1}[0-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-1]{1})";
        String name="("+prefijo+"_{1})";
        String linea = "([-]{1})";        
        String patronNombreArchivo =  name.concat(anno).concat(linea).concat(mes).concat(linea).concat(dia);
        Pattern pat = Pattern.compile(patronNombreArchivo);
        Matcher mat = pat.matcher(nombreDelArchivoAValidar.substring(0,nombreDelArchivoAValidar.length()-4));
        if (mat.matches()) {
            result = true;
        }
        return result;
  }

    public static boolean validaLaEstructuraDelNombreDelArchivoExitosa2(String nombreDelArchivoAValidar, String prefijo) {
    	boolean result = true;
		if( (nombreDelArchivoAValidar.length() != 37) || (!nombreDelArchivoAValidar.contains(prefijo)) ){
			result = false;
		}
		String cadenaAValidarFecha = nombreDelArchivoAValidar.substring(prefijo.length() + 1, nombreDelArchivoAValidar.length() - 4);
		String[] cadenaAValidarFechaPartes = cadenaAValidarFecha.split("-");		
		
		if (!esNumerico(cadenaAValidarFechaPartes[0]) || !esNumerico(cadenaAValidarFechaPartes[1]) || 
			!esNumerico(cadenaAValidarFechaPartes[2]) || !esNumerico(cadenaAValidarFechaPartes[3])){
			result = false;
		}
		return result;
    }

	
}