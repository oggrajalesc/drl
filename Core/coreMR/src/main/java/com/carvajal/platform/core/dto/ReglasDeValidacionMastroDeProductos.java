package com.carvajal.platform.core.dto;

import java.util.List;

public class ReglasDeValidacionMastroDeProductos {

	private List<RestriccionDTO> restricciones;

	public List<RestriccionDTO> getRestricciones() {
		return restricciones;
	}

	public void setRestricciones(List<RestriccionDTO> restricciones) {
		this.restricciones = restricciones;
	}
}
