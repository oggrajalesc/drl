package com.carvajal.platform.core.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Documento de s3.
 */
public class DocumentoS3DTO implements Serializable {

	private static final long serialVersionUID = -7178195534472194911L;

	@NotNull
    private String nombreDocumento;

	@NotNull
    private String rutaArchivo;

    @NotNull
    private String nombreBucket;

    @NotNull
    private String regionName;
    

    public String getNombreDocumento() {
		return nombreDocumento;
	}


	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}


	public String getRutaArchivo() {
		return rutaArchivo;
	}


	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}


	public String getNombreBucket() {
		return nombreBucket;
	}


	public void setNombreBucket(String nombreBucket) {
		this.nombreBucket = nombreBucket;
	}


	public String getRegionName() {
		return regionName;
	}


	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}


	@Override
    public String toString() {
        return "DocumentoDTO{" +
            "nombreDocumento=" + getNombreDocumento() +
            ", idDocumentoPadre=" + getNombreBucket() +
            ", nombreDocumento='" + getRegionName() + "'" +
            ", rutaArchivo='" + getRutaArchivo() + "'" +
            "}";
    }
}
