package com.carvajal.platform.core.dto;

import java.io.Serializable;
import java.util.Date;

public class MetaDataDocumentDTO implements Serializable{
	
	private static final long serialVersionUID = -6399903705521987367L;
	
	private String Account;
	private Date CreationDate;
	private String Company;
	private String FullFilePath;
	private String TipoArchivo;
	private String Channel;
	private String FileName;
	private String Country;
	private String RelativeFilePath;
	private String Mimetype;
	private String TransaccionId;
	private String FileSize;
	
	public String getAccount() {
		return Account;
	}
	public void setAccount(String account) {
		Account = account;
	}
	public Date getCreationDate() {
		return CreationDate;
	}
	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getFullFilePath() {
		return FullFilePath;
	}
	public void setFullFilePath(String fullFilePath) {
		FullFilePath = fullFilePath;
	}
	public String getTipoArchivo() {
		return TipoArchivo;
	}
	public void setTipoArchivo(String tipoArchivo) {
		TipoArchivo = tipoArchivo;
	}
	public String getChannel() {
		return Channel;
	}
	public void setChannel(String channel) {
		Channel = channel;
	}
	public String getFileName() {
		return FileName;
	}
	public void setFileName(String fileName) {
		FileName = fileName;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getRelativeFilePath() {
		return RelativeFilePath;
	}
	public void setRelativeFilePath(String relativeFilePath) {
		RelativeFilePath = relativeFilePath;
	}
	public String getMimetype() {
		return Mimetype;
	}
	public void setMimetype(String mimetype) {
		Mimetype = mimetype;
	}
	public String getTransaccionId() {
		return TransaccionId;
	}
	public void setTransaccionId(String transaccionId) {
		TransaccionId = transaccionId;
	}
	public String getFileSize() {
		return FileSize;
	}
	public void setFileSize(String fileSize) {
		FileSize = fileSize;
	}
	@Override
	public String toString() {
		return "MetaDataDocumentDto{" +
				"Account='"  + "'" +
				", CreationDate='" + getCreationDate() + "'" + 
				", Company='" +  getCompany() + "'" +
				", FullFilePath='" +  getFullFilePath() + "'" +
				", TipoArchivo='" + getTipoArchivo() + "'" + 
				", Channel='" + getChannel() + "'" + 
				", FileName='" + getFileName() + "'" +
				", Country='" + getCountry() + "'" +
				", RelativeFilePath='" + getRelativeFilePath() + "'" + 
				", Mimetype='"  + getMimetype() + "'" +
				", TransaccionId='" +  getTransaccionId() + "'" +
				", FileSize='" + getFileSize() + "'" +				
				"}";
	}
	
	
}
