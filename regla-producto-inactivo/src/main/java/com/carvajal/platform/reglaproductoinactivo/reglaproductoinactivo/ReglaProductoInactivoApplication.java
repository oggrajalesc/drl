package com.carvajal.platform.reglaproductoinactivo.reglaproductoinactivo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReglaProductoInactivoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReglaProductoInactivoApplication.class, args);
	}
}
